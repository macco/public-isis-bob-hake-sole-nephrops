/*
 * Copyright (C) 2017 avigier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package scripts;

import static org.nuiton.i18n.I18n.n;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;

import fr.ifremer.isisfish.IsisFishDAOHelper;
import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.util.Nocache;
import resultinfos.*;
import static java.util.Arrays.asList;

/**
 * ExportRecruSpat.java
 */
public class ExportRecruSpat extends SiMatrix {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(ExportRecruSpat.class);

    public ExportRecruSpat(SimulationContext context) throws TopiaException {
        super(context);
        //System.out.println("J'utilise nouveau constructeur de SiMatrix du simulateur");
    }


     /**
     * This method is called in DefaultSimulator. It is empty in the vanilla SiMatrix. Over-riding it allows some extra stuff in the simulator.
     * @param step
     * @param pop
     * @return 
     */
    @Override
    public void computeMonthExtra(TimeStep step, Population pop, MatrixND N) throws TopiaException, IsisFishException {
        //System.out.println("computeMonthExtra nouveaux simulateur et SiMatrix");
        //Add recruitment per zone result
        if (resManager.isEnabled(MatrixRecruitmentPerZone.NAME)) {
            MatrixND RecruitmentMatrixPerZone = RecruitmentPerZone(step, pop);
            resManager.addResult(step, pop, RecruitmentMatrixPerZone);
        }    
        /*
        //Add catch per SS3 fleet result. This sould be mase after discards, hence after PostAction, but to complicated to implement.
        if (resManager.isEnabled(MatrixCatchWeightAgregPourOptim.NAME)) {
            //Only for hake!
            if(pop.getName().equals("merluccius")){
                MatrixND CatchMatrixWeightAgregPourOptim = CatchWeightAgregPourOptim(step, pop);
                resManager.addResult(step, pop, CatchMatrixWeightAgregPourOptim);
            }
        }
        */
    }
    
	 /**
     * Permet de sortir le recrutement par pas de temps ET par zone.
     * @param step
     * @param pop
     * @return 
     */
    public MatrixND RecruitmentPerZone(TimeStep step, Population pop) throws TopiaException {
        //System.out.println("J'utilise la methode recruitmentPerZone du nouveau SiMatrix");
        
        List<Population> populations = Collections.singletonList(pop);//Lists all populations
        List<PopulationGroup> groups = pop.getPopulationGroup();
        List<Zone> zones = context.getPopulationMonitor().getPopulations().get(context.getPopulationMonitor().getPopulations().indexOf(pop)).getRecruitmentZone(); // Lists recruitment zones for pop population only

        MatrixND RecruitmentMatrixPerZone = MatrixFactory.getInstance().create(MatrixRecruitmentPerZone.NAME,new List[]{populations, zones},new String[]{n("Population"),n("Zones")});

        //log.info("Recruitment_1 = " + RecruitmentMatrix);
        
        // Pour chaque pas de temps on recupere le recrutement
        for(Zone zone : zones){
            double val = context.getPopulationMonitor().getRecruitment(step, pop).getValue(groups.get(0), zone);
            RecruitmentMatrixPerZone.setValue(pop,zone,val); // Dims of get Recruitment : 0 = groups ; 1 = zones. Take forst length group and the good zone.
            //System.out.println("Zone : " + zone + " Pop : " + pop.getName() + " Valeur : " + val);
        }
        //System.out.println("Espece_recru : " + pop.getSpecies().getName());
        //System.out.println("Matrice_recru : " + context.getPopulationMonitor().getRecruitment(step, pop));
        
        //log.info("RecruitmentMatrix_2 = " + RecruitmentMatrix);

        return RecruitmentMatrixPerZone;
    }

     /**
     * Permet de sortir le recrutement par pas de temps ET par zone.
     * @param step
     * @param pop
     * @return 
     **/
     /*
    public MatrixND CatchWeightAgregPourOptim(TimeStep step, Population pop) throws TopiaException {
        //System.out.println("J'utilise la methode recruitmentPerZone du nouveau SiMatrix");
            
        List<String> metierListSS3 = asList("LONGLINEGILLNET_BOB","TRAWL_FISH_BOB_E","TRAWL_FISH_BOB_W","TRAWL_NEP");
        List<String> partitions = asList("landing","discard");
        List<PopulationGroup> groups = pop.getPopulationGroup();

        MatrixND CatchMatrixWeightAgregPourOptim = MatrixFactory.getInstance().create(MatrixCatchWeightAgregPourOptim.NAME,new List[]{metierListSS3,partitions,groups},new String[]{n("metier"),n("partition"),n("group")});
    
        //For hake and Bay of Biscay only!
        TopiaContext tx = context.getDB();
        List<Metier> metierList = IsisFishDAOHelper.getMetierDAO(tx).findAll();
        tx.closeContext();
        List<String> lGBOB = asList("Metier_FiletSole_NordC","Metier_FiletSole_NordIntPC","Metier_FiletSole_InterSudPC","Metier_FiletSole_InterC","Metier_PalangreMerlu_InterSudAC","Metier_PalangreMerlu_InterSudC","Metier_PalangreMixte_NordC","Metier_PalangreMixte_InterC","Metier_FiletMerlu_NordC","Metier_FiletMerlu_NordPC","Metier_FiletMixte_NordPC","Metier_FiletMixte_NordC","Metier_FiletMixte_NordInterPC","Metier_FiletMixte_InterSudC","Metier_FiletMIxte_InterC","Metier_FiletMerlu_InterSudAPC","Metier_FiletMerlu_InterSudC","Metier_FiletMerlu_InterSudPC");
        List<String> tFBOBE = asList("Metier_ChalutMixte_NordPC","Metier_ChaluMixte_InterC","Metier_ChalutBenth_NordAPC","Metier_ChalutBenth_NordC","Metier_ChalutBenth_APCS","Metier_ChalutBenth_Inter","Metier_ChalutBenth_S","Metier_ChalutSole_InterC","Metier_ChalutSole_InterSudC","Metier_ChalutSole_NordCet","Metier_ChalutMixte_NordC","Metier_ChalutMixte_APCS");
        List<String> tFBOBW = asList("PTBV_VIIIabd","OBTS_VIIIabd");
        List<String> tNEP = asList("Metier_Lang_NordPC","Metier_Lang_InterC","Metier_Lang_InterPC");
        

        //Catch, get metier semantics numbers
        MatrixND matLand = context.getResultManager().getMatrix(step, pop,MatrixCatchWeightPerStrategyMetPerZonePop.NAME);
        //System.out.println("Somme captures : " + matLand.sumAll());
        List<String> matLandSemantics = (List<String>)matLand.getSemantic(1);
        
        int countLGBOB =0;
        int countTFBOBE =0;
        int countTFBOBW =0;
        int countTNEP =0;
        int countSems = 0;
        int metLGBOBSemanticsNumLand [] = new int [18];
        int metTFBOBESemanticsNumLand [] = new int [12];
        int metTFBOBWSemanticsNumLand [] = new int [2];
        int metTNEPSemanticsNumLand [] = new int [3];
        for(String metier : matLandSemantics){
            if(lGBOB.contains(metier)){
                metLGBOBSemanticsNumLand[countLGBOB]=countSems;
                countLGBOB+=1;
            }
            if(tFBOBE.contains(metier)){
                metTFBOBESemanticsNumLand[countTFBOBE]=countSems;
                countTFBOBE+=1;
            }
            if(tFBOBW.contains(metier)){
                metTFBOBWSemanticsNumLand[countTFBOBW]=countSems;
                countTFBOBW+=1;
            }
            if(tNEP.contains(metier)){
                metTNEPSemanticsNumLand[countTNEP]=countSems;
                countTNEP+=1;
            }
            countSems+=1;
        }
        
        //Create as many submatrix as SS3 fleets
        MatrixND matLGBOBLand = matLand.getSubMatrix(1,metLGBOBSemanticsNumLand);
        MatrixND matTFBOBELand = matLand.getSubMatrix(1,metTFBOBESemanticsNumLand);
        MatrixND matTFBOBWLand = matLand.getSubMatrix(1,metTFBOBWSemanticsNumLand);
        MatrixND matNEPLand = matLand.getSubMatrix(1,metTNEPSemanticsNumLand);
        /*
        System.out.println("Somme captures LGBOB : " + matLGBOBLand.sumAll());
        System.out.println("Somme captures TFBOBE: " + matTFBOBELand.sumAll());
        System.out.println("Somme captures TFBOBW: " + matTFBOBWLand.sumAll());
        System.out.println("Somme captures TNEP: " + matNEPLand.sumAll());
        */
        /*
        //Discards, get metier semantics numbers
        MatrixND matDisc = context.getResultManager().getMatrix(step, pop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME);
        List<String> matDiscSemantics = (List<String>)matDisc.getSemantic(1);
        countLGBOB =0;
        countTFBOBE =0;
        countTFBOBW =0;
        countTNEP =0;
        countSems = 0;
        int metLGBOBSemanticsNumDisc [] = new int [18];
        int metTFBOBESemanticsNumDisc [] = new int [12];
        int metTFBOBWSemanticsNumDisc [] = new int [2];
        int metTNEPSemanticsNumDisc [] = new int [3];
        for(String metier : matDiscSemantics){
            if(lGBOB.contains(metier)){
                metLGBOBSemanticsNumDisc[countLGBOB]=countSems;
                countLGBOB+=1;
            }
            if(tFBOBE.contains(metier)){
                metTFBOBESemanticsNumDisc[countTFBOBE]=countSems;
                countTFBOBE+=1;
            }
            if(tFBOBW.contains(metier)){
                metTFBOBWSemanticsNumDisc[countTFBOBW]=countSems;
                countTFBOBW+=1;
            }
            if(tNEP.contains(metier)){
                metTNEPSemanticsNumDisc[countTNEP]=countSems;
                countTNEP+=1;
            }
            countSems+=1;
        }
        
        //Create as many submatrix as SS3 fleets
        MatrixND matLGBOBDisc = matDisc.getSubMatrix(1,metLGBOBSemanticsNumDisc);
        MatrixND matTFBOBEDisc = matDisc.getSubMatrix(1,metTFBOBESemanticsNumDisc);
        MatrixND matTFBOBWDisc = matDisc.getSubMatrix(1,metTFBOBWSemanticsNumDisc);
        MatrixND matNEPDisc = matDisc.getSubMatrix(1,metTNEPSemanticsNumDisc);
        /*
        System.out.println("Somme rejets LGBOB : " + matLGBOBDisc.sumAll());
        System.out.println("Somme rejets TFBOBE: " + matTFBOBEDisc.sumAll());
        System.out.println("Somme rejets TFBOBW: " + matTFBOBWDisc.sumAll());
        System.out.println("Somme rejets TNEP: " + matNEPDisc.sumAll());
        */
        /*
        //Convert catch to landings
        matLGBOBLand = matLGBOBLand.minus(matLGBOBDisc);
        matTFBOBELand = matTFBOBELand.minus(matTFBOBEDisc);
        matTFBOBWLand = matTFBOBWLand.minus(matTFBOBWDisc);
        matNEPLand = matNEPLand.minus(matNEPDisc);

        List<MatrixND> matrixToWrite = asList(matLGBOBLand,matTFBOBELand,matTFBOBWLand,matNEPLand,matTFBOBWDisc,matNEPDisc);
        List<String> metierNames = asList("LONGLINEGILLNET_BOB","TRAWL_FISH_BOB_E","TRAWL_FISH_BOB_W","TRAWL_NEP","TRAWL_FISH_BOB_W","TRAWL_NEP");
        List<PopulationGroup> newPopSemantics = groups.subList(2,51) ;
        int count = 0;
        for(MatrixND mat : matrixToWrite){
            mat = mat.sumOverDim(0); //sum on strategy
            mat = mat.sumOverDim(1); //sum on métiers
            mat = mat.sumOverDim(3); //sum on zones
            mat = mat.sumOverDim(2,50,15); //sum on length classes from 100cm (class 50) and above (on 15 length classes)
            mat = mat.getSubMatrix(2,2,49);//remove classes 0 and 1 (under 4cm)
            mat.setSemantic(2,newPopSemantics);//Change populations semantics (classes 2 to 50 = classes 4 to 98 cm, and 100+cm class)
            String metierName = metierNames.get(count);// get metier name
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                PopulationGroup group = (PopulationGroup) sems[2];
                double val = i.getValue();
                if (count<4){//Landings
                    CatchMatrixWeightAgregPourOptim.setValue(metierName,"landing",group,val);
                }
                else if(count>3){//Discards
                    CatchMatrixWeightAgregPourOptim.setValue(metierName,"discard",group,val);
                }
            }
            count+=1;
        }
        return CatchMatrixWeightAgregPourOptim;
    }
       */
}
