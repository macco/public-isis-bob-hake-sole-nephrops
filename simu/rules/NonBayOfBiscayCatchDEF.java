/*
 * Copyright (C) 2017 avigier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package rules;

import static org.nuiton.i18n.I18n._;

import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;
import java.util.HashSet;
import java.util.Set;
import java.io.File;
import java.io.FilenameFilter;
import java.io.FileReader;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.util.FileUtil;
import org.nuiton.topia.TopiaContext;

//import javax.constraints.*;

import scripts.RuleUtil;
import scripts.SiMatrix;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Equation;
import fr.ifremer.isisfish.entities.Gear;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Selectivity;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.ui.input.gear.GearPopulationSelectivityModel;
import fr.ifremer.isisfish.util.Doc;


/**
 * NonBayOfBiscayCatch.java
 */
public class NonBayOfBiscayCatchDEF extends AbstractRule {

    /** to use log facility, just put in your code: System.out.println("..."); */
    private static Log log = LogFactory.getLog(NonBayOfBiscayCatchDEF.class);

    @Doc("Affected species")
    public Population param_Population = null; //pour le merlu
    
    @Doc("Begin date (time step number)")
    public TimeStep param_beginStep = new TimeStep(0); //January 2010
    @Doc("End date (time step number)")
    public TimeStep param_endStep = new TimeStep(132); //January 2020
    
    // Files on landings and discards in number between 2010 and 2012 for all non Bay of Biscay fleets
    //public String param_directory = "S:/ApplicationsISIS/AppliMerlu_Langoustine_Sole/2017_MSE_SS3_Merlu/Inputs_SoleLangoustineMerluEnLongeurGdG";
    public String param_directory = "input";
    protected File  NoNBoBLandings;
    public MatrixND matrixCatchCeltic;
    public MatrixND matrixCatchNorth;
    public MatrixND temp;
    public MatrixND LGCSlandings;
    public MatrixND TFCSElandings;
    public MatrixND TFCSWlandings;
    public MatrixND OTHERSlandings;
    public MatrixND TFCSWdiscards;
    public MatrixND OTHERSdiscards;
    public MatrixND histoTACs;
    
    protected File  NonBoBExport;
    protected MatrixND matrixNoNBoBDiscards;

    boolean affectation = false; // Flag to avoid to loop on métiers with preAction and postAction when unnecessary
    //File exportTest = new File("test.csv");
    
    protected String[] necessaryResult = {
    };

    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * @return L'aide ou la description de la regle
     */
    public String getDescription() throws Exception {
        // TODO
        return "Remove non Bay of Biscay catch : historical on 2010-2012, historical weight and computed length frequencies on 2013-2016, computed on 2017-2020.";
    }

    /**
     * Appelé au démarrage de la simulation, cette méthode permet d'initialiser
     * des valeurs.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     */
    public void init(SimulationContext context) throws Exception {
        // TODO
        NonBoBExport = new File("exports/NonBoBCatch.csv");
    }

    /**
     * La condition qui doit etre vraie pour faire les actions.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     * @return vrai si on souhaite que les actions soit faites
     */
    public boolean condition(SimulationContext context, TimeStep step, Metier metier) throws Exception {
        boolean result = true;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        }
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    public void preAction(SimulationContext context, TimeStep step, Metier metier) throws Exception {
        // Nothing happens here.
        affectation = false;
    }

    /**
     * Si la condition est vrai alors cette action est executée apres le pas
     * de temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerné
     */
    public void postAction(SimulationContext context, TimeStep step, Metier metier) throws Exception {
        //System.out.println("Affectation " + affectation);
        //Do not loop on metiers.
        
        if(!affectation){
            /*           
            //Export targeting factor initial parametrization. Useful only once, this part must always be commented.
            List <Metier> metierList  = context.getSimulationStorage().getFisheryRegion(context.getDB()).getMetier();//Get metier list
            if(step.getStep()==0){// If first time step
    
                for(int i =0; i< metierList.size();i++){
                    System.out.println("Metiers list before remove : " + metierList.get(i).getName());
                }
                metierList = metierList.subList(1,36);
                for(int i =0; i< metierList.size();i++){
                    System.out.println("Metiers list after remove : " + metierList.get(i).getName());
                }
                String header = "Metier;initValue;lowBound;HighBound\n";
                FileUtils.writeStringToFile(exportInitParamFile,header,true);
                for(int i =0; i< metierList.size();i++){
                    paramInit = metierList.get(i).getName()+";"+metierList.get(i).getMetierSeasonInfo(step.getMonth()).getTargetFactor(context.getPopulationMonitor().getPopulations().get(1).getPopulationGroup().get(0))+";0;20\n";
                    FileUtils.writeStringToFile(exportInitParamFile,paramInit,true);
                }
            
            }
            */
            
            //Get Year and step
            int currentYear = step.getYear();
            int currentStep = step.getStep();
            //System.out.println("Annee courante " + currentYear);
            //System.out.println("Pas de temps " + step.getStep());
            
            PopulationMonitor popMon = context.getPopulationMonitor();

            //Unit of population : the same as the initial effectives, so check them to know if these are real numbers or thousands of numbers.
            Population pop = (Population) context.getDB().findByTopiaId(param_Population.getTopiaId());
            MatrixND abondance = popMon.getN(pop);
            List<PopulationGroup> groups = (List<PopulationGroup>)abondance.getSemantic(0);
            
            //2010-2016 : remove historical catch in numbers; loop on historical files
            if(currentYear<7 & currentYear>-1){
                /*
                //List historical files of landings and discards in numbers
                File directory = new File(param_directory + "/simulatingFleets/");
                //Landings
                File[] LandingsFilesList = directory.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File directory, String name) {
                        return name.matches(".*LandingsDataNumbers.*");
                    }
                });
                //Discards
                File[] DiscardsFilesList = directory.listFiles(new FilenameFilter() {
                    @Override
                    public boolean accept(File directory, String name) {
                        return name.matches(".*DiscardsDataNumbers.*");
                    }
                });
                */
                /*
                for(int num=0 ; num<LandingsFilesList.length ; num++){
                    System.out.println(LandingsFilesList[num].getName());
                }
                for(int num=0 ; num<DiscardsFilesList.length ; num++){
                    System.out.println(DiscardsFilesList[num].getName());
                }
                */
                
                int nbGroup = abondance.getDim()[0];
                //System.out.println("Dimension matrice abondance 0 " + nbGroup + " classes de taille");
                int nbStep = 84; // 2010-2016 = 7 years = 84 months
                int[] dimMatrix = {nbStep,nbGroup};
                               
                //Open all the files, stock them in matrixes, close the files
                matrixCatchCeltic = MatrixFactory.getInstance().create(dimMatrix);
                matrixCatchNorth = MatrixFactory.getInstance().create(dimMatrix);
                temp = MatrixFactory.getInstance().create(dimMatrix);

                LGCSlandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSElandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSWlandings= MatrixFactory.getInstance().create(dimMatrix);
                OTHERSlandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSWdiscards= MatrixFactory.getInstance().create(dimMatrix);
                OTHERSdiscards= MatrixFactory.getInstance().create(dimMatrix);

                LGCSlandings.importCSV(new FileReader("input/simulatingFleets/LandingsDataNumbersLONGLINEGILLNET_CS.csv"),new int []{0,0});
                TFCSElandings.importCSV(new FileReader("input/simulatingFleets/LandingsDataNumbersTRAWL_FISH_CS_E.csv"),new int []{0,0});
                TFCSWlandings.importCSV(new FileReader("input/simulatingFleets/LandingsDataNumbersTRAWL_FISH_CS_W.csv"),new int []{0,0});
                OTHERSlandings.importCSV(new FileReader("input/simulatingFleets/LandingsDataNumbersOTHERS.csv"),new int []{0,0});
                TFCSWdiscards.importCSV(new FileReader("input/simulatingFleets/DiscardsDataNumbersTRAWL_FISH_CS_W.csv"),new int []{0,0});
                OTHERSdiscards.importCSV(new FileReader("input/simulatingFleets/DiscardsDataNumbersOTHERS.csv"),new int []{0,0});

                matrixCatchCeltic=matrixCatchCeltic.add(LGCSlandings.reduce()).add(TFCSElandings.reduce()).add(TFCSWlandings.reduce()).add(TFCSWdiscards.reduce()).reduce();
                matrixCatchNorth=matrixCatchNorth.add(OTHERSlandings.reduce()).add(OTHERSdiscards.reduce()).reduce();
                /*
                for(int i = 0 ; i < LandingsFilesList.length ; i++){
                    //System.out.println("Num fichier : "+ i);
                    if (LandingsFilesList[i].getName().equals("LandingsDataNumbersLONGLINEGILLNET_CS.csv")||LandingsFilesList[i].getName().equals("LandingsDataNumbersTRAWL_FISH_CS_E.csv")||LandingsFilesList[i].getName().equals("LandingsDataNumbersTRAWL_FISH_CS_W.csv")){
                        //System.out.println("Nom fichier : "+ LandingsFilesList[i].getName());
                        temp.importCSV(new FileReader(LandingsFilesList[i]),new int []{0,0});
                        matrixCatchCeltic = matrixCatchCeltic.add(temp.reduce());
                        matrixCatchCeltic = matrixCatchCeltic.reduce();
                    }else if(LandingsFilesList[i].getName().equals("LandingsDataNumbersOTHERS.csv")){
                        //System.out.println("Nom fichier : "+ LandingsFilesList[i].getName());
                        temp.importCSV(new FileReader(LandingsFilesList[i]),new int []{0,0});
                        matrixCatchNorth = matrixCatchNorth.add(temp.reduce());
                        matrixCatchNorth = matrixCatchNorth.reduce();
                    }
                    
                }
                */
/*
                matrixLandNumOTHERS = MatrixFactory.getInstance().create(dimMatrix);
                matrixLandNumOTHERS.importCSV(new FileReader(LandingsFilesList[3]),new int []{0,0});
                matrixLandNumOTHERS=matrixLandNumOTHERS.reduce();
                //System.out.println("matrixLandNumOTHERS : " + matrixLandNumOTHERS);
*/              
                /*
                for(int i = 0 ; i<DiscardsFilesList.length ; i++){
                    if (DiscardsFilesList[i].getName().equals("DiscardsDataNumbersTRAWL_FISH_CS_W.csv")){
                        temp.importCSV(new FileReader(DiscardsFilesList[i]),new int []{0,0});
                        matrixCatchCeltic = matrixCatchCeltic.add(temp.reduce());
                        matrixCatchCeltic = matrixCatchCeltic.reduce();
                    }else if(DiscardsFilesList[i].getName().equals("DiscardsDataNumbersOTHERS.csv")){
                        temp.importCSV(new FileReader(DiscardsFilesList[i]),new int []{0,0});
                        matrixCatchNorth = matrixCatchNorth.add(temp.reduce());
                        matrixCatchNorth = matrixCatchNorth.reduce();
                    }                    
                }
                */
                //Discards REAL NUMBERS
                //System.out.println("abondance : " + abondance);
                
                //For each historical file, remove from abundance
                //System.out.println("Abundance matrix before : " + abondance);
                
                for (MatrixIterator i = abondance.iterator(); i.next();) {
                    Object[] coord = i.getSemanticsCoordinates(); // coord[0] = length group ; coord[1] = zone (Celtic Sea or Northern Area)
                    
                    //System.out.println("Nb dimensions matrice abondance 0 " + coord.length);
                    //System.out.println("Semantiques matrice abondance 0 " + coord[0]);
                    //System.out.println("Semantiques matrice abondance 1 " + coord[1]);
                    Zone zone = (Zone) coord[1];
                    
                    if (zone.getName().equals("Zone_CelticSea")) {
                        //Get ISIS-Fish abundance in REAL NUMBERS
                        double valAbondance = abondance.getValue(coord[0],coord[1]);
                        
                        //For each landings and discards file, get land and/or disc value, sum all these values, in REAL NUMBERS
                        double valCatch = matrixCatchCeltic.getValue(step.getStep(),groups.indexOf(coord[0]));    
                        
                        //Replace ISIS-Fish abundance by ISIS-Fish abundance - sum of catch in REAL NUMBERS
                        double newAbondance = valAbondance-valCatch;
                        double ratio = valCatch/valAbondance;
                        if ( ratio < 0.9 ){
                            abondance.setValue(coord[0],coord[1],newAbondance);
                        } else if ( ratio>=0.9) {
                            double newCatch = 0.9*valAbondance ;
                            double ratioCatch = newCatch/valCatch;
                            newAbondance = valAbondance - newCatch;
                            matrixCatchCeltic.setValue(step.getStep(),groups.indexOf(coord[0]),newCatch);
                            LGCSlandings.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*LGCSlandings.getValue(step.getStep(),groups.indexOf(coord[0])));
                            TFCSElandings.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*TFCSElandings.getValue(step.getStep(),groups.indexOf(coord[0])));
                            TFCSWlandings.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*TFCSWlandings.getValue(step.getStep(),groups.indexOf(coord[0])));
                            TFCSWdiscards.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*TFCSWdiscards.getValue(step.getStep(),groups.indexOf(coord[0])));
                            abondance.setValue(coord[0],coord[1],newAbondance);
                            
                                double eqCheck = TFCSWdiscards.getValue(step.getStep(),groups.indexOf(coord[0])) +TFCSWlandings.getValue(step.getStep(),groups.indexOf(coord[0])) +LGCSlandings.getValue(step.getStep(),groups.indexOf(coord[0])) + TFCSElandings.getValue(step.getStep(),groups.indexOf(coord[0]));
                                //System.out.println("EQUALITY CHECK celtic: group " + groups.indexOf(coord[0]) + " step " + step.getStep() + " 1/ " + matrixCatchCeltic.getValue(step.getStep(),groups.indexOf(coord[0])) + " 2/ " + eqCheck);    
                            
                        }
                        //String toExport = valCatch + ";" + zone.getName() + ";" + step.getStep() + ";" + groups.indexOf(coord[0]) + "\n";
                        //FileUtils.writeStringToFile(exportTest,toExport,true);

                        //Put debug stuff everywhere to do checks on calculations
                        //System.out.println("Group " + coord[0] + " Abondance before " + valAbondance + " land num LGCS " + valLand1 + " land num TFCSE" + valLand2 + " land num TFCSW " + valLand3 + " disc num TFCSW" + valDisc + " final abondance " + newAbondance);
                        
                    } else if (zone.getName().equals("Zone_NorthernArea")) {
                        //Get ISIS-Fish abundance in REAL NUMBERS
                        double valAbondance = abondance.getValue(coord[0],coord[1]);
                        
                        //For each landings and discards file, get land and/or disc value, sum all these values, in REAL NUMBERS
                        double valCatch = matrixCatchNorth.getValue(step.getStep(),groups.indexOf(coord[0]));
                        
                        //Replace ISIS-Fish abundance by ISIS-Fish abundance - sum of catch in REAL NUMBERS
                        double newAbondance = valAbondance-valCatch;
                        double ratio = valCatch/valAbondance;
                        if ( ratio < 0.9 ){
                            abondance.setValue(coord[0],coord[1],newAbondance);
                        } else if ( ratio>=0.9) {
                            double newCatch = 0.9*valAbondance ;
                            double ratioCatch = newCatch/valCatch;
                            newAbondance = valAbondance - newCatch;
                            matrixCatchNorth.setValue(step.getStep(),groups.indexOf(coord[0]),newCatch);
                            OTHERSlandings.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*OTHERSlandings.getValue(step.getStep(),groups.indexOf(coord[0])));
                            OTHERSdiscards.setValue(step.getStep(),groups.indexOf(coord[0]),ratioCatch*OTHERSdiscards.getValue(step.getStep(),groups.indexOf(coord[0])));
                            abondance.setValue(coord[0],coord[1],newAbondance); 
                            
                                double eqCheck = OTHERSlandings.getValue(step.getStep(),groups.indexOf(coord[0])) +OTHERSdiscards.getValue(step.getStep(),groups.indexOf(coord[0]));
                                //System.out.println("EQUALITY CHECK north: group " + groups.indexOf(coord[0]) + " step " + step.getStep() + " 1/ " + matrixCatchNorth.getValue(step.getStep(),groups.indexOf(coord[0])) + " 2/ " + eqCheck);    
                            
                        }
                    }
                        //String toExport = valCatch + ";" + zone.getName() + ";" + step.getStep() + ";" + groups.indexOf(coord[0]) + "\n";
                        //FileUtils.writeStringToFile(exportTest,toExport,true);

                        //Put debug stuff everywhere to do checks on calculations
                        //System.out.println("Group " + coord[0] + " Abondance before " + valAbondance + " land num OTHERS " + valLand + " disc num OTHERS" + valDisc + " final abondance " + newAbondance);
                                        
                }
                //System.out.println("Abundance matrix after : " + abondance);
            }
            
            
            //2017-2020 : remove catch, using the TAc and pre-determined length compositions for each fleet and partition
            if(currentYear>6){

                //Get TAC (calculated in last year OgiveDeTriMerluretentionTACNoTAC2010SS3.java condition)
                //Get the simulation parameter
                double TACvalue = (double) context.getValue("TACvalue_" + currentYear);
                
                //Get LFD for the fleet. On this "LFD", proportions on zone, season, fleet have already been applied, thereis just a product by the weight in tons to do. For each fleet/partition/season , the unit is numbers. It has been computed so that if converted in weight, the total caught is 1 tonne.
                //List historical files of landings and discards in numbers

                int nbGroup = abondance.getDim()[0];
                //System.out.println("Dimension matrice abondance 0 " + nbGroup + " classes de taille");
                int nbStep = 12; // 12 months.
                int[] dimMatrix = {nbStep,nbGroup};
                               
                //Open all the files, stock them in matrixes, close the files
                matrixCatchCeltic = MatrixFactory.getInstance().create(dimMatrix);
                matrixCatchNorth = MatrixFactory.getInstance().create(dimMatrix);
                temp = MatrixFactory.getInstance().create(dimMatrix);

                LGCSlandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSElandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSWlandings= MatrixFactory.getInstance().create(dimMatrix);
                OTHERSlandings= MatrixFactory.getInstance().create(dimMatrix);
                TFCSWdiscards= MatrixFactory.getInstance().create(dimMatrix);
                OTHERSdiscards= MatrixFactory.getInstance().create(dimMatrix);

                LGCSlandings.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720debLONGLINEGILLNET_CS.csv"),new int []{0,0});
                TFCSElandings.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720debTRAWL_FISH_CS_E.csv"),new int []{0,0});
                TFCSWlandings.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720debTRAWL_FISH_CS_W.csv"),new int []{0,0});
                OTHERSlandings.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720debOTHERS.csv"),new int []{0,0});
                TFCSWdiscards.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720rejTRAWL_FISH_CS_W.csv"),new int []{0,0});
                OTHERSdiscards.importCSV(new FileReader("D:/isis-fish-4.4.2.2/input/simulatingFleets/LFD1720rejOTHERS.csv"),new int []{0,0});

                // Product with the computed TAC, and the constant proportions of TAC for big regions
                // 56% for the celtic Sea
                // 62.5% for the northern area
                LGCSlandings=LGCSlandings.mults(0.56*TACvalue);
                TFCSElandings=TFCSElandings.mults(0.56*TACvalue);
                TFCSWlandings=TFCSWlandings.mults(0.56*TACvalue);
                OTHERSlandings=OTHERSlandings.mults(0.625*TACvalue);
                TFCSWdiscards=TFCSWdiscards.mults(0.56*TACvalue);
                OTHERSdiscards=OTHERSdiscards.mults(0.625*TACvalue);
                
                matrixCatchCeltic=matrixCatchCeltic.add(LGCSlandings.reduce()).add(TFCSElandings.reduce()).add(TFCSWlandings.reduce()).add(TFCSWdiscards.reduce()).reduce();
                matrixCatchNorth=matrixCatchNorth.add(OTHERSlandings.reduce()).add(OTHERSdiscards.reduce()).reduce();
                
                // What month? It is used to pick the good season in the catch matrix
                int monthNumber = step.getStep() - 12*(step.getStep()/12); /// It works because we are working with INTEGERS
   
                for (MatrixIterator i = abondance.iterator(); i.next();) {
                    Object[] coord = i.getSemanticsCoordinates(); // coord[0] = length group ; coord[1] = zone (Celtic Sea or Northern Area)
                    
                    //System.out.println("Nb dimensions matrice abondance 0 " + coord.length);
                    //System.out.println("Semantiques matrice abondance 0 " + coord[0]);
                    //System.out.println("Semantiques matrice abondance 1 " + coord[1]);
                    Zone zone = (Zone) coord[1];                    
                    
                    if (zone.getName().equals("Zone_CelticSea")) {
                        //Get ISIS-Fish abundance in REAL NUMBERS
                        double valAbondance = abondance.getValue(coord[0],coord[1]);
                        
                        //For each landings and discards file, get land and/or disc value, sum all these values, in REAL NUMBERS.
                        double valCatch = matrixCatchCeltic.getValue(monthNumber,groups.indexOf(coord[0]));    
                        
                        //Replace ISIS-Fish abundance by ISIS-Fish abundance - sum of catch in REAL NUMBERS
                        double newAbondance = valAbondance-valCatch;
                        double ratio = valCatch/valAbondance;
                        if ( ratio < 0.9 ){
                            abondance.setValue(coord[0],coord[1],newAbondance);
                        } else if ( ratio>=0.9) {
                            double newCatch = 0.9*valAbondance ;
                            double ratioCatch = newCatch/valCatch;
                            newAbondance = valAbondance - newCatch;
                            matrixCatchCeltic.setValue(monthNumber,groups.indexOf(coord[0]),newCatch);
                            LGCSlandings.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*LGCSlandings.getValue(monthNumber,groups.indexOf(coord[0])));
                            TFCSElandings.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*TFCSElandings.getValue(monthNumber,groups.indexOf(coord[0])));
                            TFCSWlandings.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*TFCSWlandings.getValue(monthNumber,groups.indexOf(coord[0])));
                            TFCSWdiscards.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*TFCSWdiscards.getValue(monthNumber,groups.indexOf(coord[0])));
                            abondance.setValue(coord[0],coord[1],newAbondance); 
                            
                                double eqCheck = TFCSWdiscards.getValue(monthNumber,groups.indexOf(coord[0])) +TFCSWlandings.getValue(monthNumber,groups.indexOf(coord[0])) +LGCSlandings.getValue(monthNumber,groups.indexOf(coord[0])) + TFCSElandings.getValue(monthNumber,groups.indexOf(coord[0]));
                                //System.out.println("EQUALITY CHECK celtic: group " + groups.indexOf(coord[0]) + " step " + step.getStep() + " 1/ " + matrixCatchCeltic.getValue(monthNumber,groups.indexOf(coord[0])) + " 2/ " + eqCheck);    
                            
                        }
                        //String toExport = valCatch + ";" + zone.getName() + ";" + step.getStep() + ";" + groups.indexOf(coord[0]) + "\n";
                        //FileUtils.writeStringToFile(exportTest,toExport,true);

                        //Put debug stuff everywhere to do checks on calculations
                        //System.out.println("Group " + coord[0] + " Abondance before " + valAbondance + " land num LGCS " + valLand1 + " land num TFCSE" + valLand2 + " land num TFCSW " + valLand3 + " disc num TFCSW" + valDisc + " final abondance " + newAbondance);
                        
                    } else if (zone.getName().equals("Zone_NorthernArea")) {
                        //Get ISIS-Fish abundance in REAL NUMBERS
                        double valAbondance = abondance.getValue(coord[0],coord[1]);
                        
                        //For each landings and discards file, get land and/or disc value, sum all these values, in REAL NUMBERS.
                        double valCatch = matrixCatchNorth.getValue(monthNumber,groups.indexOf(coord[0]));
                        
                        //Replace ISIS-Fish abundance by ISIS-Fish abundance - sum of catch in REAL NUMBERS
                        double newAbondance = valAbondance-valCatch;
                        double ratio = valCatch/valAbondance;
                        if ( ratio < 0.9 ){
                            abondance.setValue(coord[0],coord[1],newAbondance);
                        } else if ( ratio>=0.9) {
                            double newCatch = 0.9*valAbondance ;
                            double ratioCatch = newCatch/valCatch;
                            newAbondance = valAbondance - newCatch;
                            matrixCatchNorth.setValue(monthNumber,groups.indexOf(coord[0]),newCatch);
                            OTHERSlandings.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*OTHERSlandings.getValue(monthNumber,groups.indexOf(coord[0])));
                            OTHERSdiscards.setValue(monthNumber,groups.indexOf(coord[0]),ratioCatch*OTHERSdiscards.getValue(monthNumber,groups.indexOf(coord[0])));
                            abondance.setValue(coord[0],coord[1],newAbondance); 
                            
                                double eqCheck = OTHERSlandings.getValue(monthNumber,groups.indexOf(coord[0])) +OTHERSdiscards.getValue(monthNumber,groups.indexOf(coord[0]));
                                //System.out.println("EQUALITY CHECK north: group " + groups.indexOf(coord[0]) + " step " + step.getStep() + " 1/ " + matrixCatchNorth.getValue(monthNumber,groups.indexOf(coord[0])) + " 2/ " + eqCheck);    
                            
                        }                      
                    }                   
                }


                
                /*
                //////////////////////////////////////////////
                //Simulate catch in these zones (calculate catch in numbers)
                //See Methot, et al. 2013 A.5.13 formula. Also, use a solver : http://openrules.com/jsr331/ , see interfaces Solver and Problem. Normally, a few lines should be enough.
                
                // Get selectivities per length for each fleet, accessibility, natural mortality ; abundances, get year and time step => made earlier
                // Selectivities
                List <Metier> metiers = context.getSimulationStorage().getFisheryRegion(context.getDB()).getMetier();
                //System.out.println("Longueur liste metiers : " + metiers.size());
                
                //Create and initialize selectivities. Check : see the commented block and the debug. The selectivities are definied as in SS3.
                Selectivity selLGCS=metiers.get(0).getGear().getPopulationSelectivity(pop) ;
                Selectivity selTFCSW=metiers.get(0).getGear().getPopulationSelectivity(pop) ;
                Selectivity selTFCSE=metiers.get(0).getGear().getPopulationSelectivity(pop) ;
                Selectivity selOTHERS=metiers.get(0).getGear().getPopulationSelectivity(pop) ;

                for(int index=0; index< metiers.size(); index ++){
                    Metier metierInTheLoop = metiers.get(index);
                    String metierName = metierInTheLoop.getName();
                    if(metierName.equals("Metier_GILLNET_CS")){
                        selLGCS = metierInTheLoop.getGear().getPopulationSelectivity(pop);
                    }else if(metierName.equals("Metier_TRAWL_FISH_CS_W")){
                        selTFCSW = metierInTheLoop.getGear().getPopulationSelectivity(pop);
                    }else if(metierName.equals("Metier_TRAWL_FISH_CS_E")){
                        selTFCSE = metierInTheLoop.getGear().getPopulationSelectivity(pop);
                    }else if(metierName.equals("Metier_OTHERS")){
                        selOTHERS = metierInTheLoop.getGear().getPopulationSelectivity(pop);
                    }
                }
                
                
                for(int indexG=0; indexG< metiers.size(); indexG ++){
                    Metier metierInTheLoop = metiers.get(indexG);
                    String metierName = metierInTheLoop.getName();
                    for(int index=0; index< groups.size(); index ++){
                        if(metierName.equals("Metier_GILLNET_CS")){
                            System.out.println("selLGCS group "+ groups.get(index) +" : " + selLGCS.getCoefficient(pop,groups.get(index),metierInTheLoop));
                        }else if(metierName.equals("Metier_TRAWL_FISH_CS_W")){
                            System.out.println("selTFCSW group "+ groups.get(index) +" : " + selTFCSW.getCoefficient(pop,groups.get(index),metierInTheLoop));
                        }else if(metierName.equals("Metier_TRAWL_FISH_CS_E")){
                            System.out.println("selTFCSE group "+ groups.get(index) +" : " + selTFCSE.getCoefficient(pop,groups.get(index),metierInTheLoop));
                        }else if(metierName.equals("Metier_OTHERS")){
                            System.out.println("selOTHERS group "+ groups.get(index) +" : " + selOTHERS.getCoefficient(pop,groups.get(index),metierInTheLoop));
                        }
                    }
                }
                
                
                //Accessibilities
                MatrixND accessibility = pop.getCapturability(); // columns = seasons ; rows = length groups
                //System.out.println("accessibility : " + accessibility);
                
                //Natural mortality
                double naturalM = 0.4;
                //System.out.println("naturalM : " + naturalM);

                //Calculate catch in number per area and length bin for this time step
                                
                //Remove from abundance

                //Alter ISIS-Fish results??? How?
                */
            }
// On stocke cette valeur dans le SimulationContext (pour calculs intermediaires de F dans la regle O)
            if(currentYear<7 & currentYear>-1 ){
                for(int i =0; i<72; i++){
                    double valueNonBob=0;
                    double otherValueNonBob=0;
                    valueNonBob=valueNonBob+matrixCatchNorth.getValue(step.getStep(),i)+matrixCatchCeltic.getValue(step.getStep(),i);
                    otherValueNonBob=otherValueNonBob+OTHERSlandings.getValue(step.getStep(),i) +OTHERSdiscards.getValue(step.getStep(),i)+TFCSWdiscards.getValue(step.getStep(),i) +TFCSWlandings.getValue(step.getStep(),i) +LGCSlandings.getValue(step.getStep(),i) + TFCSElandings.getValue(step.getStep(),i);
                    context.setValue("totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep(), valueNonBob);
                    //System.out.println("totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep() + " ; " + valueNonBob);
                    //System.out.println("Other totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep() + " ; " + otherValueNonBob);
                }
            }
            if(currentYear>6 ){
                for(int i =0; i<72; i++){
                    double valueNonBob=0;
                    double otherValueNonBob=0;
                    valueNonBob=valueNonBob+matrixCatchNorth.getValue(step.getMonth().getMonthNumber(),i)+matrixCatchCeltic.getValue(step.getMonth().getMonthNumber(),i);
                    otherValueNonBob=otherValueNonBob+OTHERSlandings.getValue(step.getMonth().getMonthNumber(),i) +OTHERSdiscards.getValue(step.getMonth().getMonthNumber(),i)+TFCSWdiscards.getValue(step.getMonth().getMonthNumber(),i) +TFCSWlandings.getValue(step.getMonth().getMonthNumber(),i) +LGCSlandings.getValue(step.getMonth().getMonthNumber(),i) + TFCSElandings.getValue(step.getMonth().getMonthNumber(),i);
                    context.setValue("totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep(), valueNonBob);
                    //System.out.println("totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep() + " ; " + valueNonBob);
                    //System.out.println("Other totOtherCatchNumStepNONBOB_" + i + "_" + step.getStep() + " ; " + otherValueNonBob);
                }
            }
            //if(currentYear<7 & currentYear>-1 & step.getMonth().equals(Month.DECEMBER)){
                /*
                System.out.println("Matrix non Bob dimensions " + matrixCatchCeltic.getDimCount());
                System.out.println("Matrix non Bob dimensions " + matrixCatchNorth.getDimCount());
                for(int i =0; i<matrixCatchCeltic.getDimCount(); i++){
                    System.out.println("Matrix non Bob dimensions celtic " + matrixCatchCeltic.getDim(i));  
                    System.out.println("Matrix non Bob dimensions celtic " + matrixCatchCeltic.getDimensionName(i));    
                    System.out.println("Matrix non Bob dimensions celtic " + matrixCatchCeltic.getSemantic(i));          
                }
                for(int i =0; i<matrixCatchNorth.getDimCount(); i++){
                    System.out.println("Matrix non Bob dimensions north " + matrixCatchNorth.getDim(i));  
                    System.out.println("Matrix non Bob dimensions north " + matrixCatchNorth.getDimensionName(i));    
                    System.out.println("Matrix non Bob dimensions north " + matrixCatchNorth.getSemantic(i));          
                }
                System.out.println("Matrix non Bob dimensions A1 celtic " + matrixCatchCeltic);
                System.out.println("Matrix non Bob dimensions A1 north" + matrixCatchNorth);
                */
                //MatrixND catchNoNBoB=matrixCatchCeltic.copy();
                //catchNoNBoB=catchNoNBoB.add(matrixCatchNorth.reduce()).reduce();
                /*
                System.out.println("Matrix non Bob dimensions A2 celtic " + matrixCatchCeltic);
                System.out.println("Matrix non Bob dimensions A2 north" + matrixCatchNorth);
                
                System.out.println("Matrix non Bob dimensions A " + catchNoNBoB.getDimCount());
                for(int i =0; i<catchNoNBoB.getDimCount(); i++){
                    System.out.println("Matrix non Bob dimensions nonbob A" + catchNoNBoB.getDim(i));  
                    System.out.println("Matrix non Bob dimensions nonbob A" + catchNoNBoB.getDimensionName(i));    
                    System.out.println("Matrix non Bob dimensions nonbob A" + catchNoNBoB.getSemantic(i));          
                }
                System.out.println("Matrix non Bob dimensions A " + catchNoNBoB);
                */
                //catchNoNBoB=catchNoNBoB.getSubMatrix(0,(step.getStep()-11),12);
                /*
                System.out.println("Matrix non Bob dimensions B " + catchNoNBoB.getDimCount());
                for(int i =0; i<catchNoNBoB.getDimCount(); i++){
                    System.out.println("Matrix non Bob dimensions nonbob B" + catchNoNBoB.getDim(i));  
                    System.out.println("Matrix non Bob dimensions nonbob B" + catchNoNBoB.getDimensionName(i));    
                    System.out.println("Matrix non Bob dimensions nonbob B" + catchNoNBoB.getSemantic(i));          
                }
                System.out.println("Matrix non Bob dimensions B " + catchNoNBoB);
                */
                
                //catchNoNBoB=catchNoNBoB.sumOverDim(0); // Sum over the year; keep only groups
                /*
                System.out.println("Matrix non Bob dimensions C " + catchNoNBoB.getDimCount());
                for(int i =0; i<catchNoNBoB.getDimCount(); i++){
                    System.out.println("Matrix non Bob dimensions nonbob C" + catchNoNBoB.getDim(i));  
                    System.out.println("Matrix non Bob dimensions nonbob C" + catchNoNBoB.getDimensionName(i));    
                    System.out.println("Matrix non Bob dimensions nonbob C" + catchNoNBoB.getSemantic(i));          
                }
                System.out.println("Matrix non Bob dimensions C " + catchNoNBoB);
                */
                
                //for (int i = 0; i<catchNoNBoB.getDim(1); i++) {
                //    context.setValue("totOtherCatchNumStepNONBOB_" + i + "_" + currentYear, catchNoNBoB.getValue(0,i));
                //    System.out.println("totOtherCatchNumStepNONBOB_" + i + "_" + currentYear + " ; " + catchNoNBoB.getValue(0,i));
                //}
            //}
            //if(currentYear>6 & step.getMonth().equals(Month.DECEMBER)){
                //MatrixND catchNoNBoB=matrixCatchCeltic.copy();
                //catchNoNBoB=catchNoNBoB.add(matrixCatchNorth.reduce()).reduce().sumOverDim(0); // Sum over the year; keep only groups
                //for (int i = 0; i<catchNoNBoB.getDim(1); i++) {
                //    context.setValue("totOtherCatchNumStepNONBOB_" + i + "_" + currentYear, catchNoNBoB.getValue(0,i));
                //    System.out.println("totOtherCatchNumStepNONBOB_" + i + "_" + currentYear + " ; " + catchNoNBoB.getValue(0,i));
                //}
            //}
            //System.out.println("DEBUG MATRIX CATCH NON BOB " + catchNoNBoB.getDimCount());

            //for(int i =0; i<catchNoNBoB.getDimCount(); i++){
            //    System.out.println("DEBUG MATRIX CATCH NON BOB ; "+ i + " ; " + catchNoNBoB.getDim(i));
            //    System.out.println("DEBUG MATRIX CATCH NON BOB ; "+ i + " ; " + catchNoNBoB.getDimensionName(i));
            //    System.out.println("DEBUG MATRIX CATCH NON BOB ; "+ i + " ; " + catchNoNBoB.getSemantic(i));
            //}
            
            /*
             * Exports landings and discards matrix in export directory, with year number. Export in NUMBERS.
             */
            String stringToExport = "";
            int year = 2010 + currentYear;
            for (int i =0; i< groups.size(); i++) {
                    double valDebLGCS;
                    double valDebTFCSE;
                    double valDebTFCSW;
                    double valDebOTH;
                    double valRejTFCSW;
                    double valRejOTH;
                if(year<2017){
                    valDebLGCS = LGCSlandings.getValue(step.getStep(),i);
                    valDebTFCSE = TFCSElandings.getValue(step.getStep(),i);
                    valDebTFCSW = TFCSWlandings.getValue(step.getStep(),i);
                    valDebOTH = OTHERSlandings.getValue(step.getStep(),i);
                    valRejTFCSW = TFCSWdiscards.getValue(step.getStep(),i);
                    valRejOTH = OTHERSdiscards.getValue(step.getStep(),i);
                }else{
                    int monthNumber = step.getStep() - 12*(step.getStep()/12); /// It works because we are working with INTEGERS
                    valDebLGCS = LGCSlandings.getValue(monthNumber,i);
                    valDebTFCSE = TFCSElandings.getValue(monthNumber,i);
                    valDebTFCSW = TFCSWlandings.getValue(monthNumber,i);
                    valDebOTH = OTHERSlandings.getValue(monthNumber,i);
                    valRejTFCSW = TFCSWdiscards.getValue(monthNumber,i);
                    valRejOTH = OTHERSdiscards.getValue(monthNumber,i);
                }
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "LONGLINEGILLNET_CS" + ";"+ "2" + ";" + valDebLGCS + "\n";
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "TRAWL_FISH_CS_E" + ";"+ "2" + ";" + valDebTFCSE + "\n";
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "TRAWL_FISH_CS_W" + ";"+ "2" + ";" + valDebTFCSW + "\n";
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "OTHERS" + ";"+ "2" + ";" + valDebOTH + "\n";
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "TRAWL_FISH_CS_W" + ";"+ "1" + ";" + valRejTFCSW + "\n";
                stringToExport+= step.getStep() + ";" + year + ";" + i + ";" + "OTHERS" + ";"+ "1" + ";" + valRejOTH + "\n";
            }
            FileUtils.writeStringToFile(NonBoBExport,stringToExport,true);

            //Seasonal matrix for some parts. : values are 3 times to high, divide everything by 3.

            affectation = true;
        }
        //End of postAction
    }
        //Méthode main + évaluer = exécution du script, pratique pour éval rapide de variables 
    //static public void main (String...args) throws Exception {
        //int monthNumber = 27 - 12*(27/12);
        //System.out.println(monthNumber);
    //}
}
