/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import java.io.Writer;

import org.nuiton.math.matrix.*;
import resultinfos.*;

import fr.ifremer.isisfish.util.Doc;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.types.TimeStep;

/**
 * Biomasses.java
 *
 * Created: 23 novembre 2006
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision:  $
 *
 * Last update: $Date: 28/08/2012$
 * by : $Author:lgasche $
 */
public class BiomasseFeconde implements ExportStep {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(BiomasseFeconde.class);

    protected String [] necessaryResult = {
            MatrixBiomass.NAME,
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "BiomasseFeconde";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Exporte les biomasses fecondes, tableau avec des lignes pop;id;zone;date;nombre";
    }
    
    /**
     * Appele au début de l'export
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    public void exportBegin(SimulationStorage simulation, Writer out)
            throws Exception {
                
        // if you need to do something at the begin of export, put your code here
    }
    
    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(step, pop, MatrixBiomass.NAME);
            for (MatrixIterator i=mat.iterator(); i.hasNext();) {
                i.next();
                Object [] sems = i.getSemanticsCoordinates();
                PopulationGroup group = (PopulationGroup)sems[0];
                Zone zone = (Zone)sems[1];
                
                double val = i.getValue()* group.getMaturityOgive();
                out.write(pop.getName() +";"+ group.getId() +";"+ zone.getName() +";"+ step.getStep() +";"+ val +"\n");
            }
        }
    }

     /**
     * Appele a la fin de l'export
     *
     * @param simulation la simulation dont on souhaite exporter les resultats
     * @param out la sortie sur lequel il faut ecrire l'export
     * @throws Exception if export fail
     */
    public void exportEnd(SimulationStorage simulation, Writer out)
            throws Exception {
        // if you need to do something at the end of export, put your code here
    }
}
