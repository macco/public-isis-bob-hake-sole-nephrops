/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2016 Ifremer, CodeLutin, Chatellier Eric
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.nuiton.math.matrix.*;
import org.nuiton.math.matrix.MatrixFactory;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.RecruitmentInput;
import fr.ifremer.isisfish.types.RecruitmentInputMap;
import fr.ifremer.isisfish.types.ReproductionData;
import resultinfos.MatrixRecruitmentPerZone;

/**
 * Abundances.java
 */
public class RecruitmentSpatial implements ExportStep {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(RecruitmentSpatial.class);

    protected String[] necessaryResult = {
        //Abundance : get abundances (then recruitments) at the beginning of the time step, just after recuitment.
        MatrixRecruitmentPerZone.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "SpatialRecruitment";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Spatial recruitment before any process applies on the fishery, at the beginning of the month. Columns : step;po;zone;number.";
    }

    @Override
    public void exportBegin(SimulationStorage simulation, Writer out) throws Exception {
        out.write("population;step;zone;value\n");
    }
    
    @Override
    public void export(SimulationStorage simulation, TimeStep step, Writer out) throws Exception {
        for (Population pop : simulation.getParameter().getPopulations()) {
            MatrixND mat = simulation.getResultStorage().getMatrix(step, pop, MatrixRecruitmentPerZone.NAME);
            for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                Zone zone = (Zone) sems[1];
                double val = i.getValue();
                out.write(pop.getName() + ";" + step.getStep() + ";" + zone.getName() + ";" + val + "\n");
            }
        }
    }

    @Override
    public void exportEnd(SimulationStorage simulation, Writer out) throws Exception {

    }
}
