/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package exports;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;

import org.nuiton.math.matrix.*;

import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.export.ExportStep;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.datastore.SimulationStorage;

import resultinfos.*;

/**
 * FishingMortality.java
 * 
 * Created: 19 Avril 2012
 * 
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 0.1 $
 * 
 * Last update: $Date: 0000-00-00 00:00:00 $ by : $Author: $
 */
public class MortalitePecheTotale implements ExportStep {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(MortalitePecheTotale.class);

    protected String[] necessaryResult = {  	
        MatrixTotalFishingMortality.NAME
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    @Override
    public String getExportFilename() {
        return "MortalitePecheTotale";
    }

    @Override
    public String getExtensionFilename() {
        return ".csv";
    }

    @Override
    public String getDescription() {
        return "Exporte la mortalite par peche totale de la simulation par population (Fpop par an calcule en decembre)";
    }
    
    @Override
    public void exportBegin(SimulationStorage simulation, Writer out) throws Exception {
        out.write("population;step;value\n");
    }
    
    @Override
    public void export(SimulationStorage simulation,TimeStep step, Writer out) throws Exception {
        if (step.getMonth()== Month.DECEMBER){ // F is calculated for a whole year in december.
            TimeStep lastStep = simulation.getResultStorage().getLastStep();
            for (Population pop : simulation.getParameter().getPopulations()){
    			MatrixND mat = simulation.getResultStorage().getMatrix(step,pop, MatrixTotalFishingMortality.NAME);
    			if (mat != null) { // can be null if simulation is stopped before last year simulation
                    for (MatrixIterator i = mat.iterator(); i.hasNext();) {
                          i.next();
                          double val = i.getValue();          
                          out.write(pop.getName() + ";" + step.getStep() + ";" + val + "\n");
    				}
    			}
    		}
    	}
    }

   @Override
    public void exportEnd(SimulationStorage simulation, Writer out) throws Exception {
    }
}
	
 
