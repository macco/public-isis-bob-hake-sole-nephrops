/*
 * Copyright (C) 2017 avigier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package simulationplans;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.Writer;
import java.io.File;
import java.io.FileReader;
import java.util.Arrays;
import static java.util.Arrays.asList;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.nuiton.math.matrix.*;
import org.nuiton.util.*;
import org.nuiton.topia.*;
import resultinfos.*;

import fr.ifremer.isisfish.util.Doc;
import fr.ifremer.isisfish.*;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.simulator.SimulationPlan;
import fr.ifremer.isisfish.simulator.SimulationPlanContext;
import fr.ifremer.isisfish.simulator.SimulationPlanIndependent;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.simulator.ResultManager;

/**
 * LHSTargetFactorsSeasons.java
 */
public class ElasticityAccessibility4MetiersMetSeasSplit implements SimulationPlanIndependent {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(ElasticityAccessibility4MetiersMetSeasSplit.class);
    static private final String MATRIX = "matrix";
    private MatrixND matrix = null;
    
    @Doc("Population which parameters are calibrated")
    public Population param_Population = null;
    @Doc("File containing weightings on weight data")
    public String param_weightingsweightFile = "input/Weightingsweight.csv";    
    @Doc("File containing weightings on length composition data")
    public String param_weightingsLFDFile = "input/WeightingsLFD.csv";
    int param_parameterNumber = 20;
    @Doc("GA parameter: the alpha parameter in the objective function. The higher alpha, the higher the weight accorded to length compositions compared to total weight.")
    public double param_alphaOF= 10;
    public int param_first = 0;
    public int param_simulationNumber = 51;
    public String param_directory = "input/ElasticityPlanSeasonMetCorrNOSPLGIter3SPLIT.csv";
    public String param_exportPath = "output/ExportElasticityAccessibility4MetiersMetSeasSplit.csv";
    public String param_observationFile = "input/CatchWeightPerLengthSemantics.csv";
    protected String exportHisto = "";
    protected File observationsFile;
    protected MatrixND matrixObservations;
    protected MatrixND matrixWeightingsweight;
    protected MatrixND matrixWeightingsLFD;
    protected File exportHistoric;
    protected File weightingsweightFile;
    protected File weightingsLFDFile;
    int compteurSimus;
    
    protected String[] necessaryResult = {
        // put here all necessary result for this rule
        // example:
        // MatrixBiomass.NAME,
        // MatrixNetValueOfLandingsPerStrategyMet.NAME,
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur le plan.
     * @return L''aide ou la description du plan
     */
    @Override
    public String getDescription() throws Exception {
        // TODO change description
        return "Elastictity on 20 metiers*seasons target factors. Does not compute objective fiunction.";
    }

    /**
     * Appel au demarrage de la simulation, cette methode permet d'initialiser
     * des valeurs
     * @param simulation La simulation pour lequel on utilise cette regle
     */
    public void init(SimulationPlanContext context) throws Exception {
        //Get parameter values computed by LHS
        exportHistoric = new File(param_exportPath);
        File dir = new File(param_directory);
        matrix = MatrixFactory.getInstance().create(new int[]{param_simulationNumber, param_parameterNumber});
        matrix.importCSV(new FileReader(dir), new int[]{0,0});
     }

    /**
     * Call before each simulation, changes database.
     * 
     * @param context plan context
     * @param nextSimulation storage used for next simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    @Override
    public boolean beforeSimulation(SimulationPlanContext context,SimulationStorage nextSimulation) throws Exception {
        int simNum = nextSimulation.getParameter().getSimulationPlanNumber();
        if (simNum + param_first < param_simulationNumber) {
            int counter = 0;
            
            // Get simulationContext and alter it. DO NOT use ToPIA contexts, to much risks of locking the database for nothing.
            // Use a pre-script, that will be written by the optimization script, to change the database without risk.
            SimulationParameter params = nextSimulation.getParameter();
            
            //Change target factors values for each metier. Parameters are always stocked in the same order : first all the métiers, in metierList order, then the 4 seasons.
            String script = "";
            script = script + "context.setComputeValue(\"Tarf.LBOBs1\", "+matrix.getValue(simNum,0)+");\n";
            script = script + "context.setComputeValue(\"Tarf.LBOBs2\", "+matrix.getValue(simNum,1)+");\n";
            script = script + "context.setComputeValue(\"Tarf.LBOBs3\", "+matrix.getValue(simNum,2)+");\n";
            script = script + "context.setComputeValue(\"Tarf.LBOBs4\", "+matrix.getValue(simNum,3)+");\n";
            script = script + "context.setComputeValue(\"Tarf.GBOBs1\", "+matrix.getValue(simNum,4)+");\n";
            script = script + "context.setComputeValue(\"Tarf.GBOBs2\", "+matrix.getValue(simNum,5)+");\n";
            script = script + "context.setComputeValue(\"Tarf.GBOBs3\", "+matrix.getValue(simNum,6)+");\n";
            script = script + "context.setComputeValue(\"Tarf.GBOBs4\", "+matrix.getValue(simNum,7)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBEs1\", "+matrix.getValue(simNum,8)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBEs2\", "+matrix.getValue(simNum,9)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBEs3\", "+matrix.getValue(simNum,10)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBEs4\", "+matrix.getValue(simNum,11)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBWs1\", "+matrix.getValue(simNum,12)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBWs2\", "+matrix.getValue(simNum,13)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBWs3\", "+matrix.getValue(simNum,14)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TFBWs4\", "+matrix.getValue(simNum,15)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TNEPs1\", "+matrix.getValue(simNum,16)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TNEPs2\", "+matrix.getValue(simNum,17)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TNEPs3\", "+matrix.getValue(simNum,18)+");\n";
            script = script + "context.setComputeValue(\"Tarf.TNEPs4\", "+matrix.getValue(simNum,19)+");\n";
    
            if (!params.getUsePreScript()) {
                params.setUsePreScript(true);
            }
    
            params.setPreScript(script);
            nextSimulation.setParameter(params); // on force la sauvegarde            
             return true;
        } else {
            return false;
        }     
    }

    /**
     * Call after each simulation.
     * 
     * @param context plan context
     * @param lastSimulation storage used for simulation
     * @return true if we must do next simulation, false to stop plan
     * @throws Exception
     */
    @Override
    public boolean afterSimulation(SimulationPlanContext context,SimulationStorage lastSimulation) throws Exception {
        return true;
    }
}
