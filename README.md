# ISIS_BoB_Hake_Sole_Nephrops

This repository contains files to run ISIS-Fish simulations for Bay of Biscay hake-sole-Nephrops application.

It is always highly recommended to work with the latest ISIS-Fish releas (V 4.4.6.0 at the moment of this commit)

Should any issue arise about the files, the assumptions, etc., please contact me: audric.vigier@mailo.com

You can also find technical support in English and French on the users mailing list, as long as you describe properly your problem (providing your debug file is also a good practice): isis-fish-users@list.isis-fish.org

# Requirements

__ You NEED to install ISIS-Fish __ on your computer in order to use these files. See: http://isis-fish.org/v4/user/installation.html and http://isis-fish.org/download.html . If this is your first ISIS-Fish installation, unzip the downloaded folder, and run ISIS-Fish (launch isisfish.bat or isisfish.sh in the unzipped folder). You can check under the Configuration>Configuration tab where isis-fish-4 folder is stored.

New ISIS-Fish users are invited to browse the online documentation: http://isis-fish.org/v4/index.html . 

# Repository contents

- GdGMerluMSE_26Oct2018_1cm2cm10cm_ChangeMHake_Sole1Zone.zip : region. You may need to load it in V4.4.6.0 to be able to use it, despite it being already there by default. You can load a database .zip into ISIS-Fish using the user interface. To do so, under the book panel, click Region>Import a region and select the .zip file. it should now appear in the drop-down list.

- input repertory: containts essential input files for the base, simulations and rules.

- exports repertory: contains a repertory with simulated catch outside of the Bay of Biscay. It is exported in an external .csv file (instead of added to ISIS-Fish catch matrix) for historical reasons (coupling with another model, which has been abandoned). Feel free to change this if you feel like it!

- output repertory: contains a repertory with simulated catch for Spanish gillnetters and longliners of the Bay of Biscay. It should be unzipped to be read. It is exported in external .csv files (instead of added to ISIS-Fish catch matrix) for historical reasons (coupling with another model, which has been abandoned). Feel free to change this if you feel like it!

- .bat and .jar files: these are the .bat and .jar files used with our ISIS-Fish installation.

__ input, output and exports repertories HAVE to be in the same reportory as the .bat file __

- simu (ran as "sim_calibReduceEM05_step2iter3_sole3_nep0_sole0_10years_varsAnn_CPUE1_SOLE1zone_q1_met1_TACHISTOCorrTarfCorr_Corr_2021-03-21-13-41") : this folder is the run simulation, with the management scenario being historical TALs/TACs. Copy-paste it in isis-fish-4/isis-database/simulations. It contains results, so that you can vizualize them in ISIS-Fish interface without re-running the simulation. Some of its exports are zipped, and need be unzipped to be read externally. The catch/landings/discards files DO NOT contain information on Spanish longliners and gillnetters and on catch outside of the Bay of Biscay (see exports and output repertories).

- simuOLD : the same simulation, but compatible with V4.4.2.2 (don't use if you work with latest version)

- scripts repertory: contains many scripts necessary to run the simulation. Some of them have hard-coded path file sin them that will need to be changed (rules). O[...]_OLD has deprecated assumptions but compiles with V4.4.5.1

- scriptsOLD : the same scripts but compatible with V4.4.2.2, does not compile with V4.4.5.1  (don't use if you work with latest version)

Contents of scripts repertory:

- 3 rules to be copy-pasted in isis-fish-4/isis-community-database/rules : CapturesLongLinersnetters_Peojection_Ratio_PropSPDEF.java (Spanish longliners and gillnetters Bay of Biscay catch), OgiveDeTriMerluRetentionTACAllTransitionMSY_reduceE_varsAnn_catchCorrTACHISTO_Corr.java (management rules), NonBayOfBiscayCatchDEF.java (out of Bay of Biscay catch). They have to be called in that order in the ISIS interface (trick: remember the first letter of each script). These scripts, especially the O[...] script, contain a lot of stuff do not use anymore, that were related to (frequently changing) assumptions not anymore used. Feel free to clear the useless stuff or to improve the coding! The latter scripts contains corrections compared to previous version.  

- 3 exports to be copy-pasted in isis-fish-4/isis-community-database/exports : AbundanceBeginMonth.java, LandingsPoids.java and RecruitmentSpatial.java . They allow to export more simulation outputs in .csv files.

- 2 simulators to be copy-pasted in isis-fish-4/isis-community-database/simulators : Default[...].java. They are needed to access results before the conditions and pre-actions, which some rules need in order to run.

- 1 script to be copy-pasted in isis-fish-4/isis-community-database/scripts : ExportRecruSpat.java, to be able to export spatial recruitment simulated values.

- 1 script to be copy-pasted in isis-fish-4/isis-community-database/resultinfos : MatrixRecruitmentPerZone.java, to be able to export spatial recruitment simulated values.

- 1 pre-simulation script to copy-paste in the user interface: presimu.txt

- 1 technical documentation : companion_document.pdf

Contents of modelSKillAssessment repertory:

A series of .R scripts, .Rdata and .png files to process and display some ISSI-Fish outputs. The .Rdata are to be opened with R version >=3.6 in order to display properly the ggplots.

The derivation of catch observations (catchData.Rdata), and a fine-scale SACROIS effort file (SACROIScc2010.Rdata, replaced here by an aggregated version SACROISobsShareable.Rdata) are not shared on this public repository, as we lack the permissions. this however does not prevent from running the scripts. Please contact us for further requests about these files.

Reproduce data paper plots : catchSoleMetierMonth.png with observationsSole/readSoleObs.R ; landingsMetierQuarter.R with observationsLangoustine/readNepObs.R ; plotFitDataPaper.png and plotMapDataPaper.png with plots4paper.R.

# Reproduce the run and plots

Run the simulation:

- 1/ Launch ISIS-FIsh, using isisfish.bat or isisfish.sh

- 2/ Load the database (GdGMerluMSE_26Oct2018_1cm2cm10cm_ChangeMHake_Sole1Zone.zip) using the use interface (book panel, click Region>Import a region and select the .zip file). This is not needed if you already loaded the database previously.

- 3/ Copy-paste all the folders and scripts as indicated above. Once this is done, turn off and on ISIS-Fish, so that scripts and simulations can be accessed form the user interface.

- 4/ Under the Configuration panel (2nd tab on the left), load the simulation ("Load a former simulation"), and choose simu. You should be able to see initial numbers under the Parameters tabs, the 3 rules, already loaded, under the Rules tab. Under pre-simulation script tab, you need to tick the box and to copy-paste the contents of the pre-simulation script. To avoid future confusion, under Parameters tab, change the simulation name (call it myrun for instance).

- 5/ Under Parameters tab, at the botton of the window, click "Simulate" to run the simulation. This should take you to the Simulation panel (fish icon), where you can monitor the progress of the simulation. This may take a long time to run (25 minutes on a recent computer (laptop with Intel®Core(TM)i5-8300H CPU (2 x 2.30 GHz), and 16.0 GB RAM; OS Windows 10 Home 64 bit)). When the simulation ends, it should be moved to the bottom list in the Simulation panel. You may close ISIS-Fish.

Reproduce the data paper plots:

- 1/ Run script conditioning_toShare.R. You'll need to change some directories (workingPath, where .Rdata will be saved; ISISsimulation should match your new simulation name; ISISpath should match the location of isis-fish-4/isis-database/simulations/ (or of the simulation folder), ISISbatPath the directory containing the .bat file (or the directory containing exports and output directories).

- 2/ Appendix plots about sole catch and Norway lobster landings: execute observationsSole/readSoleObs.R and execute observationsLangoustine/readNepObs.R (after changing the path names; outputs catchSoleMetierMonth.png and landingsMetierQuarter.png). Main text plots: execute plots4Paper.R (after changing the path names; outputs plotFitDataPaper.png and plotMapDataPaper.png). A lot of other plots are also produced.

# Previous commits

18/09/2020: "Correction: correction on the script rule O[...] (there was a pb. with sole, also correction on de minimis exemption)"

11/02/2021: Update to work with V 4.4.5.1. Files for V4.4.2.5 are still there, just in case. latestCorrections and master branches are the same, and what was the master branch is now base_OLD

02/08/2021: Updated with latest base simulation, which differs in one assumption regarding effort re-adjustement compared to previous base simulation. Files should be compatible with V 4.4.5.1. Only master branch is up to date.

22/10/2021: Reproduced simulation and plots with the instructions on the Git using R 3.6.0 and ISIS-Fish 4.4.6.0.

12/02/2022: Preparing the repo for publication
