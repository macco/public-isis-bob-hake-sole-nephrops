/*
 * Copyright (C) 2017 avigier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 */
package simulators;

import fr.ifremer.isisfish.IsisFishException;
import fr.ifremer.isisfish.datastore.StorageException;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationSeasonInfo;
import fr.ifremer.isisfish.rule.Rule;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.simulator.RuleMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.SimulationControl;
import fr.ifremer.isisfish.simulator.SimulationParameter;
import fr.ifremer.isisfish.simulator.Simulator;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.types.TimeStep;
import resultinfos.*;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.topia.TopiaContext;
import org.nuiton.topia.TopiaException;
import scripts.ExportRecruSpat;
import scripts.GravityModel;
import scripts.SiMatrix;

import java.util.List;

import static org.nuiton.i18n.I18n.t;
import static org.nuiton.i18n.I18n.n;

/**
 * DefaultSimulatorRecruitmentPerZone.java
 */
public class DefaultSimulatorRecruitmentPerZoneResultBeforeCondition extends DefaultSimulatorResultBeforeCondition {

    /** to use log facility, just put in your code: log.info("..."); */
    private static Log log = LogFactory.getLog(DefaultSimulatorRecruitmentPerZoneResultBeforeCondition.class);
    /**
     * When you create new simulator with specific SiMatrix you must
     * overwrite this method to return your SiMatrix version
     * @param context
     * @return
     * @throws TopiaException 
     */
    protected SiMatrix newSiMatrix(SimulationContext context) throws TopiaException {
        //System.out.println("Nouveau simulateur!!!!");
        return new ExportRecruSpat(context);
    }
   
}
