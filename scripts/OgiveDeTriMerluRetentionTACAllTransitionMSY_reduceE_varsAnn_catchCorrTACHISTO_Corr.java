package rules;

import java.util.ArrayList;
import java.io.*;
import java.io.File;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.Vector;
import static java.nio.charset.StandardCharsets.UTF_8;					  
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.j2r.*;
import org.nuiton.j2r.jni.*;
import org.nuiton.j2r.net.*;
import org.nuiton.j2r.REngine;
import org.nuiton.j2r.RProxy;
import org.nuiton.j2r.types.*;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixND;
import org.nuiton.math.matrix.*;
import resultinfos.*;
import scripts.RuleUtil;
import scripts.MinimisationUtil;
import scripts.ObjectiveFunction;
import scripts.ObjectiveFunctionBaranov;
import scripts.RuleUtil;
import scripts.SiMatrix;
import scripts.SiMatrix;

import fr.ifremer.isisfish.datastore.SimulationStorage;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.entities.*;
import fr.ifremer.isisfish.rule.AbstractRule;
import scripts.SiMatrix;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.result.ResultInfo;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.simulator.ResultManager;
import fr.ifremer.isisfish.util.Doc;


/**
 * Cette methode remplace aussi TailleMin pour le Merlu dans PecherieGdG
 * inspire de TailleMIN
 * Created: 30 novembre 2006
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 1.1 $
 *
 * Last update: $Date: 2012 08_28 $
 * by : $Stephanie Mahevas$
 */
public class OgiveDeTriMerluRetentionTACAllTransitionMSY_reduceE_varsAnn_catchCorrTACHISTO_Corr extends AbstractRule {

    /** to use log facility, just put in your code: System.out.println("..."); */
    static private Log log = LogFactory.getLog(OgiveDeTriMerluRetentionTACAllTransitionMSY_reduceE_varsAnn_catchCorrTACHISTO_Corr.class);

    @Doc(value = "Begin step")
    public TimeStep param_beginStep = new TimeStep(0);

    @Doc(value = "End step")
    public TimeStep param_endStep = new TimeStep(132);
    
    @Doc(value = "First year (first year modelled in ISIS-Fish)")
    public int param_firstYear = 2010;
    public int param_alpha = 2015;
    public int param_beta = 2020;
    public int param_pi = 5;
    
    @Doc(value = "Affected species")
    public Species param_species = null;
    public Species param_species2 = null; //Sole
    public Species param_species3 = null; //Nep
    
    @Doc("MSY Btrigger")
    public double param_msyBtrigger = 45000; // Tons
    public double param_msyBtrigger2 = 10600; // Tons
    public double param_msyBtrigger3 = 0; // Tons
    
    @Doc("F_MSY")
    public double param_FMSY = 0.437; // Value fixed by WKMSYREF4, not the one used by WGBIE.
    public double param_FMSY2 = 0.584;
    public double param_FMSY3 = 0.078;
    
    @Doc("F_pa")
    public double param_Fpa = 0.62;
    public double param_Fpa2 = 0.43;
    public double param_Fpa3 = 0;

    @Doc(value = "param_TailleMin")
    public double param_TailleMin = 27;
    double param_TailleMinLang = 26;
    
    public String param_nomfichier_output = "output/catchSpanishLGWithRule";
    public String param_nomfichier_output_num = "output/catchSpanishLGNumWithRule";
    public String param_nomfichier_TAC = "output/TACvalues";
    public String param_nomfichier_TraceTarfs = "output/TraceTarfs";
    protected double TACvalue; // A TAC value to be calculated in the rule. To be used in condition, preAction and postAction.
    protected double TACBOBvalue; // A TAC value to be calculated in the rule. To be used in condition, preAction and postAction.
    protected double TACvalueSole; // A TAC value to be calculated in the rule. To be used in condition, preAction and postAction.
    protected double TACvalueLang; // A TAC value to be calculated in the rule. To be used in condition, preAction and postAction.
    
    protected File  outputCatch;
    protected File  outputCatchNum;
    protected File  outputTACvalues;
    protected File  TraceTarfs;
    protected String outputSpanishCatch;
    protected String outputSpanishCatchNum;
    
    boolean affectation;
    boolean affectationPreAction;
    boolean flagLGEsp; // Flag to work on spanish longliners and gillnetters
    boolean doTAC = false ; // Switch to do TAC or not
    boolean doTACHake = false ; // Switch to do TAC or not
    boolean doTACSole = false ; // Switch to do TAC or not
    boolean doTACNep = false ; // Switch to do TAC or not
    boolean firstIteration = true;
    boolean [] doLO = {false,false,false,false,false,false,true,true,true,true,true}; // Switch to do landings obligation or not
    double [] exemptions = {0,0,0,0,0,0,0.07,0.07,0.06,0.06,0.05}; // de minimis exemptions. Assumption : in 2018-2020, de minimis exemptions are the same
    double [] exemptionsSoleTRAWL = {0,0,0,0,0,0,0.05,0.05,0.05,0.05,0.05}; // de minimis exemptions. Assumption : in 2018-2020, de minimis exemptions are the same
    double [] exemptionsSoleGILLNET = {0,0,0,0,0,0,0.03,0.03,0.03,0.03,0.03}; // de minimis exemptions. Assumption : in 2018-2020, de minimis exemptions are the same
    //double [] TAC = {999999999.0,20609.0,20609.0,25970.0,30610.0,33977.0,40393.0,44808.0}; // TACs for the 2010-2017 period in tons
    double [] TAC = {999999999.0,999999999.0,999999999.0,69440.0,81846.0,90849.0,108784.0,119765.0,111785.0,141160.0,131664.0}; // TACs for the 2010-2017 period in tons
    double [] TACBOB = {999999999.0,999999999.0,999999999.0,25970.0,30610.0,33977.0,40393.0,44808.0,42460.0,52118.0,50441.0};
    //double [] TAC = {999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0}; // TACs for the 2010-2017 period in tons
    //double [] TACBOB = {999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0,999999999.0};
    double [] TACSole = {999999999.0,999999999.0,999999999.0,4100.0,3800.0,3800.0,3420.0,3420.0,3621.0,3872.0,3666.0};
    double [] TAClang = {999999999.0,999999999.0,999999999.0,3899.0,3899.0,3899.0,3899.0,4160.0,3614.0,3878.0,3886.0};
    File metierFlags = new File("output/flagChecks.csv");//For debugging only, do not use
    File strMetBefore = new File("output/StrMetBeforeChecks.csv");//For debugging only, do not use
    File strMetAfter = new File("output/StrMetAfterChecks.csv");//For debugging only, do not use
    double propTACHake = 1;
    double propTACSole = 1;
    double propTACNep = 1;
    
    //Selectivity parameters estimated in the spatial SS3 (Vigier et al., in press)
    double P1_NEP = 29.8012;
    double P2_NEP = 2.11801;
    double P1_TFBW = 28.3948;
    double P2_TFBW = 1.21606;
    
    public String[] necessaryResult = {
        // put here all necessary result for this rule
        // example: 
        // ResultName.MATRIX_BIOMASS,
        // ResultName.MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET,
        MatrixCatchWeightPerStrategyMetPerZonePop.NAME,
        MatrixDiscardsWeightPerStrMetPerZonePop.NAME ,
        MatrixCatchPerStrategyMetPerZonePop.NAME,
        MatrixCatchWeightPerStrategyMetPerZonePop.NAME,
        MatrixDiscardsPerStrMetPerZonePop.NAME,
        MatrixEffortPerStrategyMet.NAME,
        MatrixAbundanceBeginMonth.NAME,
        MatrixAbundance.NAME,
        MatrixTotalFishingMortality.NAME,
        MatrixBiomass.NAME,
        MatrixFishingMortalityPerGroup.NAME,
        MatrixNoActivity.NAME,
    };

    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * 
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() throws Exception {
        return "Contient les TACs pour la période 2010-2017 pêcherie merlu gdG,\nles rejets induits par ces TACs, et les courbes de rétentions\ndéfinissant les profils en taille des rejets.\nImplémentation de l'obligation de débarquement. Contient le couplage avec SS3 pour 2018-2020. Règle non générique.";
    }

    /**
      * des valeurs
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
        //System.out.println("init regle ogive merlu");
        SimulationStorage storage = context.getSimulationStorage();
        int genNumber = storage.getParameter().getOptimizationGeneration();
        int indivNumber = storage.getParameter().getOptimizationGenerationIndividual();
        outputCatch = new File (param_nomfichier_output +"_"+genNumber+"_"+indivNumber+".csv");
        outputCatchNum = new File (param_nomfichier_output_num +"_"+genNumber+"_"+indivNumber+".csv");
        outputTACvalues = new File (param_nomfichier_TAC +".csv");
        TraceTarfs = new File (param_nomfichier_TraceTarfs +".csv");
        affectation = true;
        flagLGEsp = true ;
        firstIteration=true;
        for(int i=param_beginStep.getStep();i<=param_endStep.getStep();i++){
            context.setValue("firstIteration_"+i,true);
        }
        
    }

   
    /**
     * La condition qui doit etre vraie pour faire les actions
     * 
     * @param context La simulation pour laquelle on utilise cette regle
     * @return vrai si on souhaite que les actions soient faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {

        //Do not loop on metier for some code chunks
        firstIteration = (boolean) context.getValue("firstIteration_"+step.getStep());
        //System.out.println("VALUE DEBUG first iter : " + firstIteration);
        
        /*            
         *             Do the rule?
         */
        boolean result = true;
        flagLGEsp = true ;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        } else if(step.getMonth().equals(Month.JANUARY)){
            // reinit priceSwitch at the beginning of each year
            if(firstIteration){
                context.setValue("priceSwitch"+param_species.getName(),0);
                doTAC = false;
                doTACHake = false;
                doTACSole = false;
                doTACNep = false;
                context.setValue("SpLG"+param_species.getName()+"flag",0); // Flag for spanish longliners and gillnetters
                context.setValue("TAC_hake_close",0);
                context.setValue("TAC_sole_close",0);
                context.setValue("TAC_nep_close",0);
                context.setValue("TAC_hake_reached",12);
                context.setValue("TAC_sole_reached",12);
                context.setValue("TAC_nep_reached",12);
                //System.out.println("VALUE DEBUG condition : " + context.getValue("SpLGMerluflag") + " step " + step.getStep());
            }
            context.setValue(metier.getName()+param_species.getName()+"flag",0);
            context.setValue(metier.getName()+"landHake",0); //Is it mandatory to land hake for this meteir?0; no ; 1, yes
        }
        affectationPreAction=true;

        /*            
         *             Conditions on TAC
         */
         // 2010-2015 : no landing obligation and no MSY transition : TAL applies to the 3 stocks
         // 2016-2020 : landing obligation and MSY transition : TAC applies to hake and sole, a TAL applies to nephrops

        // Get total catch (and discards and landintgs if necessary) in the Bay of Biscay for each species
        int year = step.getYear();          
        TargetSpecies ts = metier.getMetierSeasonInfo(step.getMonth()).getSpeciesTargetSpecies(param_species);
        TargetSpecies ts2 = metier.getMetierSeasonInfo(step.getMonth()).getSpeciesTargetSpecies(param_species2);
        TargetSpecies ts3 = metier.getMetierSeasonInfo(step.getMonth()).getSpeciesTargetSpecies(param_species3);
        double catchTons = RuleUtil.getTotalCatchTons(context, param_species, step);
        double catchTonsSole = RuleUtil.getTotalCatchTons(context, param_species2, step);
        double catchTonsNep = RuleUtil.getTotalCatchTons(context, param_species3, step);
        double discTons=0;
        double discTonsSole=0;
        double discTonsLang=0;
        double otherCatchTons = 0;  //Longliners and gillnetters Spain
        double otherDiscTons = 0;  //Longliners and gillnetters Spain
        double landTons=0;
        double landTonsSole=0;
        double landTonsLang=0;
        
        //Catch in tons of hake (spanish longliners and gillnetters in the Bay of Biscay);
        if(!step.getMonth().equals(Month.JANUARY)){
            for(int stepNum = 12*year; stepNum<step.getStep(); stepNum++){
                for (int lengthGroupId =0; lengthGroupId<72 ; lengthGroupId++) {
                    otherCatchTons = otherCatchTons + (double) context.getValue("totOtherCatchStep_" + lengthGroupId + "_" + stepNum);
                }
            }
        }

        //System.out.println("Other catch tons step : " + step.getStep() + " ; "+ otherCatchTons);
        //System.out.println("catch tons step : " + step.getStep() + " ; "+catchTons);
        //Total catch of hake
        catchTons = catchTons + otherCatchTons;
        //System.out.println("sum catch tons step : " + step.getStep() + " ; "+ catchTons);

        //if(year<6){ // COMPUTE FOR EACH YEAR, EVEN AFTER 2015, GIVEN THAT NEP DOES NOT HAVE LO.
            //Get discards in the Bay of Biscay for each species
            // Before 2016, a TAL applies instead of a TAC, since landing obligation is not implemented. Hence, work on landings (catch-discards)
            //List current year time step
           if(!step.getMonth().equals(Month.JANUARY)){
                List stepsDisc = new ArrayList();
                TimeStep nextStepDisc = step.previous();
                stepsDisc.add(nextStepDisc);
                
                while (nextStepDisc.previous().getYear()==year){ // Tant qu'un pas de temps précédent fait partie de la meme annee, le rajouter
                    nextStepDisc = nextStepDisc.previous();
                    stepsDisc.add(nextStepDisc);
                }
                SimulationStorage simulationForDiscards = context.getSimulationStorage();
                Population pop = param_species.getPopulation().iterator().next(); // This must contain only one population : hake.      
                MatrixND matRejets =  simulationForDiscards.getResultStorage().getMatrix(stepsDisc, pop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME);
                if(matRejets!=null){
                    MatrixND matTotalRejets = matRejets.copy();
                    discTons = matTotalRejets.sumAll()/1000; // from kg to tons!
                }
    
                Population pop2 = param_species2.getPopulation().iterator().next(); // This must contain only one population : sole.      
                MatrixND matRejetsSole =  simulationForDiscards.getResultStorage().getMatrix(stepsDisc, pop2, MatrixDiscardsWeightPerStrMetPerZonePop.NAME);
                if(matRejetsSole!=null){
                    MatrixND matTotalRejetsSole = matRejetsSole.copy();
                    discTonsSole = matTotalRejetsSole.sumAll()/1000; // from kg to tons!
                }
    
                Population pop3 = param_species3.getPopulation().iterator().next(); // This must contain only one population : Nephrops.      
                MatrixND matRejetsLang =  simulationForDiscards.getResultStorage().getMatrix(stepsDisc, pop3, MatrixDiscardsWeightPerStrMetPerZonePop.NAME);
                if(matRejetsLang!=null){
                    MatrixND matTotalRejetsLang = matRejetsLang.copy();
                    discTonsLang = matTotalRejetsLang.sumAll()/1000; // from kg to tons!
                }
           }
        //}

        //Discards in tons of hake (spanish longliners and gillnetters in the Bay of Biscay);
        //System.out.println("Condition 0 step : " + step.getStep() + " metier : " + metier.getName());
        if(!step.getMonth().equals(Month.JANUARY)){
            for(int stepNum = 12*year; stepNum<step.getStep(); stepNum++){
                for (int lengthGroupId =0; lengthGroupId<72 ; lengthGroupId++) {
                    //System.out.println("Condition discard : step " + stepNum + " length : " + lengthGroupId);
                    otherDiscTons = otherDiscTons + (double) context.getValue("totOtherDiscardStep_" + lengthGroupId + "_" + stepNum);
                }
            }
        }
        
        //if(year<6){
            //Hake landings
            //System.out.println("Other disc tons step : " + step.getStep() + " ; "+ otherDiscTons);
            //System.out.println("disc tons step : " + step.getStep() + " ; "+ discTons);
            discTons = discTons + otherDiscTons;
            //System.out.println("sum disc tons step : " + step.getStep() + " ; "+ discTons);
            landTons = catchTons - discTons;
            //System.out.println("sum land tons step : " + step.getStep() + " ; "+ landTons);
            //Sole landings
            landTonsSole = catchTonsSole - discTonsSole;
            //Nephrops landings
            landTonsLang = catchTonsNep - discTonsLang;
        //}
       
        //Get the TAC for the Bay of Biscay only, for each species. Without MSY transition before 2016, with MSY transition then.
        // No MSY transition
        if((year<11)&(step.getMonth().equals(Month.JANUARY)& firstIteration)){
           //Get TAC for each species (only at the beginning of the year)
           //Hake
           TACBOBvalue=TACBOB[year];
           context.setValue("TACvalue_" + year, TAC[year]);
           context.setValue("TACBOBvalue_" + year, TACBOBvalue);

           //Sole
           TACvalueSole = TACSole[year];
           context.setValue("TACvalueSole_" + year, TACvalueSole);
           
           //Nephrops
           TACvalueLang = TAClang[year];
           context.setValue("TACvalueLang_" + year, TACvalueLang);

           //Save
           String stringTACExport = year+";hake;"+context.getValue("TACvalue_" + year)+"\n"+year+";sole;"+TACvalueSole+"\n"+year+";lang;"+TACvalueLang+"\n";
           if(year==0){
                FileUtils.writeStringToFile(outputTACvalues,"year;stock;value\n", UTF_8, false);
           }
           FileUtils.writeStringToFile(outputTACvalues,stringTACExport, UTF_8, true);
        }
        
        //MSY transition
        if((year>=11)&(step.getMonth().equals(Month.JANUARY)& firstIteration)){
            //For hake and sole, get SSB and compute F to implement. Get n-4 to n-2 F for each length class of interest, compute F to implement per group, deduce a TAC
            //For Nephrops, compute harvest rate to implement, get abundance, deduce catch in numbers and a TAC.

            List<Population> Pops = context.getPopulationMonitor().getPopulations();
            ResultStorage matResult = context.getSimulationStorage().getResultStorage();

            for(Population onePop:Pops){
                //Hake
                if (onePop.getSpecies() .getName().equals(param_species.getName())){

                    //Get SSB
                    MatrixND MatBiom = matResult.getMatrix(step, onePop, MatrixBiomass.NAME).copy();
                    //System.out.println("Import de la biomasse au tout debut du mois de janvier realise");
                    MatBiom = MatBiom.reduce();
        
                    double SSBiom = 0;
                    for (MatrixIterator i=MatBiom.iterator(); i.hasNext();) {
                        i.next();
                        Object [] sems = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup)sems[0];
                        
                        double val = i.getValue()* group.getMaturityOgive();
            
                        SSBiom = SSBiom + val;
                    }
                    SSBiom = SSBiom / 1000;
                    
                    //System.out.println("DEBUG MSY HAKE SSB " + SSBiom);
                    //System.out.println("DEBUG MSY HAKE param_msyBtrigger " + param_msyBtrigger);
                    
                                        //Compute F to implement : a/ get F at the beginning of the transition b/ compute F to implement
                    //a/
                    //System.out.println("DEBUG MSY HAKE a");
                    double F2015;
                    if(year==11){
                        List listofSteps = new ArrayList();
                        TimeStep nextStep= step.previousYear(); 
                        for(int i=1; i<=12; i++){
                            listofSteps.add(nextStep);
                            nextStep= nextStep.next();
                        }
                        MatrixND MatF2015_catch = matResult.getMatrix(listofSteps, onePop, MatrixCatchPerStrategyMetPerZonePop.NAME).copy().sumOverDim(4);// (dimension 0 step, 1 strategy, 2 metier, 3 group, 4 zone) In numbers BUT groups
                        MatF2015_catch = MatF2015_catch.sumOverDim(2);
                        MatF2015_catch = MatF2015_catch.sumOverDim(1);
                        MatF2015_catch = MatF2015_catch.sumOverDim(0);
                        MatF2015_catch = MatF2015_catch.reduce(); // (dimension 0 step, 1 strategy, 2 metier, 3 group, 4 zone) In numbers BUT groups
                        System.out.println("DEBUG MSY HAKE F2015 : " + MatF2015_catch);
                        MatrixND MatF2015 = matResult.getMatrix(step.previousYear(), onePop, MatrixAbundanceBeginMonth.NAME).copy().sumOverDim(1); // (dimension -1 step, 0 group, 1 zone)In numbers BUT groups 
                        MatF2015 = MatF2015.reduce(); // (dimension -1 step, 0 group, 1 zone)In numbers BUT groups 
                        System.out.println("DEBUG MSY NEP F2015 : " + MatF2015);

                        // And SPLG and Non BoB catch. Loop over hake groups and add the catch in numbers of all 2015
                        for (MatrixIterator i = MatF2015_catch.iterator(); i.next();) {
                            Object [] coord = i.getSemanticsCoordinates();
                            PopulationGroup lengthGroup = (PopulationGroup) coord[0];
                            int lengthGroupId = lengthGroup.getId();
                            double catchToAdd = 0;
                            for(int steps = 60; steps<=71; steps++){
                                catchToAdd = catchToAdd + (double) context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + steps);
                                System.out.println("DEBUG MSY HAKE SP catch group : " + lengthGroupId + " step " + steps + " value" + (double)context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + steps));
                                catchToAdd = catchToAdd + (double)context.getValue("totOtherCatchNumStepNONBOB_" + lengthGroupId + "_"+steps);
                                System.out.println("DEBUG MSY HAKE Nobbob catch group : " + lengthGroupId + " value" + (double)context.getValue("totOtherCatchNumStepNONBOB_" + lengthGroupId + "_"+steps));
                            }
                            
                            System.out.println("DEBUG MSY HAKE F2015 AGAIN : " + MatF2015_catch);
                            double newValue = MatF2015_catch.getValue(coord) + catchToAdd;
                            MatF2015_catch.setValue(coord,newValue);
                        }
                        System.out.println("DEBUG MSY HAKE F2015 AGAIN : " + MatF2015_catch);

                        //Get M (clean way)
                        // compute total mortality by suming naturalDeathRate and fishingMortalityOtherFleetsMatrix
                        MatrixND naturalDeathRatePop = onePop.getNaturalDeathRateMatrix();
                        naturalDeathRatePop = naturalDeathRatePop.meanOverDim(1); // moyenne sur Zone
                        naturalDeathRatePop = naturalDeathRatePop.reduce(); // Enleve les dimensions de taille 1
                        MatrixND fishingMortalityOther = onePop.getFishingMortalityOtherFleetsMatrix();
                        fishingMortalityOther = fishingMortalityOther.meanOverDim(1); // moyenne sur Zone
                        fishingMortalityOther = fishingMortalityOther.reduce(); // Enleve les dimensions de taille 1
                        System.out.println("DEBUG MSY HAKE F2015 M : " + naturalDeathRatePop);
                        System.out.println("DEBUG MSY HAKE F2015 M : " + fishingMortalityOther);
                        
                        //Get F
                        for (MatrixIterator i = MatF2015.iterator(); i.next();) {
                            Object [] coord = i.getSemanticsCoordinates();
                            PopulationGroup lengthGroup = (PopulationGroup) coord[0];
                            double Mgroup = naturalDeathRatePop.getValue(lengthGroup) + fishingMortalityOther.getValue(lengthGroup);
                            int lengthGroupId = lengthGroup.getId();
                            double Cgroup = MatF2015_catch.getValue(coord);
                            double NgroupRef = MatF2015.getValue(coord);
                            ObjectiveFunction f = new ObjectiveFunctionBaranov(Cgroup, Mgroup, NgroupRef);
                            double F2015val = MinimisationUtil.fmin(0.0, 2.0, 1.0e-10, f); // step ??
                            System.out.println("DEBUG F COMPUTATION F2015 : " + F2015val + " Catch num " + Cgroup + " NgroupRef num " + NgroupRef+ " Mgroup num " + Mgroup+ " lengthGroupId num " + lengthGroupId);
                            MatF2015.setValue(coord,F2015val);
                        }
                        System.out.println("DEBUG MSY HAKE F2015 vector : " + MatF2015);

                        // groups 14:58
                        int groupMin = onePop.getFbarGroupMin();
                        int groupMax = onePop.getFbarGroupMax();
                        int groupCount = groupMax - groupMin + 1;
                        F2015 = MatF2015.getSubMatrix(0, groupMin, groupCount).divs((double)groupCount).sumAll(); // Take groups 14 to 58 (45 groups); f bar is the mean of F per group on groups 14 to 58
                        context.setValue("F2015" + onePop.getName(), F2015);
                        System.out.println("DEBUG MSY HAKE F2015 the value" +F2015);
                    }
                    F2015 = (double) context.getValue("F2015" + onePop.getName());
                    ////System.out.println("DEBUG MSY HAKE F2015 " +F2015);
                    
                    //b/
                    //System.out.println("DEBUG MSY HAKE b");
                    double multFmsy = (2010 + (double) step.getYear() - (double) param_alpha) /(double) param_pi;
                    if (multFmsy > 1){
                        multFmsy = 1;
                    }
                    System.out.println("DEBUG MSY HAKE multFmsy " +multFmsy);
                    
                    double appliedF = (1-multFmsy) * F2015 + multFmsy * param_FMSY;
                    //Compare to MSY B_trigger
                    if (SSBiom < param_msyBtrigger) {
                        appliedF = appliedF * SSBiom / param_msyBtrigger;
                    }
                    System.out.println("DEBUG MSY HAKE appliedF " +appliedF);
                    
                    //Compute TAC : a/ F bar calculation (n-2 ; n-4 years) ; b/ F per length class ; c/ abundance ; d/ catch in number ; e/ TAC calculation
                    List<PopulationGroup> groups = onePop.getPopulationGroup();
                    int groupMin = onePop.getFbarGroupMin();
                    int groupMax = onePop.getFbarGroupMax();
                    int groupCount = groupMax - groupMin + 1;
                    //System.out.println("DEBUG MSY HAKE groups " +groupMin + " ; "+groupMax + " ; "+groupCount);

                    //a/ getting f using ALL catch and abundance begin month (cannot use ISIS fish fishing mortality function)
                    ////System.out.println("DEBUG MSY HAKE a");
                    List timeStepsF = new ArrayList();
                    TimeStep nextStep= step.previous(); 
                    for(int i=2; i<=4; i++){
                        nextStep= nextStep.previousYear();
                        timeStepsF.add(nextStep);
                    }
                    //MatrixND MatFPreviousYears = matResult.getMatrix(timeStepsF, onePop, MatrixFishingMortalityPerGroup.NAME).getSubMatrix(1, groupMin, groupCount).copy(); //0 time step ; 1 classes
                    MatrixND MatFPreviousYearsAll = matResult.getMatrix(timeStepsF, onePop, MatrixFishingMortalityPerGroup.NAME).copy(); //0 time step ; 1 classes
                    //MatFPreviousYears and MatFPreviousYearsAll: just to get a proper matrix, i recompute everything.....

                    for (MatrixIterator i = MatFPreviousYearsAll.iterator(); i.next();) {
                            Object [] coord = i.getSemanticsCoordinates();
                            TimeStep stepNum = (TimeStep) coord[0];
                            int stepNumVal = stepNum.getStep();
                            PopulationGroup lengthGroup = (PopulationGroup) coord[1];
                            int lengthGroupId = lengthGroup.getId();

                            //get catch
                            List listofSteps = new ArrayList();
                            nextStep= stepNum.add(-11); //Go back to Jan of the year of interest (between n-2 and n-4)
                            for(int j=1; j<=12; j++){
                                listofSteps.add(nextStep);
                                nextStep= nextStep.next();
                            }
                            double Cgroup = matResult.getMatrix(listofSteps, onePop, MatrixCatchPerStrategyMetPerZonePop.NAME).copy().getSubMatrix(3, lengthGroupId, 1).sumAll();// (dimension 0 step, 1 strategy, 2 metier, 3 group, 4 zone) In numbers BUT groups
                            // And SPLG and Non BoB catch. Loop over hake groups and add the catch in numbers of all year
                            for(int steps = (stepNumVal-11); steps<=stepNumVal; steps++){
                                Cgroup = Cgroup + (double) context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + steps);
                                Cgroup = Cgroup + (double)context.getValue("totOtherCatchNumStepNONBOB_" + lengthGroupId + "_"+steps);
                            }
                            //Cgroup = Cgroup + (double)context.getValue("totOtherCatchNumStepNONBOB_" + lengthGroupId + "_"+stepNum.getYear());

                           //Get M (clean way)
                            // compute total mortality by suming naturalDeathRate and fishingMortalityOtherFleetsMatrix
                            MatrixND naturalDeathRatePop = onePop.getNaturalDeathRateMatrix();
                            naturalDeathRatePop = naturalDeathRatePop.meanOverDim(1); // moyenne sur Zone
                            naturalDeathRatePop = naturalDeathRatePop.reduce(); // Enleve les dimensions de taille 1
                            MatrixND fishingMortalityOther = onePop.getFishingMortalityOtherFleetsMatrix();
                            fishingMortalityOther = fishingMortalityOther.meanOverDim(1); // moyenne sur Zone
                            fishingMortalityOther = fishingMortalityOther.reduce(); // Enleve les dimensions de taille 1
                            double Mgroup = naturalDeathRatePop.getValue(lengthGroup) + fishingMortalityOther.getValue(lengthGroup);

                            //get N begin month
                            double NgroupRef = matResult.getMatrix(stepNum.add(-11), onePop, MatrixAbundanceBeginMonth.NAME).copy().getSubMatrix(0, lengthGroupId, 1).sumAll(); // (dimension -1 step, 0 group, 1 zone)In numbers BUT groups 
                            
                            ObjectiveFunction f = new ObjectiveFunctionBaranov(Cgroup, Mgroup, NgroupRef);
                            double Fval = MinimisationUtil.fmin(0.0, 2.0, 1.0e-10, f); // step ??
                            MatFPreviousYearsAll.setValue(coord,Fval);
                        }
                        MatrixND MatFPreviousYears = MatFPreviousYearsAll.copy();//.getSubMatrix(1, groupMin, groupCount).copy(); //0 time step ; 1 classes                
                        //MatrixND Fbar = MatFPreviousYears.copy().sumOverDim(1).divs(groupCount).reduce(); //Fbar                
                        MatrixND Fsum = MatFPreviousYears.copy().sumOverDim(1).reduce(); //Fsum
                        //System.out.println("DEBUG MSY HAKE Fbar " +Fbar);
                        System.out.println("DEBUG MSY HAKE Fsum " +Fsum);
                                        
                    //b/
                    /*
                    for (MatrixIterator i = MatFPreviousYearsAll.iterator(); i.next();) { // Divide by Fbar
                        Object[] coordonnees = i.getSemanticsCoordinates(); //0 date ; 1 group (Fbar groups)
                        double value = MatFPreviousYearsAll.getValue(coordonnees);
                        value = value/Fbar.getValue(coordonnees[0]);
                        MatFPreviousYearsAll.setValue(coordonnees,value);
                    }
                    */
                    for (MatrixIterator i = MatFPreviousYears.iterator(); i.next();) { // Divide by Fbar
                        Object[] coordonnees = i.getSemanticsCoordinates(); //0 date ; 1 group (all F groups)
                        double value = MatFPreviousYears.getValue(coordonnees);
                        value = value/Fsum.getValue(coordonnees[0]);
                        MatFPreviousYears.setValue(coordonnees,value);
                    }
                        System.out.println("DEBUG MSY HAKE Fsum check 1 " +MatFPreviousYears);
                        System.out.println("DEBUG MSY HAKE Fsum check 2 " +MatFPreviousYears.sumAll());

                    MatrixND Fprofile = MatFPreviousYears.copy().sumOverDim(0).divs(3.0).reduce(); //Fprofile
                    double FprofileValue = Fprofile.copy().divs((double) Fprofile.getDim(0)).sumAll();
                        System.out.println("DEBUG MSY HAKE Fsum check 3 " +Fprofile);
                        System.out.println("DEBUG MSY HAKE Fsum check 4 " +Fprofile.sumAll());
                        System.out.println("DEBUG MSY HAKE Fsum check 5 " +FprofileValue);
                    
                    //b/
                    /*
                    System.out.println("DEBUG MSY HAKE b");
                    for (PopulationGroup group : groups){
                        for(int i=2; i<=4; i++){
                            System.out.println("DEBUG MSY HAKE i : " + i);
                            System.out.println("DEBUG MSY HAKE group : " + group.getId());
                            MatFPreviousYears.getSubMatrix(0, i-2, 1).divs(Fbar.getValue(i-2));
                            System.out.println("DEBUG MSY HAKE : " + MatFPreviousYears.getSubMatrix(0, i-2, 1).getValue(0));
                        }
                    }
                    */

                    MatrixND appliedFGroups = Fprofile.mults(appliedF).divs(FprofileValue);
                        System.out.println("DEBUG MSY HAKE Fsum check 6 " +appliedF/FprofileValue);
                        System.out.println("DEBUG MSY HAKE Fsum check 7 " +appliedFGroups);
                    
                    //MatrixND appliedFGroups = MatFPreviousYearsAll.sumOverDim(0).reduce().mults(appliedF).divs(3);
                    //appliedFGroups = appliedFGroups.mults(appliedF/(appliedFGroups.copy().sumAll()/appliedFGroups.getDim(0)));

                    //c/
                    //System.out.println("DEBUG MSY HAKE c");
                    // ONLY WORKS WITH  DefaultSimulatorRecruitmentPerZoneResultBeforeCondition.java SIMULATOR, WHICH GENERATES RESULTS BEFORE THE CONDITION AND PRE-ACTION
                    System.out.println("DEBUG MSY HAKE check abundance zone semantics : " + matResult.getMatrix(step, onePop, MatrixAbundance.NAME).getSemantic(1));                  
                    MatrixND Abundance = matResult.getMatrix(step, onePop, MatrixAbundance.NAME).copy();
                    System.out.println("DEBUG MSY HAKE check abundance : " + Abundance);

                    MatrixND AbundancebeginMonth=fromAbd2AbdBeginMonth(Abundance,context,step,onePop);
                    AbundancebeginMonth=AbundancebeginMonth.getSubMatrix(1, 2, 4).sumOverDim(1).reduce();// (0 step) 0 group 1 zone, only BoB individuals
                    
                                        
                    //d/
                    //System.out.println("DEBUG MSY HAKE d");
                    MatrixND CatchNumber = appliedFGroups.copy(); // This copy is just an easy way to create the catch matrix
                    for (MatrixIterator i = CatchNumber.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        System.out.println("DEBUG MSY HAKE Fsum check 8 " +coordonnees[0]);
                        double value = AbundancebeginMonth.getValue(coordonnees[0])*(appliedFGroups.getValue(coordonnees[0])/(0.5+appliedFGroups.getValue(coordonnees[0])))*(1-Math.exp(-0.5-appliedFGroups.getValue(coordonnees[0])));
                        System.out.println("DEBUG MSY HAKE catch value : " + value);
                        CatchNumber.setValue(coordonnees,value);
                    }
                    System.out.println("DEBUG MSY HAKE check CatchNumber : " + CatchNumber);

                    //e/
                    //System.out.println("DEBUG MSY HAKE e");
                    MatrixND CatchWeight = CatchNumber.copy(); // This copy is just an easy way to create the catch matrix

                    for (MatrixIterator i = CatchNumber.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup) coordonnees[0];
                        System.out.println("DEBUG MSY HAKE Fsum check 9 " +coordonnees[0]);
                        System.out.println("DEBUG MSY HAKE Fsum check 10 " +group.getMeanWeight());
                        double value = i.getValue()*group.getMeanWeight()/1000 ; // Weight in tons
                        CatchWeight.setValue(coordonnees,value);
                        System.out.println("DEBUG MSY HAKE catch value 2 : " + value);
                    }
                    System.out.println("DEBUG MSY HAKE check CatchWeight : " + CatchWeight);

                    //Capping
                    //System.out.println("DEBUG MSY HAKE f");
                    TACvalue = CatchWeight.copy().sumAll();
                    TACvalue = TACvalue/0.375;
                    //System.out.println("DEBUG MSY HAKE value 3 : " + TACvalue);
                    //System.out.println("DEBUG MSY HAKE TAC value before : " + TACvalue);
                    double TACpreviousYear= (double) context.getValue("TACvalue_" + (year-1));
                    if(TACvalue<0.8*TACpreviousYear){
                        TACvalue=0.8*TACpreviousYear;
                    }
                    if(TACvalue>1.2*TACpreviousYear){
                        TACvalue=1.2*TACpreviousYear;
                    }
                    
                    //System.out.println("DEBUG MSY HAKE g : " + TACvalue);
                    TACBOBvalue = TACvalue*0.375;
                    //System.out.println("DEBUG MSY HAKE h");
                    context.setValue("TACvalue_" + year, TACvalue);
                    //System.out.println("DEBUG MSY HAKE i");
                    context.setValue("TACBOBvalue_" + year, TACBOBvalue);   
                    //System.out.println("DEBUG MSY HAKE j");              
                }
                
                //Sole
                if (onePop.getSpecies() .getName().equals(param_species2.getName())){
                    //System.out.println("DEBUG MSY SOLE beg");
                    
                    //Get SSB
                    MatrixND MatBiom = matResult.getMatrix(step, onePop, MatrixBiomass.NAME).copy();
                    //System.out.println("Import de la biomasse au tout debut du mois de janvier realise");
                    MatBiom = MatBiom.reduce();
        
                    double SSBiom = 0;
                    for (MatrixIterator i=MatBiom.iterator(); i.hasNext();) {
                        i.next();
                        Object [] sems = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup)sems[0];
                        
                        double val = i.getValue()* group.getMaturityOgive();
            
                        SSBiom = SSBiom + val;
                    }
                    SSBiom = SSBiom / 1000;
                    System.out.println("DEBUG MSY SOLE SSB " + SSBiom);
                    System.out.println("DEBUG MSY SOLE param_msyBtrigger " + param_msyBtrigger2);
                    
                    
                    //Compute F to implement : a/ get F at the beginning of the transition b/ compute F to implement
                    //a/
                    //System.out.println("DEBUG MSY SOLE a");
                    double F2015;
                    if(year==6){
                        MatrixND MatF2015 = matResult.getMatrix(step.previous(), onePop, MatrixTotalFishingMortality.NAME).copy();
                        F2015 = MatF2015.sumAll();
                        context.setValue("F2015" + onePop.getName(), F2015);
                    }
                    F2015 = (double) context.getValue("F2015" + onePop.getName());
                    System.out.println("DEBUG MSY SOLE F2015 " +F2015);
                    
                    //b/
                    //System.out.println("DEBUG MSY SOLE b");
                    double multFmsy = (2010 + (double) step.getYear() - (double) param_alpha) /(double) param_pi;
                    if (multFmsy > 1){
                        multFmsy = 1;
                    }
                    
                    double appliedF = (1-multFmsy) * F2015 + multFmsy * param_FMSY2;
                    System.out.println("DEBUG MSY SOLE appliedF " +multFmsy);
                    System.out.println("DEBUG MSY SOLE appliedF " +appliedF);
                    
                    //Compare to MSY B_trigger
                    if (SSBiom < param_msyBtrigger2) {
                        appliedF = appliedF * SSBiom / param_msyBtrigger2;
                    }
                    //System.out.println("DEBUG MSY HAKE F2015 " +appliedF);
                    System.out.println("DEBUG MSY SOLE appliedF " +appliedF);

                    //Compute TAC : a/ F bar calculation (n-2 ; n-4 years) ; b/ F per length class ; c/ abundance ; d/ catch in number ; e/ TAC calculation
                    List<PopulationGroup> groups = onePop.getPopulationGroup();
                    int groupMin = onePop.getFbarGroupMin();
                    int groupMax = onePop.getFbarGroupMax();
                    int groupCount = groupMax - groupMin + 1;

                    //a/
                    //System.out.println("DEBUG MSY SOLE a");
                    List timeStepsF = new ArrayList();
                    TimeStep nextStep= step.previous(); 
                    //System.out.println("DEBUG MSY SOLE a");
                    for(int i=2; i<=4; i++){
                    //System.out.println("DEBUG MSY SOLE a2");
                        nextStep= nextStep.previousYear();
                        timeStepsF.add(nextStep);
                    }
                    //System.out.println("DEBUG MSY SOLE a");
                    //MatrixND MatFPreviousYears = matResult.getMatrix(timeStepsF, onePop, MatrixFishingMortalityPerGroup.NAME).getSubMatrix(1, groupMin, groupCount).copy(); //0 time step ; 1 classes
                    MatrixND MatFPreviousYearsAll = matResult.getMatrix(timeStepsF, onePop, MatrixFishingMortalityPerGroup.NAME).copy(); //0 time step ; 1 classes
                    
                    //System.out.println("DEBUG MSY SOLE a");
                    //MatrixND Fbar = MatFPreviousYears.copy().sumOverDim(1).divs(groupCount).reduce(); //Fbar
                    MatrixND Fsum = MatFPreviousYearsAll.copy().sumOverDim(1).reduce(); //Fsum
                    //System.out.println("DEBUG MSY SOLE a");
                    
                    for (MatrixIterator i = MatFPreviousYearsAll.iterator(); i.next();) { // Divide by Fbar
                        Object[] coordonnees = i.getSemanticsCoordinates(); //0 date ; 1 group (Fbar groups)
                        TimeStep yearStep =  (TimeStep) coordonnees[0];
                        int yearNum = yearStep.getYear();
                        double value = MatFPreviousYearsAll.getValue(coordonnees);
                        //value = value/Fbar.getValue(coordonnees[0]);
                        value = value/Fsum.getValue(coordonnees[0]);
                        MatFPreviousYearsAll.setValue(coordonnees,value);
                    }

                    MatrixND Fprofile = MatFPreviousYearsAll.copy().sumOverDim(0).divs(3.0).reduce(); //Fprofile
                    double FprofileValue = Fprofile.copy().divs((double) Fprofile.getDim(0)).sumAll();
                    
                    /*
                    //b/
                    System.out.println("DEBUG MSY SOLE b");
                    for (PopulationGroup group : groups){
                        for(int i=2; i<=4; i++){
                            MatFPreviousYears.getSubMatrix(0, i-2, 1).divs(Fbar.getValue(i-2));
                            System.out.println("DEBUG MSY SOLE : " + MatFPreviousYears.getSubMatrix(0, i-2, 1).getValue(0));
                        }
                    }
                    */

                    MatrixND appliedFGroups = Fprofile.mults(appliedF).divs(FprofileValue);
                    System.out.println("DEBUG MSY SOLE appliedFGroups" + appliedFGroups);
                    
                    //MatrixND appliedFGroups = MatFPreviousYearsAll.sumOverDim(0).reduce().mults(appliedF).divs(3);
                    //appliedFGroups = appliedFGroups.mults(appliedF/(appliedFGroups.copy().sumAll()/appliedFGroups.getDim(0)));

                    //c/
                    //System.out.println("DEBUG MSY SOLE c");
                    // ONLY WORKS WITH  DefaultSimulatorRecruitmentPerZoneResultBeforeCondition.java SIMULATOR, WHICH GENERATES RESULTS BEFORE THE CONDITION AND PRE-ACTION
                    MatrixND Abundance = matResult.getMatrix(step, onePop, MatrixAbundance.NAME).copy();
                    MatrixND AbundancebeginMonth=fromAbd2AbdBeginMonth(Abundance,context,step,onePop);
                    AbundancebeginMonth=AbundancebeginMonth.sumOverDim(1).reduce();// (0 step) 0 group 1 zone
                    
                    //d/
                    //System.out.println("DEBUG MSY SOLE d");
                    MatrixND CatchNumber = appliedFGroups.copy(); // This copy is just an easy way to create the catch matrix
                    for (MatrixIterator i = CatchNumber.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        double value = AbundancebeginMonth.getValue(coordonnees[0])*(appliedFGroups.getValue(coordonnees[0])/(0.1+appliedFGroups.getValue(coordonnees[0])))*(1-Math.exp(-0.1-appliedFGroups.getValue(coordonnees[0])));
                        //System.out.println("DEBUG MSY SOLE value : " + value);
                        CatchNumber.setValue(coordonnees,value);
                    }

                    //e/
                    //System.out.println("DEBUG MSY SOLE e");
                    MatrixND CatchWeight = CatchNumber.copy(); // This copy is just an easy way to create the catch matrix      

                    for (MatrixIterator i = CatchNumber.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup) coordonnees[0];
                        double value = i.getValue()*group.getMeanWeight()/1000 ; // Weight in tons
                        CatchWeight.setValue(coordonnees,value);
                        //System.out.println("DEBUG MSY SOLE value 2 : " + value);
                    }

                    //Capping
                    //System.out.println("DEBUG MSY SOLE f");
                    TACvalueSole =CatchWeight.copy().sumAll();
                    //System.out.println("DEBUG MSY SOLE value 3 : " + TACvalueSole);
                    //System.out.println("DEBUG MSY SOLE f : " + TACvalueSole);
                    double TACpreviousYearSole= (double) context.getValue("TACvalueSole_" + (year-1));
                    if(TACvalueSole<0.8*TACpreviousYearSole){
                        TACvalueSole=0.8*TACpreviousYearSole;
                    }
                    if(TACvalueSole>1.2*TACpreviousYearSole){
                        TACvalueSole=1.2*TACpreviousYearSole;
                    }
                    //System.out.println("DEBUG MSY SOLE g : " + TACvalueSole);
                    context.setValue("TACvalueSole_" + year, TACvalueSole);  
                    //System.out.println("DEBUG MSY SOLE h");                  
                }
                //Nephrops
                if (onePop.getSpecies() .getName().equals(param_species3.getName())){
                    // No BMSY trigger : compute F to implement from FMSY
                    double fMsyHcr = param_FMSY3;
                    
                    //Compute F to implement : a/ get F at the beginning of the transition b/ compute F to implement
                    //a/
                    double F2015;
                    if(year==6){
                        List listofSteps = new ArrayList();
                        TimeStep nextStep= step.previousYear(); 
                        for(int i=1; i<=12; i++){
                            listofSteps.add(nextStep);
                            nextStep= nextStep.next();
                        }
                        double MatF2015_catch = matResult.getMatrix(listofSteps, onePop, MatrixCatchPerStrategyMetPerZonePop.NAME).copy().sumAll(); // In numbers
                        //System.out.println("DEBUG MSY NEP F2015 : " + MatF2015_catch);
                        double MatF2015_disc = matResult.getMatrix(listofSteps, onePop, MatrixDiscardsPerStrMetPerZonePop.NAME).copy().sumAll(); // In numbers
                        //System.out.println("DEBUG MSY NEP F2015 : " + MatF2015_disc);
                        double MatF2015 = matResult.getMatrix(step.previousYear(), onePop, MatrixAbundanceBeginMonth.NAME).copy().sumAll(); // In numbers
                        //System.out.println("DEBUG MSY NEP F2015 : " + MatF2015);
                        F2015 = (MatF2015_catch-0.3*MatF2015_disc)/MatF2015;
                        context.setValue("F2015" + onePop.getName(), F2015);
                    }
                    F2015 = (double) context.getValue("F2015" + onePop.getName());
                    System.out.println("DEBUG MSY NEP F2015 : " + F2015);
                    
                    //b/
                    double multFmsy = (2010 + (double) step.getYear() - (double) param_alpha) /(double) param_pi;
                    if (multFmsy > 1){
                        multFmsy = 1;
                    }
                    double appliedF = (1-multFmsy) * F2015 + multFmsy * fMsyHcr;
                    System.out.println("DEBUG MSY NEP multFmsy : " + multFmsy);
                    System.out.println("DEBUG MSY NEP appliedF : " + appliedF);
                    //Compute TAC : a/ get abundance  b/compute dead catch in numbers per class/sex/fraction  c/ Deduce TAC with length-weight relationship
                    //a/
                    //System.out.println("DEBUG MSY NEP a");
                    // ONLY WORKS WITH  DefaultSimulatorRecruitmentPerZoneResultBeforeCondition.java SIMULATOR, WHICH GENERATES RESULTS BEFORE THE CONDITION AND PRE-ACTION
                    MatrixND Abundance = matResult.getMatrix(step, onePop, MatrixAbundance.NAME).copy(); // 0 step 1 group 2 zone
                    MatrixND AbundancebeginMonth=fromAbd2AbdBeginMonth(Abundance,context,step,onePop);
                    double abundance = AbundancebeginMonth.sumAll();
                    double deadCatch = abundance * appliedF;
                    //System.out.println("DEBUG MSY NEP b " + deadCatch);
                    
                    //b/
                    //System.out.println("DEBUG MSY NEP b");
                    File fileLandNepLFDObs = new File("input/nep20132015Obs4ISISLand.csv");
                    MatrixND matrixfileLandNepLFDObs = MatrixFactory.getInstance().create(fileLandNepLFDObs);
                    //File fileDiscNepLFDObs = new File("input/nep20132015Obs4ISISDisc.csv");
                    //MatrixND matrixfileDiscNepLFDObs = MatrixFactory.getInstance().create(fileDiscNepLFDObs);
                    
                    MatrixND nepLandings = matrixfileLandNepLFDObs.copy().mults(deadCatch);
                    
                    //c/
                    //System.out.println("DEBUG MSY NEP c");
                    for(int i =1 ; i<=33;i++){ // Males
                        nepLandings.setValue(i,nepLandings.getValue(i)*0.39*Math.pow(((i-1)*2+11),3.18));
                        //System.out.println("DEBUG MSY NEP land male : " + nepLandings.getValue(i));
                    }
                    for(int i =35 ; i<=57;i++){ // Females
                        nepLandings.setValue(i,nepLandings.getValue(i)*0.81*Math.pow(((i-35)*2+11),2.97));
                        //System.out.println("DEBUG MSY NEP land female : " + nepLandings.getValue(i));
                    }
                    //System.out.println("DEBUG MSY NEP d");

                     //Capping
                    //System.out.println("DEBUG MSY NEP d");
                    TACvalueLang = nepLandings.copy().sumAll()/1000000000;
                    //System.out.println("DEBUG MSY NEP catch value 2 : " + TACvalueLang);
                    //System.out.println("DEBUG MSY NEP d : " + TACvalueLang);
                    double TACpreviousYearlang= (double) context.getValue("TACvalueLang_" + (year-1));
                    if(TACvalueLang<0.8*TACpreviousYearlang){
                        TACvalueLang=0.8*TACpreviousYearlang;
                    }
                    if(TACvalueLang>1.2*TACpreviousYearlang){
                        TACvalueLang=1.2*TACpreviousYearlang;
                    }
                    //System.out.println("DEBUG MSY NEP e : " + TACvalueLang);
                    context.setValue("TACvalueLang_" + year, TACvalueLang);
                    //System.out.println("DEBUG MSY NEP catch value 3 : " + TACvalueLang);
                    //System.out.println("DEBUG MSY NEP f");
                }
                    /*
                    //b/
                    List listofSteps = new ArrayList();
                        TimeStep nextStep= step.previousYear(); 
                        for(int i=1; i<=12; i++){
                            listofSteps.add(nextStep);
                            nextStep= nextStep.next();
                        }
                    double MatCatchPrevYearNum = matResult.getMatrix(listofSteps, onePop, MatrixCatchPerStrategyMetPerZonePop.NAME).copy().sumAll(); // In numbers
                    double MatCatchPrevYearWeight = matResult.getMatrix(listofSteps, onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy().sumAll()/1000000; // In weight (tons)
                    double meanWeightIndiv=MatCatchPrevYearWeight/MatCatchPrevYearNum;

                    //c/ prop of landings among catch in weight
                    double MatDiscardsPrevYearWeight = matResult.getMatrix(listofSteps, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy().sumAll()/1000000; // In weight (tons)
                    double propLandNep= (MatCatchPrevYearWeight-MatDiscardsPrevYearWeight)/(MatCatchPrevYearWeight);
                    
                    //System.out.println("DEBUG MSY NEP d");
                     //Capping
                    //System.out.println("DEBUG MSY NEP d");
                    TACvalueLang = deadCatch*meanWeightIndiv*propLandNep;
                    System.out.println("DEBUG MSY NEP TAC check : " + deadCatch + ";" + meanWeightIndiv+ ";" + propLandNep + ";" + TACvalueLang);
                    //System.out.println("DEBUG MSY NEP d : " + TACvalueLang);
                    double TACpreviousYearlang= (double) context.getValue("TACvalueLang_" + (year-1));
                    */
            }
            //Save
            String stringTACExport = year+";hake;"+context.getValue("TACvalue_" + year)+"\n"+year+";sole;"+TACvalueSole+"\n"+year+";lang;"+TACvalueLang+"\n";
            if(year==0){
                 FileUtils.writeStringToFile(outputTACvalues,"year;stock;value\n", UTF_8, false);
            }
            FileUtils.writeStringToFile(outputTACvalues,stringTACExport, UTF_8, true);
        }

        //Any adjustment on effort ?
        if((year>=3)&(step.getMonth().equals(Month.JANUARY)& firstIteration)){
            //System.out.println("DEBUG REF EFF VARS ANN STEP " + step.getStep());
            List<Population> Pops = context.getPopulationMonitor().getPopulations();
            ResultStorage matResult = context.getSimulationStorage().getResultStorage();
            
            List timeStepsList = new ArrayList();
            TimeStep nextStep= step.minus(step.getStep()); 
            for(int i=0; i<(step.getStep()); i++){
                timeStepsList.add(nextStep);
                nextStep= nextStep.add(1);
            }
            
            SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
            double ratioHakeInterAnnual = 1;
            double ratioSoleInterAnnual = 1;
            double ratioNepInterAnnual = 1;
            
            //System.out.println("DEBUG REF EFF VARS ANN A STEP " + step.getStep());
            
            for(Population onePop:Pops){
                // hake
                if (onePop.getSpecies() .getName().equals(param_species.getName())){
                    //System.out.println("DEBUG REF EFF VARS ANN HAKE STEP " + step.getStep());
                    double annualCPUEHake [] = new double[year];
                    
                    for(int yearNum=(year-1); yearNum<year; yearNum++){
                        // get Total catch (take all metiers)
                        MatrixND MatCatchWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy(); //0 time step ; 1 classes
                        double catchWeightVal = MatCatchWeight.sumAll()/1000; // In tons
						// If before 2016, work on LPUE, TAL, etc. Note that in 2016, it will compute a CPUE for 2015; in 2015, it will compute a LPUE for 2014
                        if(year<6){
                            double discWeightVal = 0;
                            if( matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                 MatrixND MatDiscWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                 discWeightVal = MatDiscWeight.sumAll()/1000; // In tons
                            }
                            
                            catchWeightVal=catchWeightVal-discWeightVal;
                        } 
                       // System.out.println("DEBUG REF EFF VARS ANN HAKE CATCH catchWeightVal 1 " + catchWeightVal);

                        //AND SPLG CATCH. THE COMPUTED CPUE will be catch per unit of non SPLG effort
                        if(!step.getMonth().equals(Month.JANUARY)){
                            for(int stepNum = 12*yearNum; stepNum<12*(yearNum+1); stepNum++){
                                for (int lengthGroupId =0; lengthGroupId<72 ; lengthGroupId++) {
                                    catchWeightVal = catchWeightVal + (double) context.getValue("totOtherCatchStep_" + lengthGroupId + "_" + stepNum);
                                }
                            }
                        }
						 //If before 2016, work on LPUE, TAL, etc.
                        if(!step.getMonth().equals(Month.JANUARY)){
                            for(int stepNum = 12*yearNum; stepNum<12*(yearNum+1); stepNum++){
                                for (int lengthGroupId =0; lengthGroupId<72 ; lengthGroupId++) {
                                    catchWeightVal = catchWeightVal - (double) context.getValue("totOtherDiscardStep_" + lengthGroupId + "_" + stepNum);
                                }
                            }
                        }
                        
                        //System.out.println("DEBUG REF EFF VARS ANN HAKE CATCH YEAR " + yearNum);
                        //System.out.println("DEBUG REF EFF VARS ANN HAKE CATCH catchWeightVal 2" + catchWeightVal);

                        // get effort (all metiers)
                        double effortVal=0;
                        for(int seasNum=0; seasNum<4; seasNum++){
                            MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList((yearNum*12 + seasNum*3),(yearNum*12+ (seasNum+1)*3)), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                           // System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            //System.out.println("DEBUG REF EFF VARS ANN HAKE EFF YEAR " + yearNum + "SEAS" + seasNum);

                            // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                //System.out.println("DEBUG REF EFF VARS ANN HAKE METIER " + metTemp.getName());
                                double TarfValue = 1;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TNEP"+(2010+yearNum)+"s"+(1+seasNum),1.0);
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBW"+(2010+yearNum)+"s"+(1+seasNum),1.0);   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC") |metTemp.getName().equals("Metier_ChalutSole_NordCet") |metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBE"+(2010+yearNum)+"s"+(1+seasNum),1.0);  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.GBOB"+(2010+yearNum)+"s"+(1+seasNum),1.0);   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.LBOB"+(2010+yearNum)+"s"+(1+seasNum),1.0);    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                //System.out.println("DEBUG REF EFF VARS ANN HAKE METIER TARF "+ " OLD VALUE " + oldEffort + TarfValue + " NEW VALUE " + newValue);
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();
                        }
                        // compute CPUE each year
                        //System.out.println("DEBUG REF EFF VARS ANN HAKE effortVal " + effortVal);
                        //System.out.println("DEBUG REF EFF VARS ANN HAKE catchWeightVal " + catchWeightVal);
                        //System.out.println("DEBUG REF EFF VARS ANN HAKE CPUE " + catchWeightVal/effortVal);
                        annualCPUEHake[yearNum] = catchWeightVal/effortVal; // Despite the name, it is LPUE or CPUE depending on the year (see above in the code, around l990)
                    }

                    // get mean CPUE
                    double meanCPUEHake = 0;
                    for(int i=(year-1); i<year;i++){
                        meanCPUEHake = meanCPUEHake + annualCPUEHake[i];
                    }
                    meanCPUEHake=meanCPUEHake;

                    // compute expected catch. Assumption: expected effort of year n is the effort of 2010 * its inter-annual thingy
                        double effortVal=0;
                        for(int seasNum=0; seasNum<4; seasNum++){
                            MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList((seasNum*3),((seasNum+1)*3)), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));

                            // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                //System.out.println("DEBUG REF EFF VARS ANN HAKE METIER " + metTemp.getName());
                                double TarfValue = 1.0;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s"+(1+seasNum),1.0);
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s"+(1+seasNum),1.0);   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC")  |metTemp.getName().equals("Metier_ChalutSole_NordCet")|metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s"+(1+seasNum),1.0);  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s"+(1+seasNum),1.0);   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s"+(1+seasNum),1.0);    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                //System.out.println("DEBUG REF EFF VARS ANN HAKE METIER TARF "+ " OLD VALUE " + oldEffort + TarfValue + " NEW VALUE " + newValue);
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();
                        }

                    double expectedCatch = meanCPUEHake*effortVal;
                   // System.out.println("DEBUG REF EFF VARS ANN HAKE meanCPUEHake " +meanCPUEHake);
                   // System.out.println("DEBUG REF EFF VARS ANN HAKE effortVal " +effortVal);
                    //System.out.println("DEBUG REF EFF VARS ANN HAKE EXPECTED CATCH " + expectedCatch + " TAC " + TACBOBvalue);
                        
                    // compute ratio
                    if(expectedCatch>TACBOBvalue){
                        ratioHakeInterAnnual = TACBOBvalue/expectedCatch; //TACBOBvalue is a TAL until 2015 and a TAC from 2016, so OK
                    }             
                    //System.out.println("DEBUG REF EFF VARS ANN HAKE RATIO " + ratioHakeInterAnnual);       
                }
                
                // sole
                if (onePop.getSpecies() .getName().equals(param_species2.getName())){
                    //System.out.println("DEBUG REF EFF VARS ANN SOLE STEP " + step.getStep());
                    double annualCPUESole [] = new double[year];

                    for(int yearNum=(year-1); yearNum<year; yearNum++){
                    //System.out.println("DEBUG REF EFF VARS ANN SOLE STEP " + step.getStep());
                        // get Total catch (take all metiers)
                        MatrixND MatCatchWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy(); //0 time step ; 1 classes
                        double catchWeightVal = MatCatchWeight.sumAll()/1000; // In tons
                        //    System.out.println("DEBUG REF EFF VARS ANN SOLE CATCH YEAR " + yearNum);
						// If before 2016, work on LPUE, TAL, etc. Note that in 2016, it will compute a CPUE for 2015; in 2015, it will compute a LPUE for 2014
                        if(year<6){
                            double discWeightVal = 0;
                            if( matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                 MatrixND MatDiscWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                 discWeightVal = MatDiscWeight.sumAll()/1000; // In tons
                            }
                            
                            catchWeightVal=catchWeightVal-discWeightVal;
                        }
                        
                        // get effort (TFBE and GBOB only)
                        double effortVal=0;
                        MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                        //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                        //    System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                        //    System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                        //    System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                        //System.out.println("DEBUG REF EFF VARS ANN SOLE EFF YEAR " + yearNum);
                        
                        // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                //System.out.println("DEBUG REF EFF VARS ANN SOLE METIER " + metTemp.getName());
                                
                                double TarfValue = 1.0;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBESole"+(2010+yearNum),1.0);
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = 0;   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC")  |metTemp.getName().equals("Metier_ChalutSole_NordCet")|metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBESole"+(2010+yearNum),1.0);  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.GBOBSole"+(2010+yearNum),1.0);   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = 0;    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                //System.out.println("DEBUG REF EFF VARS ANN SOLE METIER TARF "+ " OLD VALUE " + oldEffort + TarfValue + " NEW VALUE " + newValue);
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();
    
                        // compute CPUE each year
                        //System.out.println("DEBUG REF EFF VARS ANN SOLE CPUE " + catchWeightVal/effortVal);
                        annualCPUESole[yearNum] = catchWeightVal/effortVal;
                    }

                    // get mean CPUE
                    double meanCPUESole = 0;
                    for(int i=(year-1); i<year;i++){
                        meanCPUESole = meanCPUESole + annualCPUESole[i];
                    }
                    meanCPUESole=meanCPUESole;

                    // compute ratio
                    double effortVal=0;
                            MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList(0,12), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                           // System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));

                            // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                double TarfValue = 1.0;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBESole"+(2010+year),1.0);  
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = 0;   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC")  |metTemp.getName().equals("Metier_ChalutSole_NordCet")|metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.TFBESole"+(2010+year),1.0);  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.GBOBSole"+(2010+year),1.0);   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = 0;    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();

                            double expectedCatch = meanCPUESole*effortVal;
                            //System.out.println("DEBUG REF EFF VARS ANN SOLE EXPECTED CATCH " + expectedCatch + " TAC " + (double) context.getValue("TACvalueSole_" + year));
                        
                    // compute ratio
                    if(expectedCatch> (double) context.getValue("TACvalueSole_" + year)){
                        ratioSoleInterAnnual = (double) context.getValue("TACvalueSole_" + year)/expectedCatch;
                    }  
                    //System.out.println("DEBUG REF EFF VARS ANN SOLE RATIO " + ratioSoleInterAnnual); 
                }
                
                // nep
                if (onePop.getSpecies() .getName().equals(param_species3.getName())){
                    //System.out.println("DEBUG REF EFF VARS ANN NEP STEP " + step.getStep());
                    double annualCPUENep [] = new double[year];

                    for(int yearNum=(year-1); yearNum<year; yearNum++){
                        // get Total catch (take all metiers)
                        MatrixND MatCatchWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy(); //0 time step ; 1 classes
                        double catchWeightVal = MatCatchWeight.sumAll()/1000; // In tons
                        //System.out.println("DEBUG REF EFF VARS ANN NEP CATCH YEAR " + yearNum);
                        // IT IS ALWAYS A TAL WITH NEPHROPS, WORK ON LPUE WITH TALsdouble discWeightVal = 0;
                        double discWeightVal=0;
                            if( matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                 MatrixND MatDiscWeight = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                 discWeightVal = MatDiscWeight.sumAll()/1000; // In tons
                            }
                            catchWeightVal=catchWeightVal-discWeightVal;
                        
                        
                        // get effort (TNEP only)
                        double effortVal=0;
                        MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList((yearNum*12),((yearNum+1)*12)), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                        //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                        //    System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                        //    System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                        //    System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                        //System.out.println("DEBUG REF EFF VARS ANN NEP EFF YEAR " + yearNum);
                        
                        // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                //System.out.println("DEBUG REF EFF VARS ANN NEP METIER " + metTemp.getName());
                                double TarfValue = 1.0;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.Nep"+(2010+yearNum),1.0); 
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = 0;   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC") |metTemp.getName().equals("Metier_ChalutSole_NordCet") |metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = 0;  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = 0;   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = 0;    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                //System.out.println("DEBUG REF EFF VARS ANN NEP METIER TARF "+ " OLD VALUE " + oldEffort + TarfValue + " NEW VALUE " + newValue);
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();
    
                        // compute CPUE each year
                        //System.out.println("DEBUG REF EFF VARS ANN NEP CPUE " + catchWeightVal/effortVal);
                        annualCPUENep[yearNum] = catchWeightVal/effortVal;
                    }

                    // get mean CPUE
                    double meanCPUENep = 0;
                    for(int i=(year-1); i<year;i++){
                        meanCPUENep = meanCPUENep + annualCPUENep[i];
                    }
                    meanCPUENep=meanCPUENep;

                    // compute ratio
                    double effortVal=0;
                            MatrixND MatEffort = matResult.getMatrix(timeStepsList.subList(0,12), MatrixEffortPerStrategyMet.NAME).copy(); //0 time step ; 1 strategy; 2 metier
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));
                            MatEffort = MatEffort.sumOverDim(1).reduce();
                            //System.out.println("Dym mateffort = " + MatEffort.getDimCount());
                            //System.out.println("Dym mateffort dim 1 = " + MatEffort.getDimensionName(1));

                            // multiply effort values with Tarfs (use a loop)
                            List<Metier> metTempList = siMatrix.getMetiers(step);
                            for (MatrixIterator i = MatEffort.iterator(); i.next();) {
                                Object[] coordonnees = i.getSemanticsCoordinates();
                                Metier metTemp = (Metier) coordonnees[1];
                                double TarfValue = 1.0;
                                //TNEP
                                if (metTemp.getName().equals("Metier_Lang_NordPC") |metTemp.getName().equals("Metier_Lang_InterC") |metTemp.getName().equals("Metier_Lang_InterPC")){
                                    TarfValue = (double) context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0); 
                                }   
                                //TFBW
                                if (metTemp.getName().equals("PTBV_VIIIabd") |metTemp.getName().equals("OBTS_VIIIabd") ){
                                    TarfValue = 0;   
                                } 
                                //TFBE
                                if (metTemp.getName().equals("Metier_ChalutMixte_APCS") |metTemp.getName().equals("Metier_ChalutMixte_NordC") |metTemp.getName().equals("Metier_ChalutSole_InterSudC") |metTemp.getName().equals("Metier_ChalutSole_InterC") |metTemp.getName().equals("Metier_ChalutSole_NordCet") |metTemp.getName().equals("Metier_ChalutBenth_APCS") |metTemp.getName().equals("Metier_ChalutBenth_NordC") |metTemp.getName().equals("Metier_ChalutBenth_NordAPC") |metTemp.getName().equals("Metier_ChaluMixte_InterC") |metTemp.getName().equals("Metier_ChalutMixte_NordPC")){
                                    TarfValue = 0;  
                                }   
                                //GBOB
                                if(metTemp.getName().equals("Metier_FiletMerlu_InterSudPC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudC") |metTemp.getName().equals("Metier_FiletMerlu_InterSudAPC") |metTemp.getName().equals("Metier_FiletMIxte_InterC") |metTemp.getName().equals("Metier_FiletMixte_InterSudC") |metTemp.getName().equals("Metier_FiletMixte_NordInterPC") |metTemp.getName().equals("Metier_FiletMixte_NordC") |metTemp.getName().equals("Metier_FiletMixte_NordPC") |metTemp.getName().equals("Metier_FiletMerlu_NordPC") |metTemp.getName().equals("Metier_FiletSole_NordC") |metTemp.getName().equals("Metier_FiletSole_InterC") |metTemp.getName().equals("Metier_FiletSole_InterSudPC") |metTemp.getName().equals("Metier_FiletSole_NordIntPC")){
                                    TarfValue = 0;   
                                }
                                //LBOB
                                if (metTemp.getName().equals("Metier_PalangreMerlu_InterSudAC") |metTemp.getName().equals("Metier_PalangreMixte_NordC")|metTemp.getName().equals("Metier_PalangreMixte_InterC")|metTemp.getName().equals("Metier_PalangreMerlu_InterSudC") ){
                                    TarfValue = 0;    
                                } 
                                double oldEffort = i.getValue();
                                double newValue = oldEffort*TarfValue;
                                i.setValue(newValue);
                            }
                            effortVal = effortVal+MatEffort.sumAll();

                            double expectedCatch = meanCPUENep*effortVal;
                            //System.out.println("DEBUG REF EFF VARS ANN NEP EXPECTED CATCH " + expectedCatch + " TAC " + (double) context.getValue("TACvalueLang_" + year));

                    // compute ratio
                    if(expectedCatch> (double) context.getValue("TACvalueLang_" + year)){
                        ratioNepInterAnnual = (double) context.getValue("TACvalueLang_" + year)/expectedCatch;
                    } 
                    //System.out.println("DEBUG REF EFF VARS ANN NEP RATIO " + ratioNepInterAnnual);    
                        
                }

            }

                // compare ratios for each gear
                // update Tarf values for inter nanual eeffort variations
                Vector ratioToUseVect = new Vector();
                ratioToUseVect.add(ratioHakeInterAnnual);
                ratioToUseVect.add(ratioSoleInterAnnual);
                ratioToUseVect.add(ratioNepInterAnnual);
                //System.out.println("DEBUG REF EFF VARS ANN HAKE RATIO " + ratioHakeInterAnnual);  
                //System.out.println("DEBUG REF EFF VARS ANN SOLE RATIO " + ratioSoleInterAnnual);  
                //System.out.println("DEBUG REF EFF VARS ANN NEP RATIO " + ratioNepInterAnnual);

                // If at least one overshooting is planned, take the smallest ratio (choking may occur)
                // If all overshoot, take the smallest ratio
                // If none overshoot, take the biggest ratio
                double ratioToUse = 0;
                //if(ratioHakeInterAnnual<1 | ratioSoleInterAnnual<1| ratioNepInterAnnual<1){
                    ratioToUse = (double) Collections.min(ratioToUseVect); 
                //}
                //else{
                //    ratioToUse = (double) Collections.max(ratioToUseVect);
                //}
                
                
                //System.out.println("DEBUG RATIO TO USE " + ratioToUse);
                if(year==3){
                    FileUtils.writeStringToFile(TraceTarfs,"tarfName;oldValue;newValue;ratHake;ratSole;RatNep\n",false);
                }
                String string2export="Tarf.Nep"+(2010+year)+";"+(double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0)+";"+(double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                
                //TNEP ; all stocks (but sole, done on TFBE)
                //System.out.println("DEBUG ARE TARFS UPDATED TARF NEP " + (2010+year) + " " + (double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0));
                context.setComputeValue("Tarf.Nep"+(2010+year),(double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0)*ratioToUse);
                //System.out.println("DEBUG ARE TARFS UPDATED TARF NEP " + (2010+year) + " " + (double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0));
                string2export=string2export+"Tarf.TNEP"+(2010+year)+"s1"+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s1",1.0)+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s1",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TNEP"+(2010+year)+"s1",(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s1",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TNEP"+(2010+year)+"s2"+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s2",1.0)+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s2",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TNEP"+(2010+year)+"s2",(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s2",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TNEP"+(2010+year)+"s3"+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s3",1.0)+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s3",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TNEP"+(2010+year)+"s3",(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s3",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TNEP"+(2010+year)+"s4"+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s4",1.0)+";"+(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s4",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TNEP"+(2010+year)+"s4",(double)context.getValueAndCompute("Tarf.TNEP"+(2010+year)+"s4",1.0)*ratioToUse);
                
                //TFBE ; all stocks (but nep, done on TNEP)
                string2export=string2export+"Tarf.TFBESole"+(2010+year)+";"+(double)context.getValueAndCompute("Tarf.TFBESole"+(2010+year),1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBESole"+(2010+year),1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBESole"+(2010+year),(double)context.getValueAndCompute("Tarf.TFBESole"+(2010+year),1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBE"+(2010+year)+"s1"+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s1",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s1",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBE"+(2010+year)+"s1",(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s1",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBE"+(2010+year)+"s2"+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s2",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s2",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBE"+(2010+year)+"s2",(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s2",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBE"+(2010+year)+"s3"+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s3",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s3",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBE"+(2010+year)+"s3",(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s3",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBE"+(2010+year)+"s4"+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s4",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s4",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBE"+(2010+year)+"s4",(double)context.getValueAndCompute("Tarf.TFBE"+(2010+year)+"s4",1.0)*ratioToUse);
                System.out.println("DEBUG RATIO DYN E INETR-ANNUAL TNEP + TFBE " + ratioToUse);
                
                
                Vector ratioToUseVect2 = new Vector();
                ratioToUseVect2.add(ratioHakeInterAnnual);
                ratioToUseVect2.add(ratioSoleInterAnnual);
                //if(ratioHakeInterAnnual<1 | ratioSoleInterAnnual<1){
                    ratioToUse = (double) Collections.min(ratioToUseVect2); 
                //}
                //else{
                //    ratioToUse = (double) Collections.max(ratioToUseVect2);
                //}

                
                //GBOB; hake and sole
                string2export=string2export+"Tarf.GBOBSole"+(2010+year)+";"+(double)context.getValueAndCompute("Tarf.GBOBSole"+(2010+year),1.0)+";"+(double)context.getValueAndCompute("Tarf.GBOBSole"+(2010+year),1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.GBOBSole"+(2010+year),(double)context.getValueAndCompute("Tarf.GBOBSole"+(2010+year),1.0)*ratioToUse);
                string2export=string2export+"Tarf.GBOB"+(2010+year)+"s1"+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s1",1.0)+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s1",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.GBOB"+(2010+year)+"s1",(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s1",1.0)*ratioToUse);
                string2export=string2export+"Tarf.GBOB"+(2010+year)+"s2"+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s2",1.0)+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s2",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.GBOB"+(2010+year)+"s2",(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s2",1.0)*ratioToUse);
                string2export=string2export+"Tarf.GBOB"+(2010+year)+"s3"+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s3",1.0)+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s3",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.GBOB"+(2010+year)+"s3",(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s3",1.0)*ratioToUse);
                string2export=string2export+"Tarf.GBOB"+(2010+year)+"s4"+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s4",1.0)+";"+(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s4",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.GBOB"+(2010+year)+"s4",(double)context.getValueAndCompute("Tarf.GBOB"+(2010+year)+"s4",1.0)*ratioToUse);
                
                ratioToUse = ratioHakeInterAnnual;
                //TFBW; hake        
                string2export=string2export+"Tarf.TFBW"+(2010+year)+"s1"+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s1",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s1",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBW"+(2010+year)+"s1",(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s1",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBW"+(2010+year)+"s2"+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s2",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s2",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBW"+(2010+year)+"s2",(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s2",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBW"+(2010+year)+"s3"+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s3",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s3",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBW"+(2010+year)+"s3",(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s3",1.0)*ratioToUse);
                string2export=string2export+"Tarf.TFBW"+(2010+year)+"s4"+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s4",1.0)+";"+(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s4",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.TFBW"+(2010+year)+"s4",(double)context.getValueAndCompute("Tarf.TFBW"+(2010+year)+"s4",1.0)*ratioToUse);
                
                //LBOB; hake
                string2export=string2export+"Tarf.LBOB"+(2010+year)+"s1"+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s1",1.0)+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s1",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.LBOB"+(2010+year)+"s1",(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s1",1.0)*ratioToUse);
                string2export=string2export+"Tarf.LBOB"+(2010+year)+"s2"+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s2",1.0)+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s2",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.LBOB"+(2010+year)+"s2",(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s2",1.0)*ratioToUse);
                string2export=string2export+"Tarf.LBOB"+(2010+year)+"s3"+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s3",1.0)+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s3",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.LBOB"+(2010+year)+"s3",(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s3",1.0)*ratioToUse);
                string2export=string2export+"Tarf.LBOB"+(2010+year)+"s4"+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s4",1.0)+";"+(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s4",1.0)*ratioToUse+";"+ratioHakeInterAnnual+";"+ratioSoleInterAnnual+";"+ratioNepInterAnnual+"\n";
                context.setComputeValue("Tarf.LBOB"+(2010+year)+"s4",(double)context.getValueAndCompute("Tarf.LBOB"+(2010+year)+"s4",1.0)*ratioToUse);
                System.out.println("DEBUG RATIO DYN E INETR-ANNUAL HAKE AND GBOB SOLE " + ratioToUse);
                
                FileUtils.writeStringToFile(TraceTarfs,string2export,true);
        }
        //System.out.println("DEBUG ARE TARFS UPDATED TARF NEP " + (2010+year) + " " + (double)context.getValueAndCompute("Tarf.Nep"+(2010+year),1.0));
        
        // ADD CONDITION : IF THE TAC WAS ALMOST REACHED LAST MONTH, TAC IS CONSIDERED REACHED THIS MONTH. DO NOT DO THIS IN JANUARY
        if(year>=6){// From 2016, the TAC is a "real" TAC (except for Nephrops)
            if ((catchTons >= TACBOBvalue & ts!=null) | ((int) context.getValue("TAC_hake_close") ==1  & firstIteration)) {
                doTACHake = true;
                doTAC = true;
                if((int) context.getValue("TAC_hake_reached")==12){
                    context.setValue("TAC_hake_reached",step.getMonth().getMonthNumber());
                }
            }
            //System.out.println("CONDITION 8, step : " + step.getStep());
            if ((catchTonsSole >= TACvalueSole) | ((int) context.getValue("TAC_sole_close") ==1  & firstIteration)) {
                doTACSole = true;
                doTAC = true;
                if((int) context.getValue("TAC_sole_reached")==12){
                    context.setValue("TAC_sole_reached",step.getMonth().getMonthNumber());
                }
            }
            //System.out.println("CONDITION 9, step : " + step.getStep()); Its a TAL for Nephrops since no LO applies for this species
            if ((landTonsLang >= TACvalueLang) | ((int) context.getValue("TAC_nep_close") ==1  & firstIteration)) {
                doTACNep = true;
                doTAC = true;
                if((int) context.getValue("TAC_nep_reached")==12){
                    context.setValue("TAC_nep_reached",step.getMonth().getMonthNumber());
                }
            }
            //System.out.println("CONDITION 10, step : " + step.getStep());
        }
        if(year<6){// Until the end of 2015, the TAC is a TAL
            //System.out.println("sum land tons step : " + step.getStep() + " ; "+ landTons);
            //System.out.println("TACBOBvalue step : " + step.getStep() + " ; "+ TACBOBvalue);
            if ((landTons >= TACBOBvalue) | ((int) context.getValue("TAC_hake_close") ==1  & firstIteration)) {
                doTACHake = true;
                doTAC = true;
                if((int) context.getValue("TAC_hake_reached")==12){
                    context.setValue("TAC_hake_reached",step.getMonth().getMonthNumber());
                }
            }
            //System.out.println("doTACHake step : " + step.getStep() + " ; "+ doTACHake);
            if ((landTonsSole >= TACvalueSole) | ((int) context.getValue("TAC_sole_close") ==1  & firstIteration)) {
                doTACSole = true;
                doTAC = true;
                if((int) context.getValue("TAC_sole_reached")==12){
                    context.setValue("TAC_sole_reached",step.getMonth().getMonthNumber());
                }
            }
            if ((landTonsLang >= TACvalueLang) | ((int) context.getValue("TAC_nep_close") ==1  & firstIteration)) {
                doTACNep = true;
                doTAC = true;
                if((int) context.getValue("TAC_nep_reached")==12){
                    context.setValue("TAC_nep_reached",step.getMonth().getMonthNumber());
                }
            }
            //System.out.println("TAC TAD" + TACBOBvalue + "; captures : " + catchTons + " ; rejets : " + discTons + " ; debarquements : " + landTons );
            //System.out.println("TAC TAD" + TACvalueSole + "; captures : " + catchTonsSole + " ; rejets : " + discTonsSole + " ; debarquements : " + landTonsSole );
            //System.out.println("TAC TAD" + TACvalueLang + "; captures : " + catchTonsNep + " ; rejets : " + discTonsLang + " ; debarquements : " + landTonsLang );            
        }

        // ADD CONDITION : IF THE TAC IS NOT REACHED, BUT ALMOST REACHED, REDUCE EFFORT PROPORTIONALLY
        // DO NOT DO IT IN JANUARY
        
        // ADD CONDITION : IF THE TAC WAS ALMOST REACHED LAST MONTH, TAC IS CONSIDERED REACHED THIS MONTH. DO NOT DO THIS IN JANUARY
         if(year>=6){// From 2016, the TAC is a "real" TAC (except for Nephrops)
            if ((catchTons >= TACBOBvalue & ts!=null) | ((int) context.getValue("TAC_hake_close") ==1  & firstIteration)) {
                doTACHake = true;
                doTAC = true;
            }
            //System.out.println("CONDITION 8, step : " + step.getStep());
            if ((catchTonsSole >= TACvalueSole) | ((int) context.getValue("TAC_sole_close") ==1  & firstIteration)) {
                doTACSole = true;
                doTAC = true;
            }
            //System.out.println("CONDITION 9, step : " + step.getStep()); Its a TAL for Nephrops since no LO applies for this species
            if ((landTonsLang >= TACvalueLang) | ((int) context.getValue("TAC_nep_close") ==1  & firstIteration)) {
                doTACNep = true;
                doTAC = true;
            }
            //System.out.println("CONDITION 10, step : " + step.getStep());
        }
        if(year<6){// Until the end of 2015, the TAC is a TAL
            //System.out.println("sum land tons step : " + step.getStep() + " ; "+ landTons);
            //System.out.println("TACBOBvalue step : " + step.getStep() + " ; "+ TACBOBvalue);
            if ((landTons >= TACBOBvalue) | ((int) context.getValue("TAC_hake_close") ==1  & firstIteration)) {
                doTACHake = true;
                doTAC = true;
            }
            //System.out.println("doTACHake step : " + step.getStep() + " ; "+ doTACHake);
            if ((landTonsSole >= TACvalueSole) | ((int) context.getValue("TAC_sole_close") ==1  & firstIteration)) {
                doTACSole = true;
                doTAC = true;
            }
            if ((landTonsLang >= TACvalueLang) | ((int) context.getValue("TAC_nep_close") ==1  & firstIteration)) {
                doTACNep = true;
                doTAC = true;
            }
            //System.out.println("TAC TAD" + TACBOBvalue + "; captures : " + catchTons + " ; rejets : " + discTons + " ; debarquements : " + landTons );
            //System.out.println("TAC TAD" + TACvalueSole + "; captures : " + catchTonsSole + " ; rejets : " + discTonsSole + " ; debarquements : " + landTonsSole );
            //System.out.println("TAC TAD" + TACvalueLang + "; captures : " + catchTonsNep + " ; rejets : " + discTonsLang + " ; debarquements : " + landTonsLang );            
        }

        // ADD CONDITION : IF THE TAC IS NOT REACHED, BUT ALMOST REACHED, REDUCE EFFORT PROPORTIONALLY
        // DO NOT DO IT IN JANUARY
        
        if(step.getMonth().getMonthNumber()!=0 & firstIteration){
            // A/ For each species, compute mean monthly catch or landings this year
            // B/ Compare to remaining catch or landings
            double diffHake = 0;
            double diffSole = 0;
            double diffNep = 0;
            if((int) context.getValue("TAC_hake_close")!=1){
                context.setValue("TAC_close_month_number_hake",12);
            }
            if((int) context.getValue("TAC_sole_close")!=1){
                context.setValue("TAC_close_month_number_sole",12);
            }
            if((int) context.getValue("TAC_nep_close")!=1){
                context.setValue("TAC_close_month_number_nep",12);
            }

            //double propTACHake = 1;
            //double propTACSole = 1;
            //double propTACNep = 1;

            // C/ Flag closeness to TAC
            if(year>=6){
                diffHake = (catchTons/step.getMonth().getMonthNumber()) - (TACBOBvalue - catchTons);
                diffSole = (catchTonsSole/step.getMonth().getMonthNumber()) - (TACvalueSole - catchTonsSole);
                diffNep = (landTonsLang/step.getMonth().getMonthNumber()) - (TACvalueLang - landTonsLang);
                if(diffHake >0 & !doTACHake & (int) context.getValue("TAC_hake_close")==0){
                    context.setValue("TAC_hake_close",1);
                    if((int)  context.getValue("TAC_close_month_number_hake")==12){
                        context.setValue("TAC_close_month_number_hake",step.getMonth().getMonthNumber());
                    }
                    propTACHake = (TACBOBvalue - catchTons) / (catchTons/step.getMonth().getMonthNumber());
                }
                if(diffSole >0 & !doTACSole & (int) context.getValue("TAC_sole_close")==0){
                    context.setValue("TAC_sole_close",1);
                    if((int)  context.getValue("TAC_close_month_number_sole")==12){
                        context.setValue("TAC_close_month_number_sole",step.getMonth().getMonthNumber());
                    }
                    propTACSole = (TACvalueSole - catchTonsSole) / (catchTonsSole/step.getMonth().getMonthNumber());
                }
                if(diffNep >0 & !doTACNep & (int) context.getValue("TAC_nep_close")==0){
                    context.setValue("TAC_nep_close",1);
                    if((int)  context.getValue("TAC_close_month_number_nep")==12){
                        context.setValue("TAC_close_month_number_nep",step.getMonth().getMonthNumber());
                    }
                    propTACNep = (TACvalueLang - landTonsLang) / (landTonsLang/step.getMonth().getMonthNumber());
                }

                /*
                // E/ Define capping proportions to be used the month after the TAC has been considered close. Its purpose is to limit the quantity of discards under LO.
                if(doTACHake){
                    double cappingDiscardsHake = (0.07*catchTons) / (catchTons/step.getMonth().getMonthNumber());
                    context.setValue("cappingDiscardsHake",cappingDiscardsHake);
                }
                if(doTACSole){
                    double cappingDiscardsSole = (0.05*catchTons) / (catchTons/step.getMonth().getMonthNumber());
                    context.setValue("cappingDiscardsSole",cappingDiscardsSole);
                }
                */
                
            }
            if(year<6){
                diffHake = (landTons/step.getMonth().getMonthNumber()) - (TACBOBvalue - landTons);
                diffSole = (landTonsSole/step.getMonth().getMonthNumber()) - (TACvalueSole - landTonsSole);
                diffNep = (landTonsLang/step.getMonth().getMonthNumber()) - (TACvalueLang - landTonsLang);
                if(diffHake >0 & !doTACHake & (int) context.getValue("TAC_hake_close")==0){
                    context.setValue("TAC_hake_close",1);
                    if((int)  context.getValue("TAC_close_month_number_hake")==12){
                        context.setValue("TAC_close_month_number_hake",step.getMonth().getMonthNumber());
                    }
                    propTACHake = (TACBOBvalue - landTons) / (landTons/step.getMonth().getMonthNumber());
                }
                if(diffSole >0 & !doTACSole & (int) context.getValue("TAC_sole_close")==0){
                    context.setValue("TAC_sole_close",1);
                    if((int)  context.getValue("TAC_close_month_number_sole")==12){
                        context.setValue("TAC_close_month_number_sole",step.getMonth().getMonthNumber());
                    }
                    propTACSole = (TACvalueSole - landTonsSole) / (landTonsSole/step.getMonth().getMonthNumber());
                }
                if(diffNep >0 & !doTACNep & (int) context.getValue("TAC_nep_close")==0){
                    context.setValue("TAC_nep_close",1);
                    if((int)  context.getValue("TAC_close_month_number_nep")==12){
                        context.setValue("TAC_close_month_number_nep",step.getMonth().getMonthNumber());
                    }
                    propTACNep = (TACvalueLang - landTonsLang) / (landTonsLang/step.getMonth().getMonthNumber());
                }
            }
        }
        
            // D/ Reduce effort proportionally to remaning catch or landings
            // Multiply by propoTACNep. If the TAC is not to be reduced, the proportion is by default set to 1
            // If a metier is concerned by several reductions, use the biggest reduction
            // DO it only one month per species. The following months, effort reallocation is used
            //DO not use it if the TAC has already been reached
            
         if(step.getMonth().getMonthNumber()!=0 & !(((int) context.getValue("TAC_hake_close") ==0)&((int) context.getValue("TAC_sole_close") ==0)&((int) context.getValue("TAC_nep_close") ==0))){
            if(!((int)context.getValue("TAC_close_month_number_hake")!=step.getMonth().getMonthNumber() & (int)context.getValue("TAC_close_month_number_sole")!=step.getMonth().getMonthNumber() & (int)context.getValue("TAC_close_month_number_nep")!=step.getMonth().getMonthNumber())){ // Evaluate this condition separately to avoid an evaluation when these flags do not exist yet
                // Quelles sont les target species de ce metier ? (pas les primaryCatch) En fonction, determiner la proportion de TAC minimale. Dans tous les cas, le merlu est target species
                double proprotionToApply = 1.0;
                if (ts2 != null & ts3 != null){ // SI MLS cibles
                    Vector proportionTables = new Vector();
                    if((int)context.getValue("TAC_close_month_number_hake")==step.getMonth().getMonthNumber() & !doTACHake){
                        proportionTables.add(propTACHake);
                    }
                    if((int)context.getValue("TAC_close_month_number_sole")==step.getMonth().getMonthNumber() & !doTACSole){
                        proportionTables.add(propTACSole);
                    }
                    if((int)context.getValue("TAC_close_month_number_nep")==step.getMonth().getMonthNumber() & !doTACNep){
                        proportionTables.add(propTACNep);
                    }
                    proportionTables.add(1.0);// Security to vaoid a colleciton of length 0
                    proprotionToApply = (double) Collections.min(proportionTables);
                }
                if (ts2 != null & ts3 == null){ // SI MS cibles
                    Vector proportionTables = new Vector();
                    if((int)context.getValue("TAC_close_month_number_hake")==step.getMonth().getMonthNumber() & !doTACHake){
                        proportionTables.add(propTACHake);
                    }
                    if((int)context.getValue("TAC_close_month_number_sole")==step.getMonth().getMonthNumber() & !doTACSole){
                        proportionTables.add(propTACSole);
                    }
                    proportionTables.add(1.0);// Security to vaoid a colleciton of length 0
                    proprotionToApply = (double) Collections.min(proportionTables);
                    
                }
                if (ts2 == null & ts3 != null){ // SI ML cibles
                    Vector proportionTables = new Vector();
                    if((int)context.getValue("TAC_close_month_number_hake")==step.getMonth().getMonthNumber() & !doTACHake){
                        proportionTables.add(propTACHake);
                    }
                    if((int)context.getValue("TAC_close_month_number_nep")==step.getMonth().getMonthNumber() & !doTACNep){
                        proportionTables.add(propTACNep);
                    }
                    proportionTables.add(1.0);// Security to vaoid a colleciton of length 0
                    proprotionToApply = (double) Collections.min(proportionTables);
                    
                }
                if (ts2 == null & ts3 == null){ // SI M cible
                    proprotionToApply = 1.0;// Security to vaoid a colleciton of length 0
                    if((int)context.getValue("TAC_close_month_number_hake")==step.getMonth().getMonthNumber() & !doTACHake){
                        proprotionToApply = propTACHake;
                    }
                }
            
                //recupere toutes les strategies pratiquant le metier et pour lesquelles la proportion !=0
                SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
                Set<Strategy> strs = new HashSet<Strategy>();
                for (Strategy str : siMatrix.getStrategies(step)) {
                    double prop = str.getStrategyMonthInfo(step.getMonth()).getProportionMetier(metier);
                    if (prop != 0) {
                        strs.add(str);
                    }
                }

                //Pour l'ensemble des strategies pratiquant le metier
                for (Strategy str : strs) {
                    StrategyMonthInfo smi = str.getStrategyMonthInfo(step.getMonth());
                    double newProportion = smi.getProportionMetier(metier)*proprotionToApply;
                    smi.setProportionMetier(metier, newProportion);
                }
            }

         }

        /*
         // AVOID ARTIFACTIAL HIGH DISCARDS IN ONE MONTH : IF AT A PREVIOUS TIME STEP TAC WAS CONSIDERED TO BE CLOSE, DE MINIMIS EXEMPTIONS FOR THE CORRESPONDING SPECIES ARE SET TO 0
        if(step.getMonth().getMonthNumber()!=0){
            if((int)context.getValue("TAC_close_month_number_hake")<=step.getMonth().getMonthNumber()& firstIteration){
                exemptions[step.getYear()] = 0.0;
            }
            if((int)context.getValue("TAC_close_month_number_sole")<=step.getMonth().getMonthNumber()& firstIteration){
                exemptionsSoleTRAWL[step.getYear()] = 0.0;
                exemptionsSoleGILLNET[step.getYear()] = 0.0;
            }
        }
        */
         
        /*            
         *             If TAC applies, set flags on métiers concerning the application of TAC, landing obligation and of de minimis exemptions.
         *             Flags : 0, not concerned by TAC ; 1 : If TAC reached, concerned by TAC and may discard ; 2 : If TAC reached, concerned by TAC and forbidden
         *             If the TAC is not reached, do not use the flags
         */
         /*
        System.out.println("Condition Rule OgiveDeTriMerluRetentionTAC");
        System.out.println("doTAC = " + doTAC);
        System.out.println("doLO[year] = " + doLO[year]);
        System.out.println("exemptions[year] = " + exemptions[year]);
        System.out.println("flagLGEsp = " + flagLGEsp);
        */
        
        //System.out.println("Condition 3 step : " + step.getStep() + " metier : " + metier.getName());
        
            //System.out.println("CONDITION 11, step : " + step.getStep());
            if(doLO[year]){ // If landing obligation            
                context.setValue("priceSwitch"+param_species.getName(),1);
                //System.out.println("CONDITION 12, step : " + step.getStep());
                if(exemptions[year]!=0){ // If there is an exemption
                    double quantMat = 0;
                    double quantMatPop = 0; //Hake
                    double quantMatPopSole = 0; //Sole
                    double quantMatPopDisc = 0; //Hake
                    double quantMatPopSoleDisc = 0; //Sole
                    ResultStorage matResult = context.getSimulationStorage().getResultStorage();
                    SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
                    List<Population> Pops = context.getPopulationMonitor().getPopulations();
                    //System.out.println("CONDITION 13, step : " + step.getStep());
                    for (TimeStep dat = new TimeStep(0); dat.before(step); dat = dat.next()) {
                        if( dat.getYear() == year) {     
                            //System.out.println("CONDITION 13, step dat : " + dat.getStep());
                            //System.out.println("CONDITION 13, step dat : " + dat.getYear());  
                            //System.out.println("CONDITION 13, step dat : " + year);  
                            //System.out.println("CONDITION 13, step dat : " + dat.before(step));                      
                            for(Population onePop:Pops){
                                MatrixND tmp = matResult.getMatrix(dat, onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy();
                                MatrixND tmpDisc = null;
                                if( matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                    tmpDisc = matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                }
                                if (onePop.getSpecies() .getName().equals(param_species.getName())){
                                    if(tmp!=null){
                                        tmp = tmp.getSubMatrix(1, metier); //Select the metier
                                        quantMatPop += tmp.sumAll();
                                    }
                                    if(tmpDisc!=null){
                                        tmpDisc = tmpDisc.getSubMatrix(1, metier); //Select the metier
                                        quantMatPopDisc += tmpDisc.sumAll();
                                    }
                                }
                                if (onePop.getSpecies() .getName().equals(param_species2.getName())){
                                    if(tmp!=null){
                                        tmp = tmp.getSubMatrix(1, metier); //Select the metier
                                        quantMatPopSole += tmp.sumAll();
                                    }
                                    if(tmpDisc!=null){
                                        tmpDisc = tmpDisc.getSubMatrix(1, metier); //Select the metier
                                        quantMatPopSoleDisc += tmpDisc.sumAll();
                                    }
                                }
                            }
                        }
                    }
                    //System.out.println("CONDITION 14, step : " + step.getStep());

                    
                    //System.out.println("Condition 4 step : " + step.getStep() + " metier : " + metier.getName());
                    
                    //System.out.println("Condition 5 step : " + step.getStep() + " metier : " + metier.getName());
                            
                    //System.out.println("DEBUG EX" + ";" + step.getStep() + ";" + quantMat + ";" + quantMatPop + ";" + metier.getName() + ";" + quantMatPop/ quantMat );
                    //System.out.println("doTAC " +doTAC+ " doLO[year] " + doLO[year] + " exemptions[year] " + exemptions[year] );
                    // compute %  
                    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue + ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "before flag");                    
                    
                    System.out.println("step : " + step.getStep() + "TACBOBvalue : " + TACBOBvalue + "TACvalueSole : " + TACvalueSole + "TACvalueLang : " + TACvalueLang + "catchTons hake : " + catchTons + "catchTonsSole sole : " + catchTonsSole + "landTonsLang nep : " + landTonsLang +  ": doTACHake : " + doTACHake + ": doTACSole : " + doTACSole + ": doTACNep : " + doTACNep + ": quantMatPop : " + quantMatPop + ": quantMatPopDisc : " + quantMatPopDisc + ": quantMatPopSole : " + quantMatPopSole + ": quantMatPopSoleDisc : " + quantMatPopSoleDisc + ": doLO[year] : " + doLO[year] + ": exemptions[year] : " + exemptions[year] + " metier : " + metier.getName() + "flag DEBUG");
                    if(quantMatPopDisc > exemptions[year]* quantMatPop & doTACHake & !(metier.getName().equals("Metier_FiletSole_NordC")||metier.getName().equals("Metier_FiletSole_NordIntPC")||metier.getName().equals("Metier_FiletSole_InterSudPC")||metier.getName().equals("Metier_FiletSole_InterC")||metier.getName().equals("Metier_PalangreMerlu_InterSudAC")||metier.getName().equals("Metier_PalangreMixte_NordC")||metier.getName().equals("Metier_PalangreMixte_InterC")||metier.getName().equals("Metier_FiletMerlu_NordC")||metier.getName().equals("Metier_FiletMerlu_NordPC")||metier.getName().equals("Metier_FiletMixte_NordPC")||metier.getName().equals("Metier_FiletMixte_NordC")||metier.getName().equals("Metier_FiletMixte_NordInterPC")||metier.getName().equals("Metier_FiletMixte_InterSudC")||metier.getName().equals("Metier_FiletMIxte_InterC")||metier.getName().equals("Metier_FiletMerlu_InterSudAPC")||metier.getName().equals("Metier_FiletMerlu_InterSudC")||metier.getName().equals("Metier_FiletMerlu_InterSudPC")||metier.getName().equals("Metier_PalangreMerlu_InterSudC"))) { //Exemption Hake ONLY FOR TRAWLERS
                        context.setValue(metier.getName()+param_species.getName()+"flag",2); //Forbidden
                        // No exemption to avoid irrealistic simulated values.
                        // Exception : until 2019, Nephrops  metiers not targeting hake (ie fishing only in the Nephrops zone) are not affected by landing obligation and may discard.
                        //if(year <9 & metier.getName().equals("Metier_Lang_InterC")& doTACHake){
                        //        context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                        //    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ "; metier : " + metier.getName() + "flag 1");
                        //}
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue + ": exemption : "+ exemptions[year] + "; tot disc metier : " +quantMatPopDisc + "; tot catch metier : " +quantMatPop + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "flag 2");
                        System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                    }
                    if (quantMatPopDisc <= exemptions[year]* quantMatPop & doTACHake & !(metier.getName().equals("Metier_FiletSole_NordC")||metier.getName().equals("Metier_FiletSole_NordIntPC")||metier.getName().equals("Metier_FiletSole_InterSudPC")||metier.getName().equals("Metier_FiletSole_InterC")||metier.getName().equals("Metier_PalangreMerlu_InterSudAC")||metier.getName().equals("Metier_PalangreMixte_NordC")||metier.getName().equals("Metier_PalangreMixte_InterC")||metier.getName().equals("Metier_FiletMerlu_NordC")||metier.getName().equals("Metier_FiletMerlu_NordPC")||metier.getName().equals("Metier_FiletMixte_NordPC")||metier.getName().equals("Metier_FiletMixte_NordC")||metier.getName().equals("Metier_FiletMixte_NordInterPC")||metier.getName().equals("Metier_FiletMixte_InterSudC")||metier.getName().equals("Metier_FiletMIxte_InterC")||metier.getName().equals("Metier_FiletMerlu_InterSudAPC")||metier.getName().equals("Metier_FiletMerlu_InterSudC")||metier.getName().equals("Metier_FiletMerlu_InterSudPC")||metier.getName().equals("Metier_PalangreMerlu_InterSudC"))) { //EXEMPTION ONLY FOR TRAWLERS
                        if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                            context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                            System.out.println("FLAG 1 :  metier : " + metier.getName() + " step : " +step.getStep());
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }
                    if(quantMatPopDisc > exemptions[year]* quantMatPop & !doTACHake & !(metier.getName().equals("Metier_FiletSole_NordC")||metier.getName().equals("Metier_FiletSole_NordIntPC")||metier.getName().equals("Metier_FiletSole_InterSudPC")||metier.getName().equals("Metier_FiletSole_InterC")||metier.getName().equals("Metier_PalangreMerlu_InterSudAC")||metier.getName().equals("Metier_PalangreMixte_NordC")||metier.getName().equals("Metier_PalangreMixte_InterC")||metier.getName().equals("Metier_FiletMerlu_NordC")||metier.getName().equals("Metier_FiletMerlu_NordPC")||metier.getName().equals("Metier_FiletMixte_NordPC")||metier.getName().equals("Metier_FiletMixte_NordC")||metier.getName().equals("Metier_FiletMixte_NordInterPC")||metier.getName().equals("Metier_FiletMixte_InterSudC")||metier.getName().equals("Metier_FiletMIxte_InterC")||metier.getName().equals("Metier_FiletMerlu_InterSudAPC")||metier.getName().equals("Metier_FiletMerlu_InterSudC")||metier.getName().equals("Metier_FiletMerlu_InterSudPC")||metier.getName().equals("Metier_PalangreMerlu_InterSudC"))) {
                        context.setValue(metier.getName()+"landHake",1); //Must land catch 
                        System.out.println("FLAG 1 must land :  metier : " + metier.getName() + " step : " +step.getStep());
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "flag land catch");
                    }    
                    if(!doTACHake & (metier.getName().equals("Metier_FiletSole_NordC")||metier.getName().equals("Metier_FiletSole_NordIntPC")||metier.getName().equals("Metier_FiletSole_InterSudPC")||metier.getName().equals("Metier_FiletSole_InterC")||metier.getName().equals("Metier_PalangreMerlu_InterSudAC")||metier.getName().equals("Metier_PalangreMixte_NordC")||metier.getName().equals("Metier_PalangreMixte_InterC")||metier.getName().equals("Metier_FiletMerlu_NordC")||metier.getName().equals("Metier_FiletMerlu_NordPC")||metier.getName().equals("Metier_FiletMixte_NordPC")||metier.getName().equals("Metier_FiletMixte_NordC")||metier.getName().equals("Metier_FiletMixte_NordInterPC")||metier.getName().equals("Metier_FiletMixte_InterSudC")||metier.getName().equals("Metier_FiletMIxte_InterC")||metier.getName().equals("Metier_FiletMerlu_InterSudAPC")||metier.getName().equals("Metier_FiletMerlu_InterSudC")||metier.getName().equals("Metier_FiletMerlu_InterSudPC")||metier.getName().equals("Metier_PalangreMerlu_InterSudC"))) { // longliners and gillnetters have no exemption
                        context.setValue(metier.getName()+"landHake",1); //Must land catch 
                        System.out.println("FLAG 1 must land :  metier : " + metier.getName() + " step : " +step.getStep());
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "flag land catch");
                    }   
                    //System.out.println("Condition 6 step : " + step.getStep() + " metier : " + metier.getName());
                    if (doTACHake & (metier.getName().equals("Metier_FiletSole_NordC")||metier.getName().equals("Metier_FiletSole_NordIntPC")||metier.getName().equals("Metier_FiletSole_InterSudPC")||metier.getName().equals("Metier_FiletSole_InterC")||metier.getName().equals("Metier_PalangreMerlu_InterSudAC")||metier.getName().equals("Metier_PalangreMixte_NordC")||metier.getName().equals("Metier_PalangreMixte_InterC")||metier.getName().equals("Metier_FiletMerlu_NordC")||metier.getName().equals("Metier_FiletMerlu_NordPC")||metier.getName().equals("Metier_FiletMixte_NordPC")||metier.getName().equals("Metier_FiletMixte_NordC")||metier.getName().equals("Metier_FiletMixte_NordInterPC")||metier.getName().equals("Metier_FiletMixte_InterSudC")||metier.getName().equals("Metier_FiletMIxte_InterC")||metier.getName().equals("Metier_FiletMerlu_InterSudAPC")||metier.getName().equals("Metier_FiletMerlu_InterSudC")||metier.getName().equals("Metier_FiletMerlu_InterSudPC")||metier.getName().equals("Metier_PalangreMerlu_InterSudC"))) { //EXEMPTION ONLY FOR TRAWLERS
                        if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){// longline and gillnets: no exemption; metier forbidden once the TAC is reached
                            context.setValue(metier.getName()+param_species.getName()+"flag",2); // Forbidden
                            System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }


                    //Flag the spanish longliners and gillnetters: no exemption;  metier forbidden once the TAC is reached
                    if (flagLGEsp &  doTACHake){ // 2.260160e-01 = SpLG proportion of catch
                        context.setValue("SpLG"+param_species.getName()+"flag",2); //Forbidden
                        System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                        //System.out.println("FLAG" + "SpLG"+param_species.getName()+"flag");
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch() + ": exemption : "+ exemptions[year] + "; rate : " + quantMatPopSpLG + "; metier : " + "SpLG" + "flag 2");
                    }
                    //System.out.println("Condition 7 step : " + step.getStep() + " metier : " + metier.getName());
                    //System.out.println("CONDITION 15, step : " + step.getStep());
                    if(quantMatPopSoleDisc > exemptionsSoleTRAWL[year]* quantMatPopSole & doTACSole & (metier.getName().equals("Metier_Lang_NordPC") |metier.getName().equals("Metier_Lang_InterC") |metier.getName().equals("Metier_Lang_InterPC") |metier.getName().equals("Metier_ChalutMixte_APCS") |metier.getName().equals("Metier_ChalutMixte_NordC") |metier.getName().equals("Metier_ChalutSole_InterSudC") |metier.getName().equals("Metier_ChalutSole_InterC") |metier.getName().equals("Metier_ChalutSole_NordCet") |metier.getName().equals("Metier_ChalutBenth_APCS") |metier.getName().equals("Metier_ChalutBenth_NordC") |metier.getName().equals("Metier_ChalutBenth_NordAPC") |metier.getName().equals("Metier_ChaluMixte_InterC") |metier.getName().equals("Metier_ChalutMixte_NordPC"))) { //Exemption Sole TRAWL
                        context.setValue(metier.getName()+param_species.getName()+"flag",2); //Forbidden
                        System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch() + ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "flag 2");
                    }
                    if ((quantMatPopSoleDisc <= exemptionsSoleTRAWL[year]* quantMatPopSole) & doTACSole & (metier.getName().equals("Metier_Lang_NordPC") |metier.getName().equals("Metier_Lang_InterC") |metier.getName().equals("Metier_Lang_InterPC") |metier.getName().equals("Metier_ChalutMixte_APCS") |metier.getName().equals("Metier_ChalutMixte_NordC") |metier.getName().equals("Metier_ChalutSole_InterSudC") |metier.getName().equals("Metier_ChalutSole_InterC") |metier.getName().equals("Metier_ChalutSole_NordCet")|metier.getName().equals("Metier_ChalutBenth_APCS") |metier.getName().equals("Metier_ChalutBenth_NordC") |metier.getName().equals("Metier_ChalutBenth_NordAPC") |metier.getName().equals("Metier_ChaluMixte_InterC") |metier.getName().equals("Metier_ChalutMixte_NordPC"))){
                        if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                            context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                            System.out.println("FLAG 1 :  metier : " + metier.getName() + " step : " +step.getStep());
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch()+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }
                    //System.out.println("Condition 8 step : " + step.getStep() + " metier : " + metier.getName());
                    //System.out.println("CONDITION 16, step : " + step.getStep());
                    if(quantMatPopSoleDisc > exemptionsSoleGILLNET[year]* quantMatPopSole & doTACSole & (metier.getName().equals("Metier_FiletMerlu_InterSudPC") |metier.getName().equals("Metier_FiletMerlu_InterSudC") |metier.getName().equals("Metier_FiletMerlu_InterSudAPC") |metier.getName().equals("Metier_FiletMIxte_InterC") |metier.getName().equals("Metier_FiletMixte_InterSudC") |metier.getName().equals("Metier_FiletMixte_NordInterPC") |metier.getName().equals("Metier_FiletMixte_NordC") |metier.getName().equals("Metier_FiletMixte_NordPC") |metier.getName().equals("Metier_FiletMerlu_NordPC") |metier.getName().equals("Metier_FiletSole_NordC") |metier.getName().equals("Metier_FiletSole_InterC") |metier.getName().equals("Metier_FiletSole_InterSudPC") |metier.getName().equals("Metier_FiletSole_NordIntPC"))) { //Exemption Sole GILLNET
                        context.setValue(metier.getName()+param_species.getName()+"flag",2); //Forbidden
                        System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch() + ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat + "; metier : " + metier.getName() + "flag 2");
                    }
                    if ((quantMatPopSoleDisc <= exemptionsSoleGILLNET[year]* quantMatPopSole) & doTACSole & (metier.getName().equals("Metier_FiletMerlu_InterSudPC") |metier.getName().equals("Metier_FiletMerlu_InterSudC") |metier.getName().equals("Metier_FiletMerlu_InterSudAPC") |metier.getName().equals("Metier_FiletMIxte_InterC") |metier.getName().equals("Metier_FiletMixte_InterSudC") |metier.getName().equals("Metier_FiletMixte_NordInterPC") |metier.getName().equals("Metier_FiletMixte_NordC") |metier.getName().equals("Metier_FiletMixte_NordPC") |metier.getName().equals("Metier_FiletMerlu_NordPC") |metier.getName().equals("Metier_FiletSole_NordC") |metier.getName().equals("Metier_FiletSole_InterC") |metier.getName().equals("Metier_FiletSole_InterSudPC") |metier.getName().equals("Metier_FiletSole_NordIntPC"))) {
                        if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                            context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                            System.out.println("FLAG 1 :  metier : " + metier.getName() + " step : " +step.getStep());
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch()+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }
                    //System.out.println("Condition 9 step : " + step.getStep() + " metier : " + metier.getName());
                    //System.out.println("CONDITION 17, step : " + step.getStep());
                    if (doTACNep) {//Nephrops : no landing obligation, do as is done for other species without LO
                        if (ts3 != null && ts3.getPrimaryCatch()){
                            context.setValue(metier.getName()+param_species.getName()+"flag",2); // 
                            System.out.println("FLAG 2 :  metier : " + metier.getName() + " step : " +step.getStep());
                        }
                        if ((ts3 != null)&& (!ts3.getPrimaryCatch())){
                            if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                                context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard, No LO for Nephrops
                                System.out.println("FLAG 1 :  metier : " + metier.getName() + " step : " +step.getStep());
                            }
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch()+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }
                    //System.out.println("CONDITION 18, step : " + step.getStep());
                    //System.out.println("Condition 10 step : " + step.getStep() + " metier : " + metier.getName());
                    
                   
                    
                }
                
                //System.out.println("CONDITION 19, step : " + step.getStep());
                
                if(exemptions[year]==0){  // strict application : no exemption
                    //System.out.println("No exemption!!!");
                    double quantMatPop = 0; //Hake
                    double quantMatPopSole = 0; //Sole
                    double quantMatPopSpLG = 0;
                    ResultStorage matResult = context.getSimulationStorage().getResultStorage();
                    SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
                    List<Population> Pops = context.getPopulationMonitor().getPopulations();
                    //System.out.println("CONDITION 20, step : " + step.getStep());
                    for (TimeStep dat = new TimeStep(0); dat.before(step); dat = dat.next()) {
                        if( dat.getYear() == year) {                           
                            for(Population onePop:Pops){
                                MatrixND tmp = matResult.getMatrix(dat, onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy();
                                //System.out.println("NOM POP : " + pop.getName() + " NOM SPEC : " + param_species.getName());
                                if (onePop.getSpecies() .getName().equals(param_species.getName())){
                                    if(tmp!=null){
                                        tmp = tmp.getSubMatrix(1, metier); //Select the metier
                                        quantMatPop += tmp.sumAll();
                                    }
                                }
                                if (onePop.getSpecies() .getName().equals(param_species2.getName())){
                                    if(tmp!=null){
                                        tmp = tmp.getSubMatrix(1, metier); //Select the metier
                                        quantMatPopSole += tmp.sumAll();
                                    }
                                }
                            }
                        }
                    }
                    
                    // metier catch hake ?                     
                    if(quantMatPop > 0.0 & doTACHake) { // yes -> forbidden
                        context.setValue(metier.getName()+param_species.getName()+"flag",2); // Forbidden
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ ";hake caught in tons : " + quantMatPop + "; metier : " + metier.getName() + "flag 2");
                    } // else can still fish flag stays at 0
                    //System.out.println("CONDITION 21, step : " + step.getStep());                     
                    if(quantMatPopSole > 0.0 & doTACSole) { // yes -> forbidden
                        context.setValue(metier.getName()+param_species.getName()+"flag",2); // Forbidden
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ ";hake caught in tons : " + quantMatPop + "; metier : " + metier.getName() + "flag 2");
                    }
                    //System.out.println("CONDITION 22, step : " + step.getStep());                     
                    if (doTACNep) {//Nephrops : no landing obligation, do as is done for other species without LO
                        if (ts3 != null && ts3.getPrimaryCatch()){
                            context.setValue(metier.getName()+param_species.getName()+"flag",2); // 
                        }
                        if ((ts3 != null) && (!ts3.getPrimaryCatch())) {
                            if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                                context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard, No LO for Nephrops
                            }
                        }
                        //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TACvalue+ "; primary target species : " + ts.getPrimaryCatch()+ ": exemption : "+ exemptions[year] + "; rate : " + quantMatPop/ quantMat + "; quantMatPop : " + quantMatPop + "; quantMat : " +  quantMat +   "; metier : " + metier.getName() + "flag 1");
                    }
                    //System.out.println("CONDITION 23, step : " + step.getStep());
                    //Flag the spanish longliners and gillnetters
                    if (flagLGEsp){
                        if(otherCatchTons > 0.0 & doTACHake) {
                            context.setValue("SpLG"+param_species.getName()+"flag",2); //Forbidden
                            //System.out.println("FLAG" + "SpLG"+param_species.getName()+"flag");
                            //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch() + ": exemption : "+ exemptions[year] + "; hake caught : " + quantMatPopSpLG + "; metier : " + metier.getName() + "flag 2");
                        }else if (doTACHake) {
                            context.setValue("SpLG"+param_species.getName()+"flag",1); // May discard
                            //System.out.println("FLAG" + "SpLG"+param_species.getName()+"flag");
                            //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ ": exemption : "+ exemptions[year] + "; hake caught : " + quantMatPopSpLG +  "; metier : " + metier.getName() + "flag 1");
                        }    
                    }
                    //System.out.println("CONDITION 24, step : " + step.getStep());
                    // No exemption to avoid unrealistic values.
                    // Exception : until 2019, Nephorps  metiers not targeting hake (ie fishing only in the Nephrops zone) are not affected by landing obligation and may discard.
                    //if(year <9 & metier.getName().equals("Metier_Lang_InterC")){
                    //    context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                    //    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ "; metier : " + metier.getName() + "flag 1");
                    //}
                    //System.out.println("CONDITION 25, step : " + step.getStep());
                }
            }
            if(!doLO[year]) { // If no landing obligation
                //System.out.println("CONDITION 26, step : " + metier.getName());
                //System.out.println("No LO!!!");  
                if (ts != null && ts.getPrimaryCatch() && doTACHake) { // HAKE
                    // recupere tous les metiers qui ont l'espece en capture principale =>metiers vises
                    context.setValue(metier.getName()+param_species.getName()+"flag",2); // 
                    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ "; metier : " + metier.getName() + "flag 2");
                }
                if (((ts != null) && (!ts.getPrimaryCatch())) && doTACHake){
                    if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                        context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                    }                                                      
                }
                //System.out.println("CONDITION 27, step : " + step.getStep());
                if (ts2 != null && ts2.getPrimaryCatch() && doTACSole) { // SOLE
                    // recupere tous les metiers qui ont l'espece en capture principale =>metiers vises
                    context.setValue(metier.getName()+param_species.getName()+"flag",2); // 
                    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ "; metier : " + metier.getName() + "flag 2");
                }
                if (((ts2 != null) && (!ts2.getPrimaryCatch())) && doTACSole){
                    if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                        context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                    }                                                      
                }
                //System.out.println("CONDITION 28, step : " + step.getStep());
                if (ts3 != null && ts3.getPrimaryCatch() && doTACNep) { // LANG
                    // recupere tous les metiers qui ont l'espece en capture principale =>metiers vises
                    context.setValue(metier.getName()+param_species.getName()+"flag",2); // 
                    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch()+ "; metier : " + metier.getName() + "flag 2");
                }
                if (((ts3 != null) && (!ts3.getPrimaryCatch())) && doTACNep){
                    if ((int) context.getValue(metier.getName()+param_species.getName()+"flag") !=2){
                        context.setValue(metier.getName()+param_species.getName()+"flag",1); // May discard
                    }                                                      
                }
                //System.out.println("CONDITION 29, step : " + step.getStep());import static java.nio.charset.StandardCharsets.UTF_8;
                if (flagLGEsp && doTACHake){
                    context.setValue("SpLG"+param_species.getName()+"flag",2); //Forbidden, as this metier has hake as main catch
                    //System.out.println("FLAG" + "SpLG"+param_species.getName()+"flag");
                    //System.out.println("step : " + step.getStep() + ": doTAC : " + doTAC + "; cum catch : " +catchTons+ "; TAC : " +TAC[year]+ "; primary target species : " + ts.getPrimaryCatch() + ": exemption : "+ exemptions[year] + "; metier : " + metier.getName() + "flag 2");  
                }
                //System.out.println("CONDITION 30, step : " + step.getStep());
                //System.out.println("Condition 12 step : " + step.getStep() + " metier : " + metier.getName());
            }
               
        //Debug only, always comment this
        String toExportflag = step.getStep() + ";" + metier.getName()+ ";" + context.getValue(metier.getName()+param_species.getName()+"flag") + "\n"; // Test to check flagging and preAction and postActions. Comment it.
        //System.out.println("Condition 13 step : " + step.getStep() + " metier : " + metier.getName());        
        if (flagLGEsp){
            toExportflag += step.getStep() + ";" + "SpLG"+param_species.getName()+"flag" + ";" + context.getValue("SpLG"+param_species.getName()+"flag") + "\n";
        }
        //System.out.println("Condition 14 step : " + step.getStep() + " metier : " + metier.getName());
        FileUtils.writeStringToFile(metierFlags,toExportflag,true);
        flagLGEsp = false; //Spanish longliners and gillnetters have been flagged
        /*
        System.out.println("Fin condition Rule OgiveDeTriMerluRetentionTAC");
        System.out.println("doTAC = " + doTAC);
        System.out.println("doLO[year] = " + doLO[year]);
        System.out.println("exemptions[year] = " + exemptions[year]);
        System.out.println("flagLGEsp = " + flagLGEsp);
        */
        
        context.setValue("firstIteration_"+step.getStep(),false);
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est execut
          * temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier) throws Exception {
        System.out.println("PREACTION 1 step : " + step.getStep());
        // preAction linked to the TACs : how to re-allocate effort between metiers?
        
        affectation = true;
        flagLGEsp= true;
        //double cappingProportion = 1.0;
       
        if(affectationPreAction){
            System.out.println("PREACTION 2 step : " + step.getStep());
            List <Metier> metierList  = context.getSimulationStorage().getFisheryRegion(context.getDB()).getMetier();//Get metier list
            for(Metier metierLoop : metierList){
                if((int) context.getValue(metierLoop.getName()+param_species.getName()+"flag") == 2) {
                    context.getMetierMonitor().addforbiddenMetier(metierLoop);
                }
            }
                /*
                if(step.getYear()>=6){
                    // CAPPING DISCARDS UNDER LO (AVOIDING AN ARTIFACT)
                    if(doTACHake & !doTACSole){
                        cappingProportion = (double) context.getValue("cappingDiscardsHake");
                    }
                    if(doTACSole & !doTACHake){
                        cappingProportion = (double) context.getValue("cappingDiscardsSole");
                    }
                    if(doTACSole & doTACHake){
                        cappingProportion = (double) context.getValue("cappingDiscardsHake");
                        if((double) context.getValue("cappingDiscardsHake") > (double) context.getValue("cappingDiscardsSole")){
                            cappingProportion = (double) context.getValue("cappingDiscardsSole");    
                        }
                    }
                }
                */
        
            /*            
             *             Re-allocate effort between metiers for flag 2 métiers (forbidden because of TAC and/or landing obligation)
             *             This is not done here for spanish longliners and gillnetters! Their flag is used in the dedicated management rule. If flag =1, nothing happens. If flag = 2, the métier does not fish, its effort is NOT reallocated.
             */  
             
            if(doTAC){
                System.out.println("[TAC] preAction for step " + step.getStep());
                //System.out.println(" TAC atteint [TAC] preAction for: " + metier);
                TargetSpecies ts = metier.getMetierSeasonInfo(step.getMonth()).getSpeciesTargetSpecies(param_species);
                for(Metier metierLoop : metierList){
                    if((int) context.getValue(metierLoop.getName()+param_species.getName()+"flag") == 2) { // If métier is forbidden, re-allocate its effort in other métiers if possible, else allocate its effort to non activity
                        // recupere tous les metiers qui ont l'espece en capture principale =>metiers vises
                        System.out.println("PREACTION 3 metier : " + metierLoop.getName() + " step " + step.getStep());
                        //context.getMetierMonitor().addforbiddenMetier(metier);
            
                        //recupere toutes les strategies pratiquant le metier et pour lesquelles la proportion !=0
                        SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
                        Set<Strategy> strs = new HashSet<Strategy>();
                        for (Strategy str : siMatrix.getStrategies(step)) {
                            double prop = str.getStrategyMonthInfo(step.getMonth()).getProportionMetier(metierLoop);
                            if (prop != 0) {
                                strs.add(str);
                            }
                        }
        
                        //Pour l'ensemble des strategies pratiquant le metier interdit
                        for (Strategy str : strs) {
                            StrategyMonthInfo smi = str.getStrategyMonthInfo(step.getMonth());
                            
                            // 1er cas de figure: l'ensemble des metiers ayant le meme engin que le metier interdit, mais qui n'ont pas le merlu comme espece principale.
                            Set<Metier> possibleMetierCase1 = new HashSet<Metier>();
                            // second cas de figure: l'ensemble des metiers pratiqués avec une proportion non nulle par les bateaux de la flottille 
                            Set<Metier> possibleMetierCase2 = new HashSet<Metier>();
                            // 3 eme cas de figure: l'ensemble des metiers possibles pour les bateaux de la flottille, qui ne sont pas le metiers "nonActivity"
                            Set<Metier> possibleMetierCase3 = new HashSet<Metier>();
            
                            for (EffortDescription effort : str.getSetOfVessels().getPossibleMetiers()) {
                                Metier newMetier = effort.getPossibleMetiers();
                                // Re-alloocate if we are not working on nonActivity, and re-allocate only to not forbidden métiers different from nonActivite
                                if ((!metierLoop.getName().equalsIgnoreCase("nonActivite")) & ((int) context.getValue(newMetier.getName()+param_species.getName()+"flag") != 2) & !newMetier.getName().equals("nonActivite")) {
                                    possibleMetierCase3.add(newMetier);
                                    if (smi.getProportionMetier(newMetier) != 0) {
                                        possibleMetierCase2.add(newMetier);
                                        if (metierLoop.getGear().equals(newMetier.getGear())) {
                                            possibleMetierCase1.add(newMetier);
                                        }
                                    }
                                }
                            }
        
                            //Liste des metiers possibles en fonction du cas
                            Set<Metier> possibleMetier = new HashSet<Metier>();
                            if (possibleMetierCase1.size() != 0) {
                                //System.out.println("[TAC] Use case 1");
                                possibleMetier = possibleMetierCase1;
                            } else if (possibleMetierCase2.size() != 0) {
                                //System.out.println("[TAC] Use case 2");
                                possibleMetier = possibleMetierCase2;
                            } else if (possibleMetierCase3.size() != 0) {
                                //System.out.println("[TAC] Use case 3");
                                possibleMetier = possibleMetierCase3;
                            }
        
                            // Debug file, always comment these lines
                            String toExportPossibleMetier = "";
                            String toExportPossibleMetierProp = "";
                            if (possibleMetier.size() != 0){
                                Iterator<Metier> metTemp = possibleMetier.iterator();
                                while (metTemp.hasNext()) {
                                    Metier nextMetier = metTemp.next();
                                    toExportPossibleMetier = toExportPossibleMetier + nextMetier.getName();
                                    toExportPossibleMetierProp = toExportPossibleMetierProp + smi.getProportionMetier(nextMetier);
                                    if(metTemp.hasNext()){
                                        toExportPossibleMetier = toExportPossibleMetier + "," ;
                                        toExportPossibleMetierProp = toExportPossibleMetierProp + "," ;
                                    }
                                }
                            }
                            String toExportStrMetProp = step.getStep() + ";" + metierLoop.getName()+ ";" + str.getName() + ";" + smi.getProportionMetier(metierLoop) + ";" + toExportPossibleMetier  + ";" + toExportPossibleMetierProp + "\n"; // Test to check flagging and preAction and postActions. Comment it.
                            FileUtils.writeStringToFile(strMetBefore,toExportStrMetProp, UTF_8, true);
        
                            //S'il y a des metiers alternatifs possibles
                            if (possibleMetier.size() != 0) {
                                // on repartit maintenant l'effort entre les differents metiers
                                // possibles dans la meme strategie si un metier possible existe
                                // bien la repartion est proportionnelle a l'effort deja alloue
                                // dans la strategie
                                    
                                double somme = 0;
                                for (Metier met : possibleMetier) {
                                    somme += smi.getProportionMetier(met);
                                }
                                for (Metier met : possibleMetier) {
                                    double newProportion;
                                    if (somme == 0) {
                                        // cas ou aucun autre metier n'est pratique, on repartit quand meme l'effort equitablement sur tous les autres metier
                                        newProportion = smi.getProportionMetier(metierLoop) / possibleMetier.size();
                                    } else {
                                        newProportion = smi.getProportionMetier(met) + (smi.getProportionMetier(metierLoop) * smi.getProportionMetier(met) / somme);
                                    }
        
                                    smi.setProportionMetier(met, newProportion);
                                }
                                smi.setProportionMetier(metierLoop, 0); //le metier vise a alors une proportion nulle 
                                //System.out.println("[TAC] il y a des metiers possibles");
                            } else {
                                //System.out.println("[TAC] Use no activity");
            
                                // sinon on met tout dans le metier nonActivite
                                MetierMonitor metierMon = context.getMetierMonitor();
                                MatrixND mat = metierMon.getOrCreateNoActivity(step,MatrixNoActivity.NAME, siMatrix.getStrategies(step), siMatrix.getMetiers(step));
                                mat.setValue(str, metierLoop, smi.getProportionMetier(metierLoop));
                                smi.setProportionMetier(metierLoop, 0);
                            }
                        }
                    }
                }
                                /*
                                // IN CASE OF LO, compute the maximal effort possible not to reach the de minimis exemption, and cap the effort allocated to the metier to that maximal effort
                                if(step.getYear()>=6 & TAC reached for a given species){
    
                                    if capture null or 0 => effort max =0
    
                                    if not, compute the quantity of discard this metier can still do, do the ratio allowable_discard / mean_monthly_catch.
                                    Apply that ratio to the sum of effort allocation from Jan to month n-1 for this strategy metier. This is the maximum.
                                */
                                int year = step.getYear();
                    System.out.println("PREACTION 4 step " + step.getStep());
                 if(doLO[year]){
                    Vector monthsVector = new Vector();
                    monthsVector.add((int)context.getValue("TAC_close_month_number_hake"));
                    monthsVector.add((int)context.getValue("TAC_hake_reached"));
                    monthsVector.add((int)context.getValue("TAC_close_month_number_sole"));
                    monthsVector.add((int)context.getValue("TAC_sole_reached"));
                    monthsVector.add((int)context.getValue("TAC_close_month_number_nep"));
                    monthsVector.add((int)context.getValue("TAC_nep_reached"));
                    monthsVector.add(step.getMonth().getMonthNumber());
                    int minMonth = (int) Collections.min(monthsVector);
                    System.out.println("PREACTION 5 step " + step.getStep());
                    for(Metier metierLoop : metierList){
                                double effortMax =1;
                                double quantMatPopHake = 0; //Hake
                                double quantMatPopDiscHake = 0; //Hake
                                double quantMatPopSole = 0; //Sole
                                double quantMatPopDiscSole = 0; //Sole
                                double catchAllowedHake =0;
                                double ratioHake = 0;
                                double catchAllowedSole = 0;
                                double ratioSole =0;
                        System.out.println("PREACTION 6 metier : " + metierLoop.getName() + " step " + step.getStep());
                        System.out.println("PREACTION 6 metier flag : " + context.getValue(metierLoop.getName()+param_species.getName()+"flag"));
                        if((int) context.getValue(metierLoop.getName()+param_species.getName()+"flag") != 2) {    // In case of LO and reached TAC, reduce effort of metiers who are not forbidden and are susceptible to have a very high effort, trigerring high discards           
                            System.out.println("PREACTION 7 metier : " + metierLoop.getName() + " step " + step.getStep());
                            if(doTACHake){
                                System.out.println("PREACTION 8 metier : " + metierLoop.getName() + " step " + step.getStep());
                                ResultStorage matResult = context.getSimulationStorage().getResultStorage();
                                List<Population> Pops = context.getPopulationMonitor().getPopulations();
                                for (TimeStep dat = new TimeStep(0); dat.before(step); dat = dat.next()) {
                                    if( dat.getYear() == year) {                          
                                        for(Population onePop:Pops){
                                            MatrixND tmp = matResult.getMatrix(dat, onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy();
                                            MatrixND tmpDisc = null;
                                            if( matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                                tmpDisc = matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                            }
                                            if (onePop.getSpecies() .getName().equals(param_species.getName())){
                                                if(tmp!=null){
                                                    tmp = tmp.getSubMatrix(1, metierLoop); //Select the metier
                                                    quantMatPopHake += tmp.sumAll();
                                                }
                                                if(tmpDisc!=null){
                                                    tmpDisc = tmpDisc.getSubMatrix(1, metierLoop); //Select the metier
                                                    quantMatPopDiscHake += tmpDisc.sumAll();
                                                }
                                            }
                                        }
                                    }
                                }
                                if (quantMatPopHake!=0){
                                    catchAllowedHake = exemptions[year]*quantMatPopHake - quantMatPopDiscHake;
                                    ratioHake=catchAllowedHake/(quantMatPopHake/minMonth); // to avoid to high of an effort, the number of months accounted for is the month the TAC is nearly reached or reached
                                }
                                System.out.println("CALC RATIO TAC E : step " + step.getStep() +"; metier " + metierLoop.getName() + "; quantMatPopHake " + quantMatPopHake + "; quantMatPopDiscHake " + quantMatPopDiscHake + "; exemptions[year] " + exemptions[year] + "; catchAllowedHake" + catchAllowedHake + "; ratioHake" + ratioHake);
                            }
                                
                            if(doTACSole){
                                System.out.println("PREACTION 9 metier : " + metierLoop.getName() + " step " + step.getStep());
                                ResultStorage matResult = context.getSimulationStorage().getResultStorage();
                                List<Population> Pops = context.getPopulationMonitor().getPopulations();
                                for (TimeStep dat = new TimeStep(0); dat.before(step); dat = dat.next()) {
                                    if( dat.getYear() == year) {                          
                                        for(Population onePop:Pops){
                                            MatrixND tmp = matResult.getMatrix(dat, onePop, MatrixCatchWeightPerStrategyMetPerZonePop.NAME).copy();
                                            MatrixND tmpDisc = null;
                                            if( matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME)!=null){
                                                tmpDisc = matResult.getMatrix(dat, onePop, MatrixDiscardsWeightPerStrMetPerZonePop.NAME).copy();
                                            }
                                            if (onePop.getSpecies() .getName().equals(param_species2.getName())){
                                                if(tmp!=null){
                                                    tmp = tmp.getSubMatrix(1, metierLoop); //Select the metier
                                                    quantMatPopSole += tmp.sumAll();
                                                }
                                                if(tmpDisc!=null){
                                                    tmpDisc = tmpDisc.getSubMatrix(1, metierLoop); //Select the metier
                                                    quantMatPopDiscSole += tmpDisc.sumAll();
                                                }
                                            }
                                        }
                                    }
                                }
                                if (quantMatPopSole!=0 &(metierLoop.getName().equals("Metier_Lang_NordPC") |metierLoop.getName().equals("Metier_Lang_InterC") |metierLoop.getName().equals("Metier_Lang_InterPC") |metierLoop.getName().equals("Metier_ChalutMixte_APCS") |metierLoop.getName().equals("Metier_ChalutMixte_NordC") |metierLoop.getName().equals("Metier_ChalutSole_InterSudC") |metierLoop.getName().equals("Metier_ChalutSole_InterC") |metierLoop.getName().equals("Metier_ChalutSole_NordCet") |metierLoop.getName().equals("Metier_ChalutBenth_APCS") |metierLoop.getName().equals("Metier_ChalutBenth_NordC") |metierLoop.getName().equals("Metier_ChalutBenth_NordAPC") |metierLoop.getName().equals("Metier_ChaluMixte_InterC") |metierLoop.getName().equals("Metier_ChalutMixte_NordPC"))){
                                    catchAllowedSole = exemptionsSoleTRAWL[year]*quantMatPopSole - quantMatPopDiscSole;
                                    ratioSole=catchAllowedSole/(quantMatPopSole/minMonth);
                                }
                                if (quantMatPopSole!=0 &(metierLoop.getName().equals("Metier_FiletMerlu_InterSudPC") |metierLoop.getName().equals("Metier_FiletMerlu_InterSudC") |metierLoop.getName().equals("Metier_FiletMerlu_InterSudAPC") |metierLoop.getName().equals("Metier_FiletMIxte_InterC") |metierLoop.getName().equals("Metier_FiletMixte_InterSudC") |metierLoop.getName().equals("Metier_FiletMixte_NordInterPC") |metierLoop.getName().equals("Metier_FiletMixte_NordC") |metierLoop.getName().equals("Metier_FiletMixte_NordPC") |metierLoop.getName().equals("Metier_FiletMerlu_NordPC") |metierLoop.getName().equals("Metier_FiletSole_NordC") |metierLoop.getName().equals("Metier_FiletSole_InterC") |metierLoop.getName().equals("Metier_FiletSole_InterSudPC") |metierLoop.getName().equals("Metier_FiletSole_NordIntPC"))) { //Exemption Sole GILLNET
                                    catchAllowedSole = exemptionsSoleGILLNET[year]*quantMatPopSole - quantMatPopDiscSole;
                                    ratioSole=catchAllowedSole/(quantMatPopSole/minMonth);
                                }
                                System.out.println("CALC RATIO TAC E : step " + step.getStep() +"; metier " + metierLoop.getName() + "; quantMatPopSole " + quantMatPopSole + "; quantMatPopDiscSole " + quantMatPopDiscSole + "; exemptions[year] " + exemptions[year] + "; catchAllowedSole" + catchAllowedSole + "; ratioSole" + ratioSole);
                            }
    
                                // If both species take the smallest one; otherwise take only one ratio
                                if(doTACHake & (doTACSole & !((metierLoop.getName().equals("PTBV_VIIIabd")|metierLoop.getName().equals("OBTS_VIIIabd")| metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC") |metierLoop.getName().equals("Metier_PalangreMixte_NordC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC"))) )){
                                    effortMax = ratioHake;
                                    if (ratioSole<ratioHake){
                                        effortMax = ratioSole;    
                                    }
                                }
                                
                                if(doTACHake & (doTACSole & ((metierLoop.getName().equals("PTBV_VIIIabd")|metierLoop.getName().equals("OBTS_VIIIabd")| metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC") |metierLoop.getName().equals("Metier_PalangreMixte_NordC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC"))) )){
                                    effortMax = ratioHake;// The metier cannot catch sole, consider only hake
                                }
                                System.out.println("CALC RATIO TAC E : doLO[year] " + doLO[year] + "; doTACHake" + doTACHake + "; doTACSole" + doTACSole + "; effortMax" + effortMax);
    
                                if(doTACHake & !doTACSole){
                                    effortMax = ratioHake;
                                }
    
                                if(!doTACHake & (doTACSole & !((metierLoop.getName().equals("PTBV_VIIIabd")|metierLoop.getName().equals("OBTS_VIIIabd")| metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC") |metierLoop.getName().equals("Metier_PalangreMixte_NordC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC"))) )){
                                    effortMax = ratioSole;
                                }
                                //Ifmetier is nto catching sole, effortMax =1 by default ... but I put it again, just in case
    
                                if(!doTACHake & (doTACSole & ((metierLoop.getName().equals("PTBV_VIIIabd")|metierLoop.getName().equals("OBTS_VIIIabd")| metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC") |metierLoop.getName().equals("Metier_PalangreMixte_NordC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC"))) )){
                                    effortMax = 1;
                                }
                                
    
                                if(((doTACHake & quantMatPopHake==0 )|(doTACSole & quantMatPopSole==0 &!((metierLoop.getName().equals("PTBV_VIIIabd")|metierLoop.getName().equals("OBTS_VIIIabd")| metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC") |metierLoop.getName().equals("Metier_PalangreMixte_NordC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC"))) ))){ //OR IF RATIO INFERIOR TO 0
                                    effortMax =0;    
                                }
    
                                if(effortMax<0){
                                    effortMax =0;    
                                }
    
                                System.out.println("CALC RATIO TAC E : doLO[year] " + doLO[year] + "; doTACHake" + doTACHake + "; doTACSole" + doTACSole + "; effortMax" + effortMax);
    
                                //recupere toutes les strategies pratiquant le metier et pour lesquelles la proportion !=0
                                SiMatrix siMatrix = SiMatrix.getSiMatrix(context);
                                Set<Strategy> strs = new HashSet<Strategy>();
                                for (Strategy str : siMatrix.getStrategies(step)) {
                                    //double prop = str.getStrategyMonthInfo(step.getMonth()).getProportionMetier(metierLoop);
                                    //if (prop != 0) {
                                        strs.add(str);
                                    //}
                                }
                                
                                for (Strategy str : strs) {
                                double totE =0;
                                    for (TimeStep dat = new TimeStep(0); dat.before(step); dat = dat.next()) {
                                        if( (dat.getYear() == year) & (dat.getMonth().getMonthNumber()<minMonth)) {    // Account only for time steps before the TAC was reached or considered near reached, otherwise a too big effort will be allocated.
                                            StrategyMonthInfo smiTemp = str.getStrategyMonthInfo(dat.getMonth());
                                            totE += smiTemp.getProportionMetier(metierLoop);
                                        }
                                    }
        
                                    double effortLimitMax = effortMax * totE ;
                                    double newProportion = str.getStrategyMonthInfo(step.getMonth()).getProportionMetier(metierLoop);
                                    System.out.println("CALC RATIO TAC E : doLO[year] " + doLO[year] + "; doTACHake" + doTACHake + "; doTACSole" + doTACSole + "; effortLimitMax" + effortLimitMax + "; totE" + totE + "; newProportion" + newProportion );
        
                                    if(doLO[year] & effortLimitMax<newProportion){
                                        newProportion =effortLimitMax;    
                                    }

                                    List<Metier> listOfMetiersInStrategy = str.getStrategyMonthInfo(step.getMonth()).getMetierWithProportion();

                                    if(listOfMetiersInStrategy.contains(metierLoop)){
                                        str.getStrategyMonthInfo(step.getMonth()).setProportionMetier(metierLoop,newProportion);
                                    }
                                    System.out.println("CALC RATIO TAC E : newProportion " + newProportion);
                                }
                    }
                 }              
               }
            }
        }
        affectationPreAction=false;
    }

    /**
     * Si la condition est vraie alors cette action est executee apres le pas de
     * temps de la simulation.
     * @param context La simulation pour laquelle on utilise cette regle
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier) throws Exception {
        //System.out.println("POSTACTION 1 step : " + step.getStep());
        int year = step.getYear();
        //System.out.println("DEBUT POSTACTION");
        //System.out.println("affectation = " + affectation);
        if(affectation){
            
            //System.out.println("POSTACTION 2 step : " + step.getStep());
            //System.out.println("DEBUT POSTACTION 2");    
            //System.out.println("affectation = " + affectation);        
            //debug
            //recupere toutes les strategies
            SiMatrix siMatrixAfter = SiMatrix.getSiMatrix(context);
            Set<Strategy> strsAfetr = new HashSet<Strategy>();
            for (Strategy str : siMatrixAfter.getStrategies(step)) {
                strsAfetr.add(str);
            }
            //Pour chaque strategie de ce pas de temps, stocke les proportions de tous les metiers dans cette strategie.
            for (Strategy str : strsAfetr) {
                List <Metier> allMetiers = str.getFisheryRegion().getMetier();
                for (Metier metierToCheck :  allMetiers) {
                    StrategyMonthInfo smiAfter = str.getStrategyMonthInfo(step.getMonth());
                    String toExportStrMetPropAfter = step.getStep() + ";" + metierToCheck.getName()+ ";" + str.getName() + ";" + smiAfter.getProportionMetier(metierToCheck) + "\n"; // Test to check metiers re-allocations. Comment it.
                    FileUtils.writeStringToFile(strMetAfter,toExportStrMetPropAfter, UTF_8, true);
                }
            }
            
            //System.out.println("POSTACTION 2a step : " + step.getStep());
        }
                            
            /*
             *            When dicarding is allowed, retention ogive is applied. For flag 1 métiers, discarding is implemented. As Flag 2 métiers did not catch fish, the postAction does not act on them.
             */
      // Avant les postAction, verifier si les conditions de TAC sont atteintes
      // premiere postAction : si les conditions de TAC ne sont pas atteintes, appliquer une ogive de tri separant les debarquements des rejets
      // deuxième postAction : si les conditions de TAC sont atteintes, tous les merlus sont rejetés, sans distinction de metier ou de classe de taille, la postAction des TAC s'applique
      // Application de l'ogive de tri sur les captures  
      // la matrice de captures a une dimension metier - donc a cette etape, toutes 
      // les captures de tous les metiers sont connues
      // Ne doit pas s'appliquer pour chaque metier de la boucle mais une seule fois! 
      // affectation est mis a false a la fin de la boucle sur les pop

         // Verification on selectivities, must always be commented.
         /*
         if(step.getStep()==0 & !metier.getGear().getName().equals("engin non activite")){
             Selectivity selectivity = metier.getGear().getPopulationSelectivity(param_species.getPopulation().iterator().next());
             for (PopulationGroup pg : param_species.getPopulation().iterator().next().getPopulationGroup()){
                double valToReturn =  selectivity.getCoefficient(param_species.getPopulation().iterator().next(),pg,metier);
                String toExportSel = valToReturn + ";" + metier.getName()+ ";" + pg.getLength() + "\n"; // Test to check selectivity functions. Comment it.
                FileUtils.writeStringToFile(exportTestSel,toExportSel,true); // Test to check selectivity functions. Comment it.
             }
         }
        */
        //System.out.println("POSTACTION 3 step : " + step.getStep());
    
        if (affectation) {
            //System.out.println("POSTACTION 3a step : " + step.getStep());
            //System.out.println("DEBUT POSTACTION 3");
            //System.out.println("affectation = " + affectation);
            
            /*
             *             postAction 1 : ogive de tri
             */
                                    // Action "rien" : si pour ce métier, le TAC ne s'applique pas ET que l'obligation de débarquement s'applique sans exemption de minimis
            if(!doTACHake){            // TAC is not reached : discard following retention ogives
                //System.out.println("Debut affecter rejets avec ogive");
                //System.out.println("POSTACTION 4 step : " + step.getStep());
                PopulationMonitor popMon = context.getPopulationMonitor();            
                for (Population pop : param_species.getPopulation()) {
                    MatrixND discard = popMon.getCatch(pop).copy();
                    for (MatrixIterator i = discard.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup) coordonnees[2];
                        Metier met = (Metier) coordonnees[1];
                        if((!doLO[year]) | (doLO[year] & ((int) context.getValue(met.getName()+"landHake")==0))){
                        //System.out.println("POSTACTION 5 step : " + step.getStep());
                            // Discards metier TRAWL_NEP
                            if (met.getName().equals("Metier_Lang_NordPC")||met.getName().equals("Metier_Lang_InterC")||met.getName().equals("Metier_Lang_InterPC")) {
                                  double propTrie = 1/(1+Math.exp(-(group.getLength()-P1_NEP)/P2_NEP));
                                  /*
                                  if(step.getStep()==0){
                                        String toExport = propTrie + ";" + met.getName()+ ";" + group.getLength() + "\n"; // Test to check retention functions. Comment it.
                                        FileUtils.writeStringToFile(exportTest,toExport,true); // Test to check retention functions. Comment it.
                                  }
                                  */
                                  double propRejet = 1 - propTrie;
                                  double value = i.getValue() * propRejet;
                                  //double valDeb = i.getValue() * propTrie;
                                  //double valCapt = i.getValue();
                                  if (group.getLength() >= param_TailleMin) { // The retention ogive is used only if individuals are above minimal size. Otherwise, all the catch is discrded.
                                    i.setValue(value); 
                                  }                            
                            }
                            // Discards metier TRAWL_FISH_BOB_W
                            if (met.getName().equals("PTBV_VIIIabd")||met.getName().equals("OBTS_VIIIabd")) {                             
                                  double propTrie = 1/(1+Math.exp(-(group.getLength()-P1_TFBW)/P2_TFBW));
                                  /*
                                  if(step.getStep()==0){
                                        String toExport = propTrie + ";" + met.getName()+ ";" + group.getLength() + "\n"; // Test to check retention functions. Comment it.
                                        FileUtils.writeStringToFile(exportTest,toExport,true); // Test to check retention functions. Comment it.
                                  }
                                  */
                                  double propRejet = 1 - propTrie;
                                  double value = i.getValue() * propRejet;
                                  //double valDeb = i.getValue() * propTrie;
                                  //double valCapt = i.getValue();
                                  if (group.getLength() >= param_TailleMin) { // The retention ogive is used only if individuals are above minimal size. Otherwise, all the catch is discrded.
                                    i.setValue(value);
                                  }
                            }
                            if (!(met.getName().equals("Metier_Lang_NordPC")||met.getName().equals("Metier_Lang_InterC")||met.getName().equals("Metier_Lang_InterPC")||met.getName().equals("PTBV_VIIIabd")||met.getName().equals("OBTS_VIIIabd"))){
                                if (group.getLength() >= param_TailleMin) { 
                                    i.setValue(0); // initialise discard si pas de rejet (ie taille>=TailleMin et pas metier qui rejette)
                                }
                            }
                        }
                        
                        if(doLO[year] & ((int) context.getValue(met.getName()+"landHake")==1)){
                            i.setValue(0); // initialise discard si pas de rejet
                        }

                        //Only trawls have an exemption for hake; longliners and gillneters do not discard, even if undersized.
                        if(doLO[year] & (met.getName().equals("Metier_FiletSole_NordC")||met.getName().equals("Metier_FiletSole_NordIntPC")||met.getName().equals("Metier_FiletSole_InterSudPC")||met.getName().equals("Metier_FiletSole_InterC")||met.getName().equals("Metier_PalangreMerlu_InterSudAC")||met.getName().equals("Metier_PalangreMixte_NordC")||met.getName().equals("Metier_PalangreMixte_InterC")||met.getName().equals("Metier_FiletMerlu_NordC")||met.getName().equals("Metier_FiletMerlu_NordPC")||met.getName().equals("Metier_FiletMixte_NordPC")||met.getName().equals("Metier_FiletMixte_NordC")||met.getName().equals("Metier_FiletMixte_NordInterPC")||met.getName().equals("Metier_FiletMixte_InterSudC")||met.getName().equals("Metier_FiletMIxte_InterC")||met.getName().equals("Metier_FiletMerlu_InterSudAPC")||met.getName().equals("Metier_FiletMerlu_InterSudC")||met.getName().equals("Metier_FiletMerlu_InterSudPC")||met.getName().equals("Metier_PalangreMerlu_InterSudC"))){
                            i.setValue(0); // initialise discard si pas de rejet
                        }
                    }
                    //System.out.println("POSTACTION 6 step : " + step.getStep());
                    if(flagLGEsp){ // Discards spanish longliners and gillnetters : initial the discard parameters. They can discard only when there is no LO (no exemption for them)
                        for(int groupLengthId = 0; groupLengthId<72; groupLengthId++){
                            if ((groupLengthId+0.5) >= param_TailleMin) { 
                            //System.out.println("POSTACTION 7 step : " + step.getStep());
                                //System.out.println("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep());
                                context.setValue("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep(), (double) 0); // initialise discard si pas de rejet (ie taille>=TailleMin et pas metier qui rejette)
                                context.setValue("totOtherDiscardStep_" + groupLengthId + "_" + step.getStep(), (double) 0); // initialise discard si pas de rejet (ie taille>=TailleMin et pas metier qui rejette)
                            } else{
                                if((!doLO[year])){//Landing obligation condition
                                    //System.out.println("POSTACTION 8 step : " + step.getStep());
                                    //System.out.println("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep());
                                    context.setValue("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep(), (double) context.getValue("totOtherCatchNumStep_" + groupLengthId + "_" + step.getStep()));// initialise discard : les poissons sous taille sont tous rejetés
                                    context.setValue("totOtherDiscardStep_" + groupLengthId + "_" + step.getStep(), (double) context.getValue("totOtherCatchStep_" + groupLengthId + "_" + step.getStep()));// initialise discard : les poissons sous taille sont tous rejetés
                                }
                                if((doLO[year])){//Landing obligation condition
                                    //System.out.println("POSTACTION 9 step : " + step.getStep());
                                    //System.out.println("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep());
                                    context.setValue("totOtherDiscardNumStep_" + groupLengthId + "_" + step.getStep(), (double) 0);// initialise discard : les poissons sous taille sont tous rejetés
                                    context.setValue("totOtherDiscardStep_" + groupLengthId + "_" + step.getStep(), (double) 0);// initialise discard : les poissons sous taille sont tous rejetés
                                }
                            }
                        }
                    }   
                    //System.out.println("POSTACTION 7b step : " + step.getStep()); 
                    discard.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                    popMon.addDiscard(step, pop, discard); 
                }
            }


           //System.out.println("POSTACTION 8 step : " + step.getStep());
            //Nephrops : always discard undersized individuals (survivability exemption)
            PopulationMonitor popMon3 = context.getPopulationMonitor();            
                for (Population pop : param_species3.getPopulation()) {
                    MatrixND discard3 = popMon3.getCatch(pop).copy();             
                    for (MatrixIterator i = discard3.iterator(); i.next();) {
                        Object[] coordonnees = i.getSemanticsCoordinates();
                        PopulationGroup group = (PopulationGroup) coordonnees[2];
                        if (group.getLength() >= param_TailleMinLang) { 
                            i.setValue(0); // initialise discard si pas de rejet (ie taille>=TailleMin et pas metier qui rejette)
                        }
                    }  
                    //System.out.println("POSTACTION 9 step : " + step.getStep());
                    discard3.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                    popMon3.addDiscard(step, pop, discard3); 
                }
            
            
            /*
             *             postAction 2 : TAC
             */
             
           
                //System.out.println("Debut TAC");
                PopulationMonitor popMon = context.getPopulationMonitor();  
                //System.out.println("[TAC] postAction for: " + metier);
                //System.out.println("popMon biomass" + popMon.getBiomass(param_species));
                //System.out.println("POSTACTION 10 step : " + step.getStep());
                if(doTACHake){
                    //System.out.println("POSTACTION 10a step : " + step.getStep());
                    for (Population pop : param_species.getPopulation()) {
                        if (!pop.getName().equals("Population_new")) {
                            //System.out.println("POSTACTION 10b step : " + step.getStep());
                            //System.out.println("pop : " + pop.getName());
                            // Si on a deja une matrice rejet, on la vide (elle vient
                            // forcement de la regle taille minimale, or si le TAC est
                            // atteint, tout va dorenavant dans les rejets, et on met
                            // TOUTES les captures dans les rejets
                            MatrixND discard = popMon.getDiscard(step, pop);
                            //System.out.println("discard : " + discard);
                            if (discard != null) {
                                discard.mults(0);
                            }
                            //System.out.println("catch = " + popMon.getCatch(pop));
                            discard = popMon.getCatch(pop).copy();
                            discard.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                            popMon.addDiscard(step, pop, discard);
                            //System.out.println("POSTACTION 10c step : " + step.getStep());
                            //System.out.println("[TAC] add discard for " + pop + ": " + discard);
                        }
                    }
                    if(flagLGEsp){ // Discards spanish longliners and gillnetters : initial the discard parameters
                        //System.out.println("POSTACTION 10d bis step : " + step.getStep());
                        for (int lengthGroupId=0; lengthGroupId<72; lengthGroupId++) { 
                             //System.out.println("totOtherDiscardNumStep_" + lengthGroupId + "_" + step.getStep());
                             context.setValue("totOtherDiscardNumStep_" + lengthGroupId + "_" + step.getStep(), (double) context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep())); // all fish are discarded (spanish longliners and gillnetters)
                             context.setValue("totOtherDiscardStep_" + lengthGroupId + "_" + step.getStep(), (double) context.getValue("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep())); // all fish are discarded (spanish longliners and gillnetters)
                        }
                    }
                }
                if(doTACSole){
                    //System.out.println("POSTACTION 11 step : " + step.getStep());
                    for (Population pop : param_species2.getPopulation()) {
                        if (!pop.getName().equals("Population_new")) {
                            //System.out.println("POSTACTION 11a step : " + step.getStep());
                            //System.out.println("pop : " + pop.getName());
                            MatrixND discard = popMon.getDiscard(step, pop);
                            //System.out.println("discard : " + discard);
                            if (discard != null) {
                                discard.mults(0);
                            }
                            //System.out.println("catch = " + popMon.getCatch(pop));
                            discard = popMon.getCatch(pop).copy();
                            discard.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                            popMon.addDiscard(step, pop, discard);
                            
                            //System.out.println("POSTACTION 11b step : " + step.getStep());
                            //System.out.println("[TAC] add discard for " + pop + ": " + discard);
                        }
                    }  
                }
                if(doTACNep){
                    //System.out.println("POSTACTION 12 step : " + step.getStep());
                    for (Population pop : param_species3.getPopulation()) {
                        //System.out.println("POSTACTION 12a step : " + step.getStep());
                        if (!pop.getName().equals("Population_new")) {
                            //System.out.println("POSTACTION 12b step : " + step.getStep());
                            //System.out.println("pop : " + pop.getName());
                            MatrixND discard = popMon.getDiscard(step, pop);
                            //System.out.println("discard : " + discard);
                            if (discard != null) {
                                discard.mults(0);
                            }
                            //System.out.println("catch = " + popMon.getCatch(pop));
                            discard = popMon.getCatch(pop).copy();
                            discard.setName(MatrixDiscardsPerStrMetPerZonePop.NAME);
                            popMon.addDiscard(step, pop, discard);
                            //System.out.println("POSTACTION 12c step : " + step.getStep());
                            //System.out.println("[TAC] add discard for " + pop + ": " + discard);
                        }
                    }  
                }


             /*
             *             postAction 3 : Nephrops survival to discard (30% survive)
             */
             for (Population pop : param_species3.getPopulation()) {
                 //System.out.println("POSTACTION 13 step : " + step.getStep());
                MatrixND discardNep = popMon.getDiscard(step, pop).copy().sumOverDim(0).sumOverDim(1).reduce();// (0 step) 0 strategy 1 metier 2 group 3 zone
                MatrixND abundanceNep = popMon.getN(pop); // 0 step 1 group 2 zone?
                for (MatrixIterator i = abundanceNep.iterator(); i.next();) {
                    Object[] coordonnees = i.getSemanticsCoordinates();
                    double survivors = 0.3*discardNep.getValue(coordonnees);
                    double oldAbundance = i.getValue();
                    double newValue = oldAbundance+survivors;
                    i.setValue(newValue);
                }
                //System.out.println("POSTACTION 14 step : " + step.getStep());
             }
             
            outputSpanishCatch = "";
            outputSpanishCatchNum = "";
            //System.out.println("POSTACTION 15 step : " + step.getStep());
            //System.out.println("affectation = " + affectation);
    
            /*
             * Export spanish longliners gillnetters catch
             */
            if (step.getStep()==0 & affectation){
                outputSpanishCatch+="[12,4,2,63]\n";
                outputSpanishCatch+="java.lang.Integer:0,1,2,3,4,5,6,7,8,9,10,11\n";
                outputSpanishCatch+="java.lang.String:LONGLINEGILLNET_BOB,TRAWL_FISH_BOB_E,TRAWL_FISH_BOB_W,TRAWL_NEP\n";
                outputSpanishCatch+="java.lang.String:discard,landing\n";
                outputSpanishCatch+="java.lang.Integer:2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64\n";
            }
            //System.out.println("POSTACTION 16 step : " + step.getStep());
            if (affectation){
                for (int lengthGroupId=5; lengthGroupId<72; lengthGroupId++) {
                    //System.out.println("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep());
                    //System.out.println("totOtherDiscardStep_" + lengthGroupId + "_" + step.getStep());
                    double landVal = (double) context.getValue("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep()) - (double) context.getValue("totOtherDiscardStep_" + lengthGroupId + "_" + step.getStep());
                    double discVal = (double) context.getValue("totOtherDiscardStep_" + lengthGroupId + "_" + step.getStep());
                    outputSpanishCatch+= step.getStep() + ";0;0;" + (lengthGroupId-2) +";" +discVal + "\n";
                    outputSpanishCatch+= step.getStep() + ";0;1;" + (lengthGroupId-2) +";" +landVal + "\n";
                }
                //System.out.println("POSTACTION 17 step : " + step.getStep());
                FileUtils.writeStringToFile(outputCatch,outputSpanishCatch, UTF_8, true);
                //System.out.println("POSTACTION 18 step : " + step.getStep());
            }

            if (step.getStep()==0 & affectation){
                outputSpanishCatchNum+="[12,4,2,63]\n";
                outputSpanishCatchNum+="java.lang.Integer:0,1,2,3,4,5,6,7,8,9,10,11\n";
                outputSpanishCatchNum+="java.lang.String:LONGLINEGILLNET_BOB,TRAWL_FISH_BOB_E,TRAWL_FISH_BOB_W,TRAWL_NEP\n";
                outputSpanishCatchNum+="java.lang.String:discard,landing\n";
                outputSpanishCatchNum+="java.lang.Integer:2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64\n";
            }
            //System.out.println("POSTACTION 19 step : " + step.getStep());
            if (affectation){
                for (int lengthGroupId=5; lengthGroupId<72; lengthGroupId++) {
                    //System.out.println("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep());
                    //System.out.println("totOtherDiscardNumStep_" + lengthGroupId + "_" + step.getStep());
                    double landVal = (double) context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep()) - (double) context.getValue("totOtherDiscardNumStep_" + lengthGroupId + "_" + step.getStep());
                    double discVal = (double) context.getValue("totOtherDiscardNumStep_" + lengthGroupId + "_" + step.getStep());
                    outputSpanishCatchNum+= step.getStep() + ";0;0;" + (lengthGroupId-2) +";" +discVal + "\n";
                    outputSpanishCatchNum+= step.getStep() + ";0;1;" + (lengthGroupId-2) +";" +landVal + "\n";
                }
                //System.out.println("POSTACTION 20 step : " + step.getStep());
                FileUtils.writeStringToFile(outputCatchNum,outputSpanishCatchNum, UTF_8, true);
                //System.out.println("POSTACTION 21 step : " + step.getStep());
            }
            affectation = false; // pour ne pas refaire l'affectation au prochain metier
            flagLGEsp = false; // Work on spanish longliners and gillnetters only one time
        }
    }

     /**
     * Creates a R command (creates a numeric vector). When evaluated with REngine.voidEval / eval,
     * creates the R object rVariable, which is the "R version" of javaTable
     * Works only on String [].
     * @param rVariable Name of R variable in the command
     * @param javaTable the java String table to be passed to R
     * @return a R command to be evaluated.
     */
    public String RCommand (String rVariable, String[] javaTable){
        String command = rVariable+"<-c("; // Create the ISISyears numeric vector in R; the time series considered in ISIS-Fish simulation
        for(int i =1 ; i < javaTable.length ; i++){
            command = command+javaTable[i-1]+",";
        }
        command = command+javaTable[javaTable.length-1]+")";
        return (command);
    }

    /**
     * Creates a table containing catch/discards for some times steps.
     * @param matData the MatrixND containing data on catch/discards
     * @param tableData the List to return, created before calling the function.
     * @param pop a Population.
     * @return a List containing the same information, ready to be formatted in R.
     */
    public List fillTable (MatrixND matData, List tableData,Population pop) throws Exception{
        if (matData != null) {
                matData = matData.sumOverDim(1); //sum on strategy
                
                //Create col names
                tableData.add("\"step\",\"population\",\"metier\",\"group\",\"zone\",\"value\"");
                
                //Fill the table, row by row                
                for (MatrixIterator i = matData.iterator(); i.hasNext();) { // I am sure for the coordinates of semantics, I checked
                    i.next();
                    Object[] sems = i.getSemanticsCoordinates();
                    TimeStep stepName = (TimeStep) sems[0];
                    Metier metierName = (Metier) sems[2];
                    PopulationGroup group = (PopulationGroup) sems[3];
                    Zone zone = (Zone) sems[4];
                    double val = i.getValue();
                    // Create a table with all the interesting stuff for R. This formating is designed for processing in R
                    tableData.add("\""+ stepName.getStep() + "\",\"" + pop.getName() + "\",\"" + metierName.getName() + "\",\"" + group.getId() + "\",\"" + zone.getName() + "\",\"" + val + "\"");
                }
            }
        else{ //ERROR : if MatrixND is null.
           throw new Exception("Null dataset, check your MatrixND."); 
        }
        return(tableData);
    }

    public List fillTableAbundance (MatrixND matData, List tableData,Population pop) throws Exception{
        if (matData != null) {                
            //Create col names
            tableData.add("\"step\",\"population\",\"group\",\"zone\",\"value\"");
                
            //Fill the table, row by row                
            for (MatrixIterator i = matData.iterator(); i.hasNext();) { // I am sure for the coordinates of semantics, I checked
                i.next();
                Object[] sems = i.getSemanticsCoordinates();
                TimeStep stepName = (TimeStep) sems[0];
                PopulationGroup group = (PopulationGroup) sems[1];
                Zone zone = (Zone) sems[2];
                double val = i.getValue();
                // Create a table with all the interesting stuff for R. This formating is designed for processing in R
                tableData.add("\""+ stepName.getStep() + "\",\"" + pop.getName() + "\",\"" + group.getId() + "\",\"" + zone.getName() + "\",\"" + val + "\"");
            }
        }
        else{ //ERROR : if MatrixND is null.
           throw new Exception("Null dataset, check your MatrixND.");
        }
        return(tableData);
    }

    public MatrixND fromAbd2AbdBeginMonth(MatrixND Abundance, SimulationContext context, TimeStep step, Population onePop){
                        //////////// EMBE DINTO A FUNCTION
                        //I actually want MatrixAbundanceBeginMonth : apply in this order: growth, migrations and recruitment/reproduction
                        //I have SimulationContext context, TimeStep step, Metier metier
                        //I want SimulationContext context, SiMatrix siMatrix, TimeStep step, Population pop
                        PopulationMonitor popMon = context.getPopulationMonitor();
                        SiMatrix siMatrix = SiMatrix.getSiMatrix(context); // We need a siMatrix
                        Month month = step.getMonth();
                        MatrixND biomass = siMatrix.matrixBiomass(Abundance, onePop, step);
                        PopulationSeasonInfo info = onePop.getPopulationSeasonInfo(month);
    
                        //Growth
                        MatrixND CA;
                        if (step.getStep() == 0) {
                            CA = MatrixFactory.getInstance().matrixId(
                                    onePop.sizePopulationGroup() * onePop.sizePopulationZone());
                        } else {
                            CA = info.getGroupChangeMatrix(month);
                        }
    
                        //migration
                        MatrixND M = info.getMigrationMatrix(month, Abundance);
                        log.debug("M: " + M);
                        //emigration
                        MatrixND EM = info.getEmigrationMatrix(month, Abundance);
                        log.debug("EM: " + EM);
                        //immigration
                        MatrixND IM = info.getImmigrationMatrix(month, Abundance).transpose();
                        log.debug("IM: " + IM);
    
                        MatrixND N1D = onePop.N2DToN1D(Abundance);
    
                        MatrixND tmp0 = N1D.mult(CA);
                        MatrixND tmp1 = M.minus(EM);
                        MatrixND tmp2 = tmp0.mult(tmp1);
                        MatrixND tmp3 = tmp2.add(IM);
    
                        Abundance = onePop.split2D(tmp3);
    
                        //Recru/repro
                        MatrixND R = info.getReproductionMatrix(month, Abundance);
                        popMon.setReproduction(step, onePop, Abundance, biomass, R);
                        MatrixND recruitment = popMon.getRecruitment(step, onePop);
    
                        //Repro mortality
                        popMon.applyReproductionMortality(onePop);
    
                        Abundance = Abundance.add(recruitment);
    
                        return(Abundance);
                    }
    //static public void main (String...args) throws Exception {
        //int yearNum=7;
    //System.out.println("Tarf.Nep"+(2010+yearNum));
    //TarfValue = (double) context.getValue("Tarf.Nep"+(2010+yearNum)); 
    //MatrixND appliedFGroups = MatFPreviousYears.sumOverDim(0).reduce().divs(3).mult(appliedF);
        /*
        int currentYearInt = 2016;
                File bash = new File("/users/calcul/isisfish/couplageISIS_SS3TEST3zones/bashISIS.sh");
                FileUtils.writeStringToFile(bash,"#!/bin/bash\n",false);
                FileUtils.writeStringToFile(bash,"cd /users/calcul/isisfish/couplageISIS_SS3TEST3zones/\n",true);
                FileUtils.writeStringToFile(bash,"sudo chmod -R 777 *\n",true);
                FileUtils.writeStringToFile(bash,"cd SS3_model_"+currentYearInt+"SAVE2/\n",true);
                FileUtils.writeStringToFile(bash,"xterm -e  \"./SS3_opt -nohess\"\n",true);
                FileUtils.writeStringToFile(bash,"cd /users/calcul/isisfish/isis-fish-4.4.1.0-SNAPSHOT/",true);

                // Autoriser son execution
                Process runtimeProcessautoize = Runtime.getRuntime().exec("chmod 777 /users/calcul/isisfish/couplageISIS_SS3TEST3zones/bashISIS.sh");
                int processValue = runtimeProcessautoize.waitFor();
        
                // L'executer
                String cmd = "/bin/sh /users/calcul/isisfish/couplageISIS_SS3TEST3zones/bashISIS.sh";
                Process runtimeProcess = Runtime.getRuntime().exec(cmd);
                processValue = runtimeProcess.waitFor();
               
                System.out.println("After assessment. processValue " + processValue);

            REngine engine = new RProxy(); // R session
            engine.voidEval("source(\"/users/calcul/isisfish/couplageISIS_SS3TEST3zones/fromSS32ISIS4Java.R\")");
            engine.voidEval("wpath<-\"/users/calcul/isisfish/couplageISIS_SS3TEST3zones\"");
            engine.voidEval("currentYear<-2016") ; // the year which is to be assessed (the n-2 year)
            engine.voidEval("FMSY<-0.27") ; // the FMSY value
            double TACvalue = Double.parseDouble(engine.eval("go2ISIS(currentYear,wpath,FALSE,FMSY)").toString());   
            System.out.println(TACvalue);
            
                String cmd = "/bin/sh /users/calcul/isisfish/couplageISIS_SS3TEST3zones/bashWorkplace.sh";
                Process runtimeProcess2 = Runtime.getRuntime().exec(cmd);
                int processValue = runtimeProcess2.waitFor();
                System.out.println(processValue);

                

                String cmd = "ls";
                Process runtimeProcess2 = Runtime.getRuntime().exec(cmd);
                BufferedReader stdInput = new BufferedReader(new InputStreamReader(runtimeProcess2.getInputStream()));
                String s = null;
                while ((s = stdInput.readLine()) != null) {
                    System.out.println(s);
                }
   */ 
    //}
}