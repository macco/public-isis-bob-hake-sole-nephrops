/*
 * #%L
 * IsisFish data
 * %%
 * Copyright (C) 2006 - 2011 Ifremer, CodeLutin
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 2 of the 
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public 
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-2.0.html>.
 * #L%
 */
package rules;

import static org.nuiton.i18n.I18n.t;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashSet;
import java.util.Set;
import java.io.File;
import java.io.FileReader;
import static java.nio.charset.StandardCharsets.UTF_8;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.nuiton.math.matrix.MatrixIterator;
import org.nuiton.math.matrix.MatrixFactory;
import org.nuiton.math.matrix.MatrixND;
import fr.ifremer.isisfish.util.ScriptUtil;

import scripts.ResultName;
import scripts.RuleUtil;
import scripts.SiMatrix;
import fr.ifremer.isisfish.datastore.ResultStorage;
import fr.ifremer.isisfish.entities.EffortDescription;
import fr.ifremer.isisfish.entities.Metier;
import fr.ifremer.isisfish.entities.Zone;
import fr.ifremer.isisfish.entities.Population;
import fr.ifremer.isisfish.entities.PopulationGroup;
import fr.ifremer.isisfish.entities.Species;
import fr.ifremer.isisfish.entities.Strategy;
import fr.ifremer.isisfish.entities.StrategyMonthInfo;
import fr.ifremer.isisfish.entities.TargetSpecies;
import fr.ifremer.isisfish.rule.AbstractRule;
import fr.ifremer.isisfish.simulator.MetierMonitor;
import fr.ifremer.isisfish.simulator.PopulationMonitor;
import fr.ifremer.isisfish.simulator.SimulationContext;
import fr.ifremer.isisfish.types.TimeStep;
import fr.ifremer.isisfish.types.Month;
import fr.ifremer.isisfish.util.Doc;

/**
 * 
 * 
 * Created: 16 octobre 2012
 *
 * @author anonymous <anonymous@labs.libre-entreprise.org>
 * @version $Revision: 1.3 $
 *
 * Last update: $Date: 161012 $
 * by : $Author: Stephanie $
 */
public class CapturesLongLinersnetters_Peojection_Ratio_PropSPDEF extends AbstractRule {

    /** to use log facility, just put in your code: log.info("..."); */
    static private Log log = LogFactory.getLog(CapturesLongLinersnetters_Peojection_Ratio_PropSPDEF.class);

   @Doc("Affected species")
    public Population param_Population = null; //pour le merlu
    
    @Doc("Debin date")
    public TimeStep param_beginStep = new TimeStep(0); //2010
    @Doc("End date")
    public TimeStep param_endStep = new TimeStep(132); //2019
    public boolean param_noRule; //2019
   
    
    // deux fichiers 12 lignes et 65 colonnes : RATIO de captures en Nombre
    public String param_nomfichier_ratiocapturesLiners = "input/ratiosPalangresEspagnols1112.csv";//in row : time ; in columns : age or length group
    protected File  RatioCatchHakeGdGLiners;
    protected MatrixND matrixRatioCatchHakeGdGLiners;
    public String param_nomfichier_ratiocapturesNetters = "input/ratiosFiletsEspagnols1112.csv";//in row : time ; in columns : age or length group
    protected File  RatioCatchHakeGdGNetters;
    protected MatrixND matrixRatioCatchHakeGdGNetters;

    public String param_nomfichier_Liners2010 = "input/PalangresEspagnolsCaptureNombres2010.csv";//in row : time ; in columns : age or length group
    protected File  CatchHakeGdGLiners2010;
    protected MatrixND matrixCatchHakeGdGLiners2010;
    public String param_nomfichier_Netters2010 = "input/FiletsEspagnolsCaptureNombres2010.csv";//in row : time ; in columns : age or length group
    protected File  CatchHakeGdGNetters2010;
    protected MatrixND matrixCatchHakeGdGNetters2010;

    public String param_nomfichier_weight_length_hake = "input/weight_at_length_hake.csv";
    protected File  WeightAtLengthHake;
    protected MatrixND matrixWeightAtLengthHake;

    @Doc("If the ratio Forced catch / Abundance is superior to this value, catch only this proportion times abundance, not the forced catch")
    public double param_propIfNotEnoughFish = 0.9; //
    
    protected File  outputCatch;
    protected File  outputCatchNum;
    
    protected File  outputCatchSave;
    protected File  outputCatchNumSave;
    protected File  outputCatchSaveBefore;
    protected File  outputCatchNumSaveBefore;
   
    
    protected File  abundanceInter;
    protected File  abundanceInter2;
    protected File  catchSP;
    
    protected String outputSpanishCatch;
    protected String outputSpanishCatchNum;
    
    public MatrixND matrixOtherCatch;
    public MatrixND matrixOtherCatchSum;

    boolean affectation = false;
    boolean NoRule;

    protected String[] necessaryResult = {
    // put here all necessary result for this rule
    // example: 
    // ResultName.MATRIX_BIOMASS,
    // ResultName.MATRIX_NET_VALUE_OF_LANDINGS_PER_STRATEGY_MET,
    };

    /**
     * @return the necessaryResult
     */
    @Override
    public String[] getNecessaryResult() {
        return this.necessaryResult;
    }

    /**
     * Permet d'afficher a l'utilisateur une aide sur la regle.
     * 
     * @return L'aide ou la description de la regle
     */
    @Override
    public String getDescription() {
        return t("Remove catches in number of  Spanish liners and Spanish gillnetters (as a ratio of the catches simulated by ISIS) from abundance, the former in zone_merlu_reproduction and the latter in zone_merlu_recrutement");
    }

    /**
     * Appele au demarrage de la simulation, cette methode permet d'initialiser
     * des valeurs
     * 
     * @param context La simulation pour lequel on utilise cette regle
     */
    @Override
    public void init(SimulationContext context) throws Exception {
    
        if (param_nomfichier_ratiocapturesLiners==null || "".equals(param_nomfichier_ratiocapturesLiners)){
            RatioCatchHakeGdGLiners = ScriptUtil.getFile(".*.csv", "fichier csv separateur ';'");
        } else {
            RatioCatchHakeGdGLiners = new File(param_nomfichier_ratiocapturesLiners);
        }
        if (param_nomfichier_ratiocapturesNetters==null || "".equals(param_nomfichier_ratiocapturesNetters)){
            RatioCatchHakeGdGNetters = ScriptUtil.getFile(".*.csv", "fichier csv separateur ';'");
        } else {
            RatioCatchHakeGdGNetters = new File(param_nomfichier_ratiocapturesNetters);
        }
        if (param_nomfichier_Liners2010==null || "".equals(param_nomfichier_Liners2010)){
         CatchHakeGdGLiners2010 = ScriptUtil.getFile(".*.csv", "fichier csv separateur ';'");
        } else {
            CatchHakeGdGLiners2010 = new File(param_nomfichier_Liners2010);
        }
        if (param_nomfichier_Netters2010==null || "".equals(param_nomfichier_Netters2010)){
            CatchHakeGdGNetters2010 = ScriptUtil.getFile(".*.csv", "fichier csv separateur ';'");
        } else {
            CatchHakeGdGNetters2010 = new File(param_nomfichier_Netters2010);
        }
        if (param_nomfichier_weight_length_hake==null || "".equals(param_nomfichier_weight_length_hake)){
            WeightAtLengthHake = ScriptUtil.getFile(".*.csv", "fichier csv separateur ';'");
        } else {
            WeightAtLengthHake = new File(param_nomfichier_weight_length_hake);
        }
       NoRule = param_noRule;
        outputCatch = new File("output/catchSpanishLGNoRule.csv");
        outputCatchNum = new File("output/catchSpanishLGNumNoRule.csv");
        // The 4 following are inly for debug, comment them after.
        
        outputCatchSave = new File("output/catchSpanishLGSave_NoTAC20102012NOSPLG.csv");
        outputCatchNumSave = new File("output/catchSpanishLGNumSave_NoTAC2010.csv");
        outputCatchSaveBefore = new File("output/catchSpanishLGSaveBefore_NoTAC20102012NOSPLG.csv");
        outputCatchNumSaveBefore = new File("output/catchSpanishLGNumSaveBefore_NoTAC2010.csv");
       
        
        
        /*
        abundanceInter = new File("output/AbundanceInter.csv");
        abundanceInter2 = new File("output/AbundanceInter2WithRule.csv");
        catchSP = new File("output/catchSPWithRule2010.csv");
        */
         /////*** specify dimention of the matrix containning catch
         /////*** numbers of columns : could be equal to your number of classes in ISIS but may also be different if your had only aggregated data
        int nbGroup = 72 ;
        
        /////*** enter number of rows (years*12) (lines of the catch file)
        int nbTimeStep = 12; // 12 mois par an
        int[] dimMatrix = {nbTimeStep,nbGroup}; 
        matrixRatioCatchHakeGdGLiners = MatrixFactory.getInstance().create(dimMatrix);
        matrixRatioCatchHakeGdGLiners.importCSV(new FileReader(RatioCatchHakeGdGLiners),new int []{0,0});
        matrixRatioCatchHakeGdGLiners=matrixRatioCatchHakeGdGLiners.reduce();
        //log.info("MatrixCapturesNombreLiners : " + matrixRatioCatchHakeGdGLiners);
    
        matrixRatioCatchHakeGdGNetters = MatrixFactory.getInstance().create(dimMatrix);
        matrixRatioCatchHakeGdGNetters.importCSV(new FileReader(RatioCatchHakeGdGNetters),new int []{0,0});
        matrixRatioCatchHakeGdGNetters=matrixRatioCatchHakeGdGNetters.reduce();
        //log.info("MatrixCapturesNombreNetters : " + matrixRatioCatchHakeGdGNetters);

        matrixCatchHakeGdGLiners2010 = MatrixFactory.getInstance().create(dimMatrix);
        matrixCatchHakeGdGLiners2010.importCSV(new FileReader(CatchHakeGdGLiners2010),new int []{0,0});
        matrixCatchHakeGdGLiners2010=matrixCatchHakeGdGLiners2010.reduce();
        //log.info("MatrixCapturesNombreNetters : " + matrixRatioCatchHakeGdGNetters);

        matrixCatchHakeGdGNetters2010 = MatrixFactory.getInstance().create(dimMatrix);
        matrixCatchHakeGdGNetters2010.importCSV(new FileReader(CatchHakeGdGNetters2010),new int []{0,0});
        matrixCatchHakeGdGNetters2010=matrixCatchHakeGdGNetters2010.reduce();
        //log.info("MatrixCapturesNombreNetters : " + matrixRatioCatchHakeGdGNetters);
    
        int[] dimWeight = {nbGroup, 1};
        matrixWeightAtLengthHake = MatrixFactory.getInstance().create(dimWeight);
        matrixWeightAtLengthHake.importCSV(new FileReader(WeightAtLengthHake),new int []{0,0});
        matrixWeightAtLengthHake=matrixWeightAtLengthHake.reduce();
        //log.info("matrixWeightAtAgeHake : " + matrixWeightAtAgeHake);
        
    }

    /**
     * La condition qui doit etre vrai pour faire les actions.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     * @return vrai si on souhaite que les actions soit faites
     */
    @Override
    public boolean condition(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {


        boolean result = false;
        if (step.before(param_beginStep)) {
            result = false;
        } else if (step.after(param_endStep)) {
            result = false;
        } else {
          result = true; 
        }
        return result;
    }

    /**
     * Si la condition est vrai alors cette action est executee avant le pas
     * de temps de la simulation.
     * 
     * @param context la simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     */
    @Override
    public void preAction(SimulationContext context, TimeStep step, Metier metier)
            throws Exception {
        affectation = false;

       
    }

    /**
     * Si la condition est vrai alors cette action est execute apres le pas
     * de temps de la simulation.
     * 
     * @param context La simulation pour lequel on utilise cette regle
     * @param step le pas de temps courant
     * @param metier le metier concerne
     */
    @Override
    public void postAction(SimulationContext context, TimeStep step, Metier metier)throws Exception {
            
    //ne faire cette postAction que pour le premier metier
    double propIfNotEnoughFish = param_propIfNotEnoughFish ;
            
    ResultStorage resultmanager = context.getSimulationStorage().getResultStorage();
    //affectation : do this loop only one time ; flag : do this only of the metier is allowed to fish
    boolean condition = false;
    //System.out.println("NORULE = " + NoRule);
    //System.out.println("CHECK 1 SPLGMerluflag");
    //System.out.println("CHECK 1 SPLGMerluflag"+context.getValue("SpLGMerluflag"));
    //System.out.println("CHECK 2 AFFECTATION");
    //System.out.println("CHECK 2 AFFECTATION"+affectation);
        if(!NoRule){
            //System.out.println("NORULE = " + NoRule);
            System.out.println("VALUE DEBUG : " + context.getValue("SpLGMerluflag"));
            condition =(!affectation) & ((Integer) context.getValue("SpLGMerluflag")!=2);
            if((int) context.getValue("SpLGMerluflag")==2){ // If Spanish LG are forbidden, spanish LG don't catch anything
                for (int lengthGroupId=0; lengthGroupId<72; lengthGroupId++) {
                    //System.out.println("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep());   
                    //System.out.println("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep());                           
                    context.setValue("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep(), 0.0);
                    context.setValue("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep(), 0.0);
                }
            }    
        }else{
            condition =(!affectation) ; 
        }
        if (condition) {
             
        //"Coeff correcteur multiplicatif des captures totales des others en nombre")
        // double CoefMultpErreur = 4.85; // en 2010 qui n'a plus lieu d'etre avec les nouveaux ratios
            PopulationMonitor popMon = context.getPopulationMonitor();
            Population pop = (Population) context.getDB().findByTopiaId(param_Population.getTopiaId());
            MatrixND abondance = popMon.getN(pop);
            MatrixND catches = popMon.getCatch(pop); // Ce sont les captures(tous les français + chalutiers espagnols) qui sont a multiplier au ratio de captures espagnoles
            //log.info("abondance_nbDim = " + abondance.getDimCount());
            //log.info("catches_nbDim_1 = " + catches.getDimCount());
            //log.info("catches = " + catches);
            
            // On fait la somme des captures sur les metiers et sur les strategies
            catches = catches.sumOverDim(0); // Somme sur les strategies
            //catches = catches.sumOverDim(1); // Somme sur les metiers
            catches = catches.reduce();
            //log.info("catches_nbDim_2 = " + catches.getDimCount());
            
            matrixOtherCatch = catches.copy();
            matrixOtherCatch = matrixOtherCatch.mults(0);
            //log.info("matrixOtherCatch_dim = " + matrixOtherCatch.getDimCount());

            List<PopulationGroup> groups = (List<PopulationGroup>)abondance.getSemantic(0);

            //on soustrait les captures des longliners et nettersSpanish 
            //ces captures sont calculees comme une proportion DES AUTRES CAPTURES (EN NOMBRE) pour chaque mois et groupe de taille
            for (MatrixIterator i = catches.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates(); //Coord 0 = metier ; Coord 1 = length group ; Coord 2 = zone
                Metier metierLoop = (Metier) coord[0];
                PopulationGroup pg = (PopulationGroup) coord[1];
                Zone zone = (Zone) coord[2];
                int month = step.getStep() - step.getYear()*12;
                double valCatchISIS = catches.getValue(coord);
                double valCapture = 0;
                //In the Bay of Biscay
                if((zone.getName().equals("zone_merlu_reproduction") | zone.getName().equals("zone_merlu_recrutementInter") ) & step.getStep()<=2){
                    // If loop on netters as the ratios was calculated on netters only
                    if ((metierLoop.getName().equals("Metier_FiletMerlu_InterSudAPC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudPC")|metierLoop.getName().equals("Metier_FiletMerlu_NordC")|metierLoop.getName().equals("Metier_FiletMerlu_NordPC")|metierLoop.getName().equals("Metier_FiletMIxte_InterC")|metierLoop.getName().equals("Metier_FiletMixte_InterSudC")|metierLoop.getName().equals("Metier_FiletMixte_NordC")|metierLoop.getName().equals("Metier_FiletMixte_NordInterPC")|metierLoop.getName().equals("Metier_FiletMixte_NordPC")|metierLoop.getName().equals("Metier_FiletSole_InterC")|metierLoop.getName().equals("Metier_FiletSole_InterSudPC")|metierLoop.getName().equals("Metier_FiletSole_NordC")|metierLoop.getName().equals("Metier_FiletSole_NordIntPC"))){
                        valCapture = matrixCatchHakeGdGNetters2010.getValue(month,groups.indexOf(coord[1]))/28; // Divide by 14*2 as the same value will be forces for 14 metiers in 2 zones
                        //System.out.println("Capture metier " + metierLoop.getName() + " step " + step.getStep() + " group " + pg.getId() + " zone " + zone.getName() + " = " + valCapture);
                    }
                    // If loop on liners as the ratios was calculated on liners only
                    if ((metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMixte_NordC"))){
                        valCapture = matrixCatchHakeGdGLiners2010.getValue(month,groups.indexOf(coord[1]))/8;  // Divide by 4*2 as the same value will be forces for 4 metiers in 2 zones
                        //System.out.println("Capture metier " + metierLoop.getName() + " step " + step.getStep() + " group " + pg.getId() + " zone " + zone.getName() + " = " + valCapture);
                    }
                    matrixOtherCatch.setValue(coord,valCapture);
                }
                
                else if((zone.getName().equals("zone_merlu_reproduction")| zone.getName().equals("zone_merlu_recrutementInter")|zone.getName().equals("zone_merlu_presence")) & step.getStep()>2 & step.getStep()<=11){
                    // If loop on netters as the ratios was calculated on netters only
                    if ( (metierLoop.getName().equals("Metier_FiletMerlu_InterSudAPC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudPC")|metierLoop.getName().equals("Metier_FiletMerlu_NordC")|metierLoop.getName().equals("Metier_FiletMerlu_NordPC")|metierLoop.getName().equals("Metier_FiletMIxte_InterC")|metierLoop.getName().equals("Metier_FiletMixte_InterSudC")|metierLoop.getName().equals("Metier_FiletMixte_NordC")|metierLoop.getName().equals("Metier_FiletMixte_NordInterPC")|metierLoop.getName().equals("Metier_FiletMixte_NordPC")|metierLoop.getName().equals("Metier_FiletSole_InterC")|metierLoop.getName().equals("Metier_FiletSole_InterSudPC")|metierLoop.getName().equals("Metier_FiletSole_NordC")|metierLoop.getName().equals("Metier_FiletSole_NordIntPC"))){
                        valCapture = matrixCatchHakeGdGNetters2010.getValue(month,groups.indexOf(coord[1]))/42; // Divide by 14*3 as the same value will be forces for 14 metiers in 3 zones
                        //System.out.prScriptUtilintln("Capture metier " + metierLoop.getName() + " step " + step.getStep() + " group " + pg.getId() + " zone " + zone.getName() + " = " + valCapture);
                    }
                    // If loop on liners as the ratios was calculated on liners only
                    if ( (metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMixte_NordC"))){
                        valCapture = matrixCatchHakeGdGLiners2010.getValue(month,groups.indexOf(coord[1]))/12;  // Divide by 4*3 as the same value will be forces for 4 metiers in 3 zones
                        //System.out.println("Capture metier " + metierLoop.getName() + " step " + step.getStep() + " group " + pg.getId() + " zone " + zone.getName() + " = " + valCapture);
                    }
                    matrixOtherCatch.setValue(coord,valCapture);
                }

                else if(zone.getName().equals("zone_merlu_recrutement") |zone.getName().equals("zone_merlu_recrutementInter") |zone.getName().equals("zone_merlu_reproduction")|zone.getName().equals("zone_merlu_presence")){
                    // If loop on netters as the ratios was calculated on netters only
                    if (step.getStep()>11 & (metierLoop.getName().equals("Metier_FiletMerlu_InterSudAPC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudC")|metierLoop.getName().equals("Metier_FiletMerlu_InterSudPC")|metierLoop.getName().equals("Metier_FiletMerlu_NordC")|metierLoop.getName().equals("Metier_FiletMerlu_NordPC")|metierLoop.getName().equals("Metier_FiletMIxte_InterC")|metierLoop.getName().equals("Metier_FiletMixte_InterSudC")|metierLoop.getName().equals("Metier_FiletMixte_NordC")|metierLoop.getName().equals("Metier_FiletMixte_NordInterPC")|metierLoop.getName().equals("Metier_FiletMixte_NordPC")|metierLoop.getName().equals("Metier_FiletSole_InterC")|metierLoop.getName().equals("Metier_FiletSole_InterSudPC")|metierLoop.getName().equals("Metier_FiletSole_NordC")|metierLoop.getName().equals("Metier_FiletSole_NordIntPC"))){
                        valCapture = matrixRatioCatchHakeGdGNetters.getValue(month,groups.indexOf(coord[1])) * valCatchISIS;
                    }
                    // If loop on liners as the ratios was calculated on liners only
                    if (step.getStep()>11 & (metierLoop.getName().equals("Metier_PalangreMerlu_InterSudAC")|metierLoop.getName().equals("Metier_PalangreMerlu_InterSudC")|metierLoop.getName().equals("Metier_PalangreMixte_InterC")|metierLoop.getName().equals("Metier_PalangreMixte_NordC"))){
                        valCapture = matrixRatioCatchHakeGdGLiners.getValue(month,groups.indexOf(coord[1])) * valCatchISIS;
                    }
                    matrixOtherCatch.setValue(coord,valCapture);
                }
            }

            MatrixND matrixOtherCatchNumSaveBefore = matrixOtherCatch.copy();
            /*
            // Catch export with zones. Export in numbers only, to be converted to weight then.
            String stringToExport = "";
           //Intermediary abundance export for checks
           for (MatrixIterator i = matrixOtherCatch.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                Metier met = (Metier) coord[0];
                PopulationGroup pg = (PopulationGroup) coord[1];
                Zone zone = (Zone) coord[2];
                double valCatch = matrixOtherCatch.getValue(coord);
                stringToExport+= step.getStep() + ";" + met.getName() + ";" + pg.getId() + ";" + zone.getName() + ";" + valCatch + "\n";
            }
            FileUtils.writeStringToFile(catchSP,stringToExport,true);
            
            
           
           stringToExport = "";
           //Intermediary abundance export for checks
           
           for (MatrixIterator i = abondance.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                Zone zone = (Zone) coord[1];
                double valAbundance = abondance.getValue(coord);
                stringToExport+= step.getStep() + ";" + pg.getId() + ";" + zone.getName() + ";" + valAbundance + "\n";
            }
            FileUtils.writeStringToFile(abundanceInter,stringToExport,true);
            */

          
            
            //Remove catch from abundance matrix
            MatrixND catchToRemove = matrixOtherCatch.copy();
            MatrixND matrixOtherCatchNum = matrixOtherCatch.sumOverDim(0); // Somme sur les metiers
            matrixOtherCatchNum = matrixOtherCatchNum.reduce();
            catchToRemove = catchToRemove.sumOverDim(0); // Somme sur les metiers
            catchToRemove = catchToRemove.reduce();
            for (MatrixIterator i = catchToRemove.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                Zone zone = (Zone) coord[1];
                double valAbundance = abondance.getValue(coord);
                double valCatch = catchToRemove.getValue(coord);
                double diff = valAbundance-valCatch;
                double ratio = valCatch/valAbundance;
                //System.out.println("Abondance step " + step.getStep() + " groupe " + pg.getId() + " zone " + zone.getName() + " val " + valAbundance + " catch " + valCatch);
                if ( ratio < propIfNotEnoughFish ){
                    abondance.setValue(coord,diff);
                } else if( ratio >= propIfNotEnoughFish ){ // If there not enough fish to fish, don't fish!
                    double newCatch = propIfNotEnoughFish*valAbundance ;
                    diff = valAbundance - newCatch;
                    matrixOtherCatchNum.setValue(coord,newCatch);
                    abondance.setValue(coord,diff);  
                }
                
            }

            /*
           stringToExport = "";
           //Intermediary abundance export for checks
           for (MatrixIterator i = abondance.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                Zone zone = (Zone) coord[1];
                double valAbundance = abondance.getValue(coord);
                stringToExport+= step.getStep() + ";" + pg.getId() + ";" + zone.getName() + ";" + valAbundance + "\n";
            }
            FileUtils.writeStringToFile(abundanceInter2,stringToExport,true);
            */
             
            // Transformation de captures en Abondance en captures en Biomasse
            MatrixND matrixOtherCatchSaveNum = matrixOtherCatchNum.copy(); // Save this matrix, that will not have the partition dimension, but will have the zone dimension
            matrixOtherCatchNum = matrixOtherCatchNum.sumOverDim(1); //Somme sur les zones
            matrixOtherCatchNum = matrixOtherCatchNum.reduce();

            //Imprimer encore.
            MatrixND matrixOtherCatchWeight = matrixOtherCatchNum.copy(); //Save for checks
            MatrixND matrixOtherCatchSaveWeight = matrixOtherCatchSaveNum.copy(); //Save for checks
            MatrixND matrixOtherCatchSaveWeightBefore = matrixOtherCatchNumSaveBefore.copy(); //Save for checks
            //log.info("matrixOtherCatchWeight = " + matrixOtherCatchWeight);
            
            //on stocke cette matrice en nombre dans le resultManager pour la HCR


            // Poids en TONNES par groupe de taille (issu de la relation taille/poids de l'ICES utilisee dans ISIS)
            for (MatrixIterator i = matrixOtherCatchNum.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                double valCatchNum = matrixOtherCatchNum.getValue(coord);
                matrixOtherCatchWeight.setValue(coord, valCatchNum * matrixWeightAtLengthHake.getValue(pg.getId()));
            }

            for (MatrixIterator i = matrixOtherCatchSaveWeight.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                double valCatchNum = matrixOtherCatchSaveWeight.getValue(coord);
                matrixOtherCatchSaveWeight.setValue(coord, valCatchNum * matrixWeightAtLengthHake.getValue(pg.getId()));
            }

            
            for (MatrixIterator i = matrixOtherCatchSaveWeightBefore.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[1];
                double valCatchNum = matrixOtherCatchSaveWeightBefore.getValue(coord);
                matrixOtherCatchSaveWeightBefore.setValue(coord, valCatchNum * matrixWeightAtLengthHake.getValue(pg.getId()));
            }
            
            //log.info("matrixOtherCatchWeight = " + matrixOtherCatchWeight);
                    
            // On somme la matrice cree contenant les captures autres sur les zones et les groupes 
            // pour avoir les captures autres cumules pour le pas de temps courant. 
            //double totOtherCatchStep = matrixOtherCatchWeight.sumAll();
            //log.info("totOtherCatchStep_export = " + totOtherCatchStep);
            
            // On stocke cette valeur dans le SimulationContext
            for (MatrixIterator i = matrixOtherCatchWeight.iterator(); i.next();) {
                Object [] coord = i.getSemanticsCoordinates();
                PopulationGroup lengthGroup = (PopulationGroup) coord[0];
                int lengthGroupId = lengthGroup.getId();
                //System.out.println("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep() + "; value : " + matrixOtherCatchWeight.getValue(coord));
                //System.out.println("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep() + "; value : " + matrixOtherCatchNum.getValue(coord));
                context.setValue("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep(), matrixOtherCatchWeight.getValue(coord));
                context.setValue("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep(), matrixOtherCatchNum.getValue(coord));
            }
            
            outputSpanishCatch="";
            outputSpanishCatchNum="";
            if(NoRule & !affectation){
                 /*
                 * Export spanish longliners gillnetters catch
                 */
                
                if(step.getStep()==0){
                    outputSpanishCatch+="[12,4,2,63]\n";
                    outputSpanishCatch+="java.lang.Integer:0,1,2,3,4,5,6,7,8,9,10,11\n";
                    outputSpanishCatch+="java.lang.String:LONGLINEGILLNET_BOB,TRAWL_FISH_BOB_E,TRAWL_FISH_BOB_W,TRAWL_NEP\n";
                    outputSpanishCatch+="java.lang.String:discard,landing\n";
                    outputSpanishCatch+="java.lang.Integer:2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64\n";
                }
                for (int lengthGroupId=5; lengthGroupId<72; lengthGroupId++) { 
                    double landVal = (double)context.getValue("totOtherCatchStep_" + lengthGroupId + "_" + step.getStep());
                    outputSpanishCatch+= step.getStep() + ";0;1;" + (lengthGroupId-2) +";" +landVal + "\n";
                }
                FileUtils.writeStringToFile(outputCatch,outputSpanishCatch, UTF_8, true);
    
                if(step.getStep()==0){
                    outputSpanishCatchNum+="[12,4,2,63]\n";
                    outputSpanishCatchNum+="java.lang.Integer:0,1,2,3,4,5,6,7,8,9,10,11\n";
                    outputSpanishCatchNum+="java.lang.String:LONGLINEGILLNET_BOB,TRAWL_FISH_BOB_E,TRAWL_FISH_BOB_W,TRAWL_NEP\n";
                    outputSpanishCatchNum+="java.lang.String:discard,landing\n";
                    outputSpanishCatchNum+="java.lang.Integer:2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64\n";
                }
                for (int lengthGroupId=5; lengthGroupId<72; lengthGroupId++) { 
                    double landVal = (double)context.getValue("totOtherCatchNumStep_" + lengthGroupId + "_" + step.getStep());
                    outputSpanishCatchNum+= step.getStep() + ";0;1;" + (lengthGroupId-2) +";" +landVal + "\n";
                }
                FileUtils.writeStringToFile(outputCatchNum, outputSpanishCatchNum, UTF_8, true); 
            }
    
            /*
            * Export spanish longliners gillnetters catch WITH zones but WITHOUT partitions DO NOT export under the semantics format. Always comment these parts.
            */
            
            outputSpanishCatchNum="";
            for (MatrixIterator i = matrixOtherCatchSaveNum.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                Zone zone = (Zone) coord[1];
                double valCatchNum = matrixOtherCatchSaveNum.getValue(coord);
                outputSpanishCatchNum+= step.getStep() + ";0;"+ zone.getName() + ";" + pg.getId() +";" +valCatchNum + "\n";
            }
            FileUtils.writeStringToFile(outputCatchNumSave, outputSpanishCatchNum, UTF_8, true);
            
            /*
            outputSpanishCatch="";
            for (MatrixIterator i = matrixOtherCatchSaveWeight.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                PopulationGroup pg = (PopulationGroup) coord[0];
                Zone zone = (Zone) coord[1];
                double valCatchNum = matrixOtherCatchSaveWeight.getValue(coord);
                outputSpanishCatch+= step.getStep() + ";0;"+ zone.getName() + ";" + pg.getId() +";" +valCatchNum + "\n";
            }
            FileUtils.writeStringToFile(outputCatchSave,outputSpanishCatch, UTF_8, true);
            */
            
            outputSpanishCatchNum="";
            for (MatrixIterator i = matrixOtherCatchNumSaveBefore.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                Metier metierInTheLoop = (Metier) coord[0];
                PopulationGroup pg = (PopulationGroup) coord[1];
                Zone zone = (Zone) coord[2];
                double valCatchNum = matrixOtherCatchNumSaveBefore.getValue(coord);
                outputSpanishCatchNum+= step.getStep() + ";0;"+ zone.getName() + ";"+ metierInTheLoop.getName() + ";" + pg.getId() +";" +valCatchNum + "\n";
            }
            FileUtils.writeStringToFile(outputCatchNumSaveBefore, outputSpanishCatchNum, UTF_8, true);
            
            /*
            outputSpanishCatch="";
            for (MatrixIterator i = matrixOtherCatchSaveWeightBefore.iterator(); i.next();) {
                Object[] coord = i.getSemanticsCoordinates();
                Metier metierInTheLoop = (Metier) coord[0];
                PopulationGroup pg = (PopulationGroup) coord[1];
                Zone zone = (Zone) coord[2];
                double valCatchNum = matrixOtherCatchSaveWeightBefore.getValue(coord);
                outputSpanishCatch+= step.getStep() + ";0;"+ zone.getName() + ";"+ metierInTheLoop.getName() + ";" + pg.getId() +";" +valCatchNum + "\n";
            }
            FileUtils.writeStringToFile(outputCatchSaveBefore,outputSpanishCatch, UTF_8, true);
            */
            affectation = true;
        }
    }
}
