@echo off

:: Uncomment to easy configure memory for ISIS-Fish
:: SET MEMORY="-Xmx2048M"

:: Uncomment to easy configure R for ISIS-Fish
:: SET R_HOME=C:\Program Files\R\R-3.6.2

echo [Script] Isis starting...
java %MEMORY% -jar isis-fish-4.4.5.1.jar %* > debug.txt 2>&1
